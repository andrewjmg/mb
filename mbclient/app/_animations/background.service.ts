export class Background {
  constructor(public state = 'inactive') {}

  toggleState() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }

}
