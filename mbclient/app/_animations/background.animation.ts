import { trigger, state, animate, transition, style } from '@angular/animations';

export const BackgroundAnimation = [
  trigger('backgroundState', [
    state('inactive', style({
      backgroundColor: 'transparent'
    })),
    state('active', style({
      backgroundColor: 'rgba(132, 202, 231, .1)'
    })),
    transition('inactive <=> active', animate('300ms ease-in'))
  ])
];
