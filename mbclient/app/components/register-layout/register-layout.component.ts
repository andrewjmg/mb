import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../services/auth-services/register.service';
import { RegisterConstants } from '../../shared/constants/register';

@Component({
  selector: 'app-register-layout',
  templateUrl: './register-layout.component.html',
  styleUrls: ['./register-layout.component.css']
})
export class RegisterLayoutComponent implements OnInit {

  stepInfo: any;

  constructor( private registerService: RegisterService ) {
    this.registerService.stepInfo$.subscribe( data => {
      console.log(data);
      this.stepInfo = data;
      this.stepInfo.numSteps = RegisterConstants.numSteps;
    });
  }

  ngOnInit() {
  }

}
