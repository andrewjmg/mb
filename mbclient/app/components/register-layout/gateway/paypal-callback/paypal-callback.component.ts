import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import {environment} from '../../../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {GatewayService} from '../../../../services/gateway.service';
import {AuthService} from '../../../../services/auth.service';

@Component({
  selector: 'app-paypal-callback',
  templateUrl: './paypal-callback.component.html',
  styles: []
})
export class PaypalCallbackComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private http: Http,
              private gtwService: GatewayService,
              private authService: AuthService
  ) {
  }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(query => {
      const url = `${environment.apiUrl}/gateways/paypal/token`;
      const body = {code: query['code']};
      const headers = new Headers({'Content-Type': 'application/json'});
      const options = new RequestOptions({headers});

      this.http.post(url, body, options).catch((error: any) => Observable.throw(error.json().error || 'Server error'))
        .subscribe(res => {


          const uid = localStorage.getItem('uid');
          const parsedBddy = JSON.parse(res._body);
          let id = parsedBddy.id;
          const name = 'Paypal';
          const type = 'paypal';
          id = id.split("/")[id.split("/").length - 1];
          const access_token = parsedBddy.access_token;

          const account      = JSON.parse(localStorage.getItem('account'));

          this.authService.getMe().onAuthStateChanged(user => {
            const uid = user.uid;

            this.gtwService.registerGateway(uid, name, id, access_token, type, account.currency)
              .subscribe((data) => {
                console.log('From Stripe Callback Component', data);+
                  // TODO: Check similar function but with paypal
                /*this.http.put(`${environment.apiUrl}/gateways/stripe/${data.key}`, {access_token}, options).catch((error: any) => Observable.throw(error.json().error || 'Server error'))
                  .subscribe(res => {
                    console.log(res.body);
                  });*/
                this.authService.getMe().currentUser.getIdToken().then(jwt => {
                  this.requestUserApplicationData(jwt);
                })
              });

            this.authService.getMe().currentUser.getIdToken().then(jwt => console.log(jwt));
          }, err => {
            console.log(err);
          });

          /*this.gtwService.addGateway(uid, name, id, access_token, type)
            .subscribe((data) => {
              localStorage.setItem('gtw_key', data.key);
              if (name === 'Paypal') {
                localStorage.setItem('paypal_r', '1');
              }
              console.log("We are done");
              this.router.navigate(['register/gateway']);
            });*/
        });
    });
  }

  loadCurrentUserClaims() {
    this.authService.getMe().currentUser.getIdToken(true).then(jwt => {
      const userObject = JSON.parse(atob(jwt.split('.')[1]));
      const {gateways} = userObject;
      this.setUserGateways(gateways);
    });
  }

  requestUserApplicationData(jwt) {
    const url = `${environment.apiUrl}/auth`;
    const Authorization = `Bearer ${jwt}`;
    const headers = new Headers({Authorization: Authorization});
    const options = new RequestOptions({ headers });
    this.http.post(url, {}, options).catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .subscribe(res => {
        this.loadCurrentUserClaims();
      });
  }

  setUserGateways(gateways) {

    const account = JSON.parse(localStorage.getItem('account'));

    localStorage.setItem('gateways', JSON.stringify(gateways));
    localStorage.setItem('u', JSON.stringify({name: account.user.firstName, email: account.user.email}));
    localStorage.removeItem('account');
    localStorage.removeItem('stripe_r');
    localStorage.removeItem('paypal_r');

    if (gateways && gateways.length > 0) {
      localStorage.setItem('gtwKey', gateways[0].gatewayKey);
    }

    this.router.navigate(['/first']);
  }

}
