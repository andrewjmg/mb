import {Component, OnInit, HostBinding} from '@angular/core';
import {RegisterService} from '../../../services/auth-services/register.service';
import {RegisterConstants} from '../../../shared/constants/register';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {fadeInAnimation} from '../../../_animations/index';
import {currencies} from '../../../shared/constants/currencies';
import {AngularFireDatabase} from 'angularfire2/database';


declare var $;

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css'],
  animations: [fadeInAnimation]
})
export class RegisterGatewayComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  form: FormGroup;
  gateways: any;
  selectedGateway: any;
  stripeOAuth: string;
  paypalOAuth: string;
  currencies: any[] = currencies;
  currency: any;

  stripeRegistered = false;
  paypalRegistered = false;

  constructor(private registerService: RegisterService, private router: Router, private db: AngularFireDatabase) {
    this.form = new FormGroup({
      'gateway': new FormControl('', Validators.required)
    });
    // this.currency = this.currencies[0].code;
    this.gateways = RegisterConstants.gateways;
  }

  ngOnInit() {
    console.log(this.currencies);
    this.registerService.nextStep({
      title: RegisterConstants.gateway.title,
      step: RegisterConstants.gateway.step
    });

    this.stripeOAuth = environment.stripe.oauthUrl;
    this.paypalOAuth = environment.paypal.oauthUrl;

    this.currency = this.currencies[0].code;

    /*console.log(this.registerService.getRegisterData());
    if (localStorage.getItem('stripe_r')) {
      this.stripeRegistered = !this.stripeRegistered;
      this.currency = this.currencies[0].code;
    }

    if (localStorage.getItem('paypal_r')) {
      this.paypalRegistered = !this.paypalRegistered;
    }*/
  }

  next() {

    if (this.selectedGateway && this.currency) {
      const curr = this.currencies.find(obj => obj.code === this.currency);
      if (curr) {

        const account           = JSON.parse(localStorage.getItem('account'));
        account.selectedGateway = this.selectedGateway;
        account.currency        = curr;
        localStorage.setItem('account', JSON.stringify(account));
        this.router.navigate(['/register/confirmation']);

      }
    }

    /*const gtwKey = localStorage.getItem('gtw_key');
    const gtwObject = this.db.object(`/gateways/${gtwKey}`);
    const curr = this.currencies.find(obj => obj.code === this.currency);
    if (curr) {
      gtwObject.update({currency: curr}).then(() => {
        this.router.navigate(['/register/confirmation']);
      }).catch(err => console.log(err));
    }*/
  }

  connectStripe() {
    this.selectedGateway = 'stripe';
    localStorage.setItem('gatewayType', 'stripe');
    /*if (!localStorage.getItem('stripe_r')) {
      window.location.href = this.stripeOAuth;
    }*/
  }

  connectPaypal() {
    this.selectedGateway = 'paypal';
    localStorage.setItem('gatewayType', 'paypal');
    /*if (!localStorage.getItem('paypal_r')) {
      window.location.href = this.paypalOAuth;
    }*/
  }

  onSelectionGateway(gateway: any) {
    // this.selectedGateway = gateway;
    // $('#loginGatewayModal').modal('toggle');
    this.registerService.connectStripe()
      .subscribe(data => {
        console.log(data);
      });
  }

  selectedCurrency() {
    console.log(this.currency);
  }

}
