import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../../services/auth-services/register.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { RegisterConstants } from '../../../shared/constants/register';
import { fadeInAnimation } from '../../../_animations/index';
import {AlertService} from '../../../shared/alert/_services/alert.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [fadeInAnimation]
})
export class ProfileComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  form: FormGroup;
  showAlert = false;
  userProfile: any = { firstName: '', email: '', password: '' };

  constructor( private registerService: RegisterService, private router: Router,
               private afAuth: AngularFireAuth, private db: AngularFireDatabase,
               private alertService: AlertService) {

    this.form = new FormGroup({
      'firstName': new FormControl('', [Validators.required]),
      'email'    : new FormControl('', [Validators.required, Validators.email]),
      'password' : new FormControl('', [Validators.required, Validators.minLength(8)])
    });

  }

  ngOnInit() {
    this.registerService.nextStep({
      title: RegisterConstants.profile.title,
      step: RegisterConstants.profile.step
    });
  }

  nextStep($event) {
    $event.preventDefault();
    if (this.form.valid) {

      this.afAuth.auth.createUserWithEmailAndPassword(this.form.value.email, this.form.value.password)
        .then(user => {

          console.log(user);
          localStorage.setItem('uid', user.uid);
          this.userProfile.firstName = this.form.value.firstName;
          this.userProfile.email     = this.form.value.email;
          this.userProfile.password  = this.form.value.password;
          const account              = {user: this.userProfile};

          localStorage.setItem('account', JSON.stringify(account));
          this.router.navigate(['/register/plan']);

        })
        .catch(err => {
          this.alertService.error(err.message);
          setTimeout(() => this.alertService.clear(), 3000);
        });

      /*this.afAuth.auth.createUserWithEmailAndPassword( this.userProfile.email, this.userProfile.password )
        .then(user => {
          const uid = user.uid;
          const user_list = this.db.object(`/users/${uid}`);
          user_list.set({email: this.userProfile.email, name: this.form.value.firstName})
            .then((success) => {
            localStorage.setItem('uid', uid);
            this.registerService.setUser( this.userProfile );
            const account = {user: this.userProfile};
            localStorage.setItem('account', JSON.stringify(account));
            this.router.navigate(['/register/plan']);
          })
            .catch((error) => {
            console.log(error);
          });
        }).catch(err => {
        this.alertService.error(err.message);
        // setTimeout(() => this.alertService.clear(), 3000);
      });*/
    }else {
      this.showAlert = !this.showAlert;
      setTimeout(() => this.showAlert = false, 3500);
    }
  }

}
