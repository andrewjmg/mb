import { Component, OnInit, HostBinding } from '@angular/core';
import { RegisterService } from '../../../services/auth-services/register.service';
import { RegisterConstants } from '../../../shared/constants/register';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database'
import {countries} from '../../../shared/constants/countries';
import {Observable} from 'rxjs/Observable';
import {fadeInAnimation} from '../../../_animations/index';
import {AlertService} from '../../../shared/alert/_services/alert.service';

declare var Stripe;

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css'],
  animations: [fadeInAnimation]
})
export class BillingComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  form: FormGroup;
  validating = false;
  countries         = countries;
  invalidCard       = false;
  brand             = '';
  cardError: any;
  cardNumber: any;
  cardExpiry: any;
  cardCVC: any;
  stripe: any;


  constructor(
    private registerService: RegisterService,
    private router: Router,
    private db: AngularFireDatabase,
    private alertService: AlertService) {
    this.form = new FormGroup({
      'nameCard'   : new FormControl('', Validators.required),
      'country'    : new FormControl(this.countries[0].code, Validators.required),
      'street'     : new FormControl('', Validators.required)
    });
    /*this.form = new FormGroup({
      'nameCard'   : new FormControl('', Validators.required),
      'country'    : new FormControl('', Validators.required),
      'street'     : new FormControl('', Validators.required)
    });*/
  }

  ngOnInit() {
    this.registerService.nextStep({
      title: RegisterConstants.billing.title,
      step: RegisterConstants.billing.step
    });

    this.stripe = Stripe('pk_test_x1FBg0azepCuyAgNeYYfzgFG');
    const elements = this.stripe.elements();
    this.cardNumber = elements.create('cardNumber');
    this.cardNumber.mount('#card-number');
    this.cardNumber.addEventListener('change', (event) => this.cardError = null);

    this.cardCVC = elements.create('cardCvc');
    this.cardCVC.mount('#card-cvc');
    this.cardCVC.addEventListener('change', (event) => this.cardError = null);

    this.cardExpiry = elements.create('cardExpiry');
    this.cardExpiry.mount('#card-exp');
    this.cardExpiry.addEventListener('change', (event) => this.cardError = null);

  }

// :TODO: Must check if Typeahed is good option
//      search = (text$: Observable<string>) =>
//        text$
//          .debounceTime(200)
//          .distinctUntilChanged()
//          .map(term => term.length < 2 ? []
//            : countries.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
//      formatter = (x: {name: string}) => x.name;

  nextStep($event) {
    $event.preventDefault();
    if (this.form.valid) {
      this.validating = !this.validating;
      this.stripe.createToken(this.cardNumber).then( result => {
        console.log(result);
        if (result.error) {
          console.log(result.error);
          this.invalidCard = true;
          this.validating  = !this.validating;
          this.cardError   = result.error;
          this.alertService.error(result.error.message);
          const tout = setTimeout(() => {
            this.invalidCard = !this.invalidCard;
            clearTimeout(tout);
          }, 3000);
        }else {
          this.invalidCard  = false;
          const account     = JSON.parse(localStorage.getItem('account'));
          account.cardToken = result;
          localStorage.setItem('account', JSON.stringify(account));
          this.router.navigate(['/register/gateway']);
          /*this.registerService.createCustomer({
            email: JSON.parse(localStorage.getItem('account')).user.email,
            source: result.token.id
          }).subscribe(data => {
            console.log(data);

            this.registerService.createSubscription({
              customer: data.id,
              plan_id: JSON.parse(localStorage.getItem('account')).plan.id
            }).subscribe(data_subs => {
              console.log(data_subs);

              const uid = localStorage.getItem('uid');
              const userdb = this.db.object(`/users/${uid}`);
              userdb.update({ customer_id: data.id, subscription_id: data_subs.id })
                .then(() => {
                  const account = JSON.parse(localStorage.getItem('account'));
                  account.billing = {customerId: data.id, subscriptionId: data_subs.id};
                  localStorage.setItem('account', JSON.stringify(account));
                  this.validating  = !this.validating;
                  this.router.navigate(['/register/gateway']);
                }).catch( err => {
                console.log('Update firebase error', err);
              });
            }, err => {
              console.log('Subscription error', err.json());
            });

          }, err => {
            console.log('Create Customer error', err.json());
          });*/
        }
      });
    }
  }

}
