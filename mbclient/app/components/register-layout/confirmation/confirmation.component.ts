import { Component, OnInit, HostBinding } from '@angular/core';
import {RegisterService} from '../../../services/auth-services/register.service';
import {RegisterConstants} from '../../../shared/constants/register';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import {fadeInAnimation} from '../../../_animations/index';
import * as moment from 'moment';
import {environment} from '../../../../environments/environment';
import {AngularFireDatabase} from 'angularfire2/database';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css'],
  animations: [fadeInAnimation]
})
export class ConfirmationComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  billing: any;
  gateway: any;
  plan: any;
  user: any;

  constructor(
    private registerService: RegisterService,
    private authService: AuthService,
    private router: Router,
    private db: AngularFireDatabase
  ) { }

  ngOnInit() {

    this.registerService.nextStep({
      title: RegisterConstants.confirm.title,
      step: RegisterConstants.confirm.step,
      msg: RegisterConstants.confirm.msg
    });
    const account = JSON.parse(localStorage.getItem('account'));
    this.billing  = account.cardToken.token;
    this.gateway  = account.selectedGateway;
    this.plan     = account.plan;
    this.user     = account.user;

    this.plan.created = moment(this.plan.created).format('YYYY/MM/DD');

    console.log(account);
  }

  confirm() {
    const {user: {email:email}, billing: {id: source}, plan: {id: plan_id}, gateway} = this;
    const data = {
      email,
      source,
      plan_id,
      metadata: {
        gateway
      }
    };

    this.registerService.registerWithEmail(data)
      .subscribe(register => {
        console.log(register);

        const customer     = register.customer;
        const subscription = register.subscription;
        const account      = JSON.parse(localStorage.getItem('account'));
        const uid          = localStorage.getItem('uid');

        const userdb = this.db.object(`/users/${uid}`);
        userdb.set({ name: account.user.firstName, email: account.user.email, customer_id: customer.id, subscription_id: subscription.id })
          .then(() => {
            if (this.gateway === 'stripe') {
              window.location.href = environment.stripe.oauthUrl;
            }else if (this.gateway === 'paypal') {
              window.location.href = environment.paypal.oauthUrl;
            }
          }).catch( err => {
          console.log('Update firebase error', err);
        });

      }, err => {
        console.log(err);
      });

    /*this.authService.loginWithEmailAndPassword(this.user.email, this.user.password)
      .then( data => {
        console.log(this.user);
        localStorage.setItem('u', JSON.stringify({name: this.user.firstName, email: this.user.email}));
        localStorage.setItem('uid', data.uid);
        localStorage.removeItem('account');
        localStorage.removeItem('stripe_r');
        localStorage.removeItem('paypal_r');
        this.router.navigate(['/first']);
      })
      .catch( error => {
        console.log(error);
      });*/
  }

}
