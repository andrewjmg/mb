import { Component, OnInit, HostBinding } from '@angular/core';
import {RegisterService} from '../../../services/auth-services/register.service';
import {RegisterConstants} from '../../../shared/constants/register';
import {fadeInAnimation} from '../../../_animations/index';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css'],
  animations: [fadeInAnimation]
})
export class SuccessComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  constructor( private registerService: RegisterService ) { }

  ngOnInit() {
    this.registerService.nextStep({
      step: RegisterConstants.success.step,
      msg: ''
    });
  }

}
