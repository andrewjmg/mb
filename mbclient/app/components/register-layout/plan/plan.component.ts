import { Component, OnInit, HostBinding } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {RegisterService} from '../../../services/auth-services/register.service';
import {RegisterConstants} from '../../../shared/constants/register';
import {Router} from '@angular/router';
import {PlanSelection} from '../../../shared/constants/plan-selection';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {fadeInAnimation} from '../../../_animations/index';
declare var Stripe;

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css'],
  animations: [fadeInAnimation]
})
export class PlanComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';

  month: any[];
  year: any[];
  showPlans: any[] = [];
  form: FormGroup;
  selectedPlan: any;


  constructor( private registerService: RegisterService, private router: Router, private db: AngularFireDatabase ) {
    this.form = new FormGroup({
      'plan': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {

    this.registerService.nextStep({
      title: RegisterConstants.plan.title,
      step: RegisterConstants.plan.step
    });

    this.registerService.getPlans()
      .subscribe( data => {
        console.log(data);
        this.month        = data.month;
        this.year         = data.year;
        this.showPlans    = data.month;
        this.selectedPlan = this.showPlans[0];
      });
  }

  changePlan( type: string ) {
    this.form.reset();
    this.showPlans    = type === 'month' ? this.month : this.year;
    this.selectedPlan = this.showPlans[0];
  }

  onSelectionPlan( plan: any ) {
    console.log(plan);
    this.selectedPlan = plan
  }

  nextStep() {
    if (this.form.valid) {
      const account = JSON.parse(localStorage.getItem('account'));
      account.plan  = this.selectedPlan;
      localStorage.setItem('account', JSON.stringify(account));
      this.router.navigate(['/register/billing']);
    }
  }

}
