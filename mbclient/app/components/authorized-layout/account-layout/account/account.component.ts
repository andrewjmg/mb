import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {AuthService} from '../../../../services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularFireDatabase, FirebaseObjectObservable} from 'angularfire2/database';
import * as firebase from 'firebase';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  user: FirebaseObjectObservable<any>;
  form: FormGroup;
  uid: string;

  constructor(private auth: AngularFireAuth, private authService: AuthService, private db: AngularFireDatabase) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [])
    });
  }

  ngOnInit() {
    this.authService.getMe().onAuthStateChanged((user) => {
      console.log(user);
      this.uid = user.uid;
      this.user = this.db.object(`/users/${this.uid}`);
      const u = JSON.parse(localStorage.getItem('u'));
      this.form.setValue({name: u.name, email: u.email, password: ''});
    });
  }

  updateAccount($event) {
    $event.preventDefault();
    console.log(this.form);

    if (this.form.valid) {
      if (!this.form.controls.name.pristine) {
        this.user.update({name: this.form.value.name})
          .then(() => {
            this.user.subscribe(uData => {
              this.form.reset({name: uData.name, email: uData.email, password: ''});
            }, err => {
              console.log(err);
            });
          })
          .catch(err => {
            console.log(err);
          });
      }

      if (!this.form.controls.email.pristine) {

        const u           = JSON.parse(localStorage.getItem('u'));
        const credentials = firebase.auth.EmailAuthProvider.credential(u.email, this.form.value.password);
        const user        = this.authService.getMe().currentUser;
        user.reauthenticateWithCredential(credentials)
          .then(() => {
            console.log('authenticated');
            user.updateEmail(this.form.value.email)
              .then(() => {
// TODO: Update local storage to new email, update firebase user node to new email, reset form with new values.
                alert('Email Updated')
              })
              .catch(err => {
                console.log(err);
              })
          })
          .catch(err => {
            console.log(err);
          });
      }

      if (!this.form.controls.password.pristine) {

      }
    }
  }

}
