import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../services/account.service';

@Component({
  selector: 'app-account-layout',
  templateUrl: './account-layout.component.html',
  styleUrls: ['./account-layout.component.css']
})
export class AccountLayoutComponent implements OnInit {

  location: any;

  constructor(
    private accountService: AccountService
  ) {
    this.accountService.menuLocation$.subscribe(data => {
      this.location = data;
    });
  }

  ngOnInit() {
  }

}
