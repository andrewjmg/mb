import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, FirebaseObjectObservable} from 'angularfire2/database';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from '../../../../services/auth.service';
import {AccountService} from '../../../../services/account.service';
import {AlertService} from '../../../../shared/alert/_services/alert.service';
import * as firebase from 'firebase';
import {Location} from '@angular/common';

@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  styleUrls: ['./profile-update.component.css']
})
export class ProfileUpdateComponent implements OnInit {

  user: FirebaseObjectObservable<any>;
  form: FormGroup;
  uid: string;
  nameDone  = false;
  emailDone = false;
  passDone  = false;
  loading   = false;

  constructor(
    private _location: Location,
    private auth: AngularFireAuth,
    private authService: AuthService,
    private db: AngularFireDatabase,
    private accountService: AccountService,
    private alertService: AlertService) {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      newPassword: new FormControl('', []),
      currentPassword: new FormControl('', [])
    });
  }

  ngOnInit() {

    this.accountService.changeLocation({
      location: 'Profile Update',
      description: 'Update your profile information'
    });

    this.authService.getMe().onAuthStateChanged((user) => {
      this.uid  = user.uid;
      this.user = this.db.object(`/users/${this.uid}`);
      const u   = JSON.parse(localStorage.getItem('u'));
      this.form.setValue({name: u.name, email: u.email, newPassword: '', currentPassword: ''});
    });
  }

  updateAccount($event) {
    $event.preventDefault();
    if (this.form.valid && !this.loading) {

      if (!this.form.controls.name.pristine || !this.form.controls.email.pristine || !this.form.controls.newPassword.pristine){
        this.loading = true;
      }

      if (!this.form.controls.name.pristine) {
        this.updateName();
      }else {
        this.nameDone = true;
      }

      if (!this.form.controls.email.pristine) {
        if (this.form.value.currentPassword.length >= 8) {
          this.updateEmail();
        }else {
          this.alertService.error('Wrong Password')
        }
      }else {
        this.emailDone = true;
      }

      if (!this.form.controls.newPassword.pristine) {
        if (this.form.value.newPassword.length >= 8 && this.form.value.currentPassword.length >= 8) {
          this.updatePassword();
        }else {
          this.alertService.error('The length of the passwords must be at least 8 characters');
          setTimeout(() => this.alertService.clear(), 3500);
        }
      }else {
        this.passDone = true;
      }

      if (this.loading) {
        const interval = setInterval(() => {
          if (this.nameDone && this.emailDone && this.passDone) {
            const u = JSON.parse(localStorage.getItem('u'));
            this.form.reset({name: u.name, email: u.email, newPassword: '', currentPassword: ''});
            this.loading = false;
            this.alertService.success('Profile updated');
            clearInterval(interval);
          }
        }, 500);
      }

    }
  }

  private updateName() {
    this.user.update({name: this.form.value.name})
      .then(() => {
        const user = JSON.parse(localStorage.getItem('u'));
        user.name  = this.form.value.name;
        localStorage.setItem('u', JSON.stringify(user));
        this.nameDone = true;
        this.form.controls.name.setValue(this.form.value.name);
      })
      .catch(err => {
        this.nameDone = true;
        this.loading  = false;
        this.alertService.error(err.message);
        setTimeout(() => this.alertService.clear(), 2500);
        console.log(err);
      });
  }

  private updateEmail() {
    const u           = JSON.parse(localStorage.getItem('u'));
    const credentials = firebase.auth.EmailAuthProvider.credential(u.email, this.form.value.currentPassword);
    const user        = this.authService.getMe().currentUser;
    user.reauthenticateWithCredential(credentials)
      .then(() => {
        user.updateEmail(this.form.value.email)
          .then(() => {
            this.user.update({
              email: this.form.value.email
            }).then(() => {
              u.email = this.form.value.email;
              console.log(u);
              localStorage.setItem('u', JSON.stringify(u));
              this.emailDone = true;
            }).catch(err => {
              console.log(err);
              this.emailDone = true;
              this.loading   = false;
            });
          })
          .catch((err: any) => {
            console.log(err);
            this.alertService.error(err.message);
            this.emailDone = false;
            this.loading   = false;
            setTimeout(() => this.alertService.clear(), 2500);
          });
      })
      .catch((err: any) => {
        console.log(err);
        this.alertService.error(err.message);
        this.emailDone = false;
        this.loading   = false;
      });
  }

  private updatePassword() {
    const u           = JSON.parse(localStorage.getItem('u'));
    const credentials = firebase.auth.EmailAuthProvider.credential(u.email, this.form.value.currentPassword);
    const user        = this.authService.getMe().currentUser;
    user.reauthenticateWithCredential(credentials)
      .then(() => {
        user.updatePassword(this.form.value.newPassword)
          .then(() => {
            this.passDone = true;
          })
          .catch((error: any) => {
            console.log(error);
            this.passDone = false;
            this.loading  = false;
            this.alertService.error(error.message);
            setTimeout(() => this.alertService.clear(), 2500);
          });
      })
      .catch((err: any) => {
        console.log(err);
        switch (err.code) {
          case 'auth/wrong-password':
            this.alertService.error(err.message);
            setTimeout(() => this.alertService.clear(), 2500);
            this.passDone = true;
            this.loading  = false;
            break;
        }
      });
  }

  back() {
    this._location.back();
  }
}
