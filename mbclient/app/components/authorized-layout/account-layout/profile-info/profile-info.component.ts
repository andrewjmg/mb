import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {

  user: any;
  avatarUrl: string;

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('u'));

    this.accountService.changeLocation({
      location: 'Profile',
      description: 'Manage your profile information'
    });

    this.accountService.getGravatar()
      .subscribe(data => {
        this.avatarUrl = `${data.value}?s=140`;
        console.log(this.avatarUrl);
      });

  }

}
