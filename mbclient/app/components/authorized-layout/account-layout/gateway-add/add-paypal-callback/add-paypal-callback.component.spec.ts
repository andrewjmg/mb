import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaypalCallbackComponent } from './add-paypal-callback.component';

describe('AddPaypalCallbackComponent', () => {
  let component: AddPaypalCallbackComponent;
  let fixture: ComponentFixture<AddPaypalCallbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaypalCallbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaypalCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
