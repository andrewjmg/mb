import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, RequestOptions, Headers} from '@angular/http';
import {GatewayService} from '../../../../../services/gateway.service';
import {AccountService} from '../../../../../services/account.service';
import {AlertService} from '../../../../../shared/alert/_services/alert.service';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-add-paypal-callback',
  templateUrl: './add-paypal-callback.component.html',
  styleUrls: ['./add-paypal-callback.component.css']
})
export class AddPaypalCallbackComponent implements OnInit {

  gatewayName: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: Http,
    private gtwService: GatewayService,
    private accountService: AccountService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.accountService.changeLocation({
      location: 'New PayPal Gateway',
      description: 'Just one more step, give a name to this gateway',
    });

    this.activatedRoute.queryParams.subscribe(query => {
      localStorage.setItem('add_paypal', JSON.stringify({ code: query['code'] }));
    });
  }

  confirm() {

    if (!this.gatewayName || this.gatewayName.length < 4) {
      this.alertService.clear();
      this.alertService.error('The name length must be at leas 4 characters!');
      setTimeout(() => this.alertService.clear(), 2500);
      return false;
    }

    const url     = `${environment.apiUrl}/gateways/paypal/token`;
    const body    = JSON.parse(localStorage.getItem('add_paypal'));
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});

    this.http.post(url, body, options).catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .subscribe(res => {


        const uid          = localStorage.getItem('uid');
        const parsedBddy   = JSON.parse(res._body);
        let id             = parsedBddy.id;
        const name         = this.gatewayName;
        const type         = 'paypal';
        id                 = id.split("/")[id.split("/").length - 1];
        const access_token = parsedBddy.access_token;

        this.gtwService.addGateway(uid, name, id, access_token, type)
          .subscribe((data) => {
            localStorage.setItem('gtw_key', data.key);
            if (name === 'Paypal') {
              localStorage.setItem('paypal_r', '1');
            }
            console.log("We are done");
            this.router.navigate(['/account/gateway/info']);
          });
      });
  }

}
