import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-gateway-add',
  templateUrl: './gateway-add.component.html',
  styleUrls: ['./gateway-add.component.css']
})
export class GatewayAddComponent implements OnInit {

  stripeOAuth: string;
  paypalOAuth: string;

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.accountService.changeLocation({
      location: 'New Gateway',
      description: 'Add a new gateway'
    });

    this.stripeOAuth = environment.stripe.addOAuth;
    this.paypalOAuth = environment.paypal.addOAuth;
  }

  connectStripe() {
    window.location.href = this.stripeOAuth;
  }

  connectPaypal() {
    window.location.href = this.paypalOAuth;
  }

}
