import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, RequestOptions, Headers} from '@angular/http';
import {GatewayService} from '../../../../../services/gateway.service';
import {environment} from '../../../../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {AccountService} from '../../../../../services/account.service';
import {AlertService} from '../../../../../shared/alert/_services/alert.service';

@Component({
  selector: 'app-add-stripe-callback',
  templateUrl: './add-stripe-callback.component.html',
  styleUrls: ['./add-stripe-callback.component.css']
})
export class AddStripeCallbackComponent implements OnInit {

  gatewayName: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: Http,
    private gtwService: GatewayService,
    private accountService: AccountService,
    private alertService: AlertService
    ) {

  }

  ngOnInit() {

    this.accountService.changeLocation({
      location: 'New Stripe Gateway',
      description: 'Just one more step, give a name to this gateway',
    });

    this.activatedRoute.queryParams.subscribe(query => {
      localStorage.setItem('add_stripe', JSON.stringify({ code: query['code'] }));
    });
  }

  confirm() {

    if (!this.gatewayName || this.gatewayName.length < 4) {
      this.alertService.clear();
      this.alertService.error('The name length must be at leas 4 characters!');
      setTimeout(() => this.alertService.clear(), 2500);
      return false;
    }

    const url     = `${environment.apiUrl}/gateways/stripe/token`;
    const body    = JSON.parse(localStorage.getItem('add_stripe'));
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({ headers });

    this.http.post(url, body, options).catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .subscribe(res => {
        const uid          = localStorage.getItem('uid');
        const parsedBddy   = JSON.parse(res._body);
        const name         = this.gatewayName;
        const type         = 'stripe';
        const id           = parsedBddy.id;
        const access_token = parsedBddy.access_token;
        this.gtwService.addGateway(uid, name, id, access_token, type)
          .subscribe((data) => {
            console.log('From Stripe Callback Component', data);
            localStorage.setItem('gtw_key', data.key);
            if (name === 'Stripe') {
              localStorage.setItem('stripe_r', '1');
            }
            this.router.navigate(['/account/gateway/info']);
          });
      });
  }
}
