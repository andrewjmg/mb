import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStripeCallbackComponent } from './add-stripe-callback.component';

describe('AddStripeCallbackComponent', () => {
  let component: AddStripeCallbackComponent;
  let fixture: ComponentFixture<AddStripeCallbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddStripeCallbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStripeCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
