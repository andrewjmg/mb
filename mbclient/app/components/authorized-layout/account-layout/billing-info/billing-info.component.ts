import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';

@Component({
  selector: 'app-billing-info',
  templateUrl: './billing-info.component.html',
  styleUrls: ['./billing-info.component.css']
})
export class BillingInfoComponent implements OnInit {

  subscription: any;
  source: any;
  charges: any;

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.accountService.changeLocation({
      location: 'Billing Information',
      description: 'Payment History and Charge Amount'
    });

    this.accountService.getBillingInformation()
      .subscribe(data => {
        console.log(data);
        this.subscription = data.billing.subscription;
        this.source       = data.billing.source;
        this.charges      = data.billing.charges;
      }, err => {
        console.log(err);
      });
  }

}
