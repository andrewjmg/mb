import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service';
import {FirebaseObjectObservable} from 'angularfire2/database';
import {AlertService} from '../../../../shared/alert/_services/alert.service';

declare var Stripe;

@Component({
  selector: 'app-billing-update',
  templateUrl: './billing-update.component.html',
  styleUrls: ['./billing-update.component.css']
})
export class BillingUpdateComponent implements OnInit {

  sources: any;
  stripe: any;
  cardNumber: any;
  cardExpiry: any;
  cardCVC: any;
  cardError: any;
  customerId: any;
  creatingToken = false;

  constructor(
    private accountService: AccountService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.accountService.changeLocation({
      location: 'Billing Update',
      description: 'Update your payment method'
    });

    this.accountService.getCustomerId()
      .subscribe(data => {
        this.customerId = data.$value;
        this.loadCustomerInfo(this.customerId);
      }, err => {
        console.log(err);
      });

    this.stripe     = Stripe('pk_test_x1FBg0azepCuyAgNeYYfzgFG');
    const elements  = this.stripe.elements();
    this.cardNumber = elements.create('cardNumber');
    this.cardNumber.mount('#cardNumber');
    this.cardNumber.addEventListener('change', (event) => this.cardError = null);

    this.cardCVC = elements.create('cardCvc');
    this.cardCVC.mount('#cardCvc');
    this.cardCVC.addEventListener('change', (event) => this.cardError = null);

    this.cardExpiry = elements.create('cardExpiry');
    this.cardExpiry.mount('#cardExp');
    this.cardExpiry.addEventListener('change', (event) => this.cardError = null);
  }

  loadCustomerInfo(customerId) {
    this.accountService.getCustomerInfo({customerId})
      .subscribe(data => {
        this.sources = data.sources;
      }, err => {
        console.log(err.json());
      });
  }

  clearCard() {
    this.cardNumber.clear();
    this.cardCVC.clear();
    this.cardExpiry.clear();
  }

  submit($event) {
    $event.preventDefault();
    this.creatingToken = !this.creatingToken;
    this.stripe.createToken(this.cardNumber)
      .then( result => {
        if (result.error) {
          this.creatingToken = !this.creatingToken;
          this.alertService.error(result.error.message);
          setTimeout(() => this.alertService.clear(), 2500);
        }else {
          this.accountService.newCard({customerId: this.customerId, token: result.token.id, sources: this.sources.data})
            .subscribe(data => {
              this.creatingToken = !this.creatingToken;
              this.alertService.success('Updated payment method!');
              this.loadCustomerInfo(this.customerId);
              this.clearCard();
              setTimeout(() => this.alertService.clear(), 3000);
            }, err => {
              console.log(err);
              this.alertService.error('Something went wrong, please try later');
              setTimeout(() => this.alertService.clear(), 3000);
              this.clearCard();
            });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

}
