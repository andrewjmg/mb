import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayInfoComponent } from './gateway-info.component';

describe('GatewayInfoComponent', () => {
  let component: GatewayInfoComponent;
  let fixture: ComponentFixture<GatewayInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
