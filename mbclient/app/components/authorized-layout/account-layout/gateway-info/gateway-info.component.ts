import { Component, OnInit } from '@angular/core';
import {GatewayService} from '../../../../services/gateway.service';
import {AccountService} from '../../../../services/account.service';
import {AlertService} from '../../../../shared/alert/_services/alert.service';

declare var $;

@Component({
  selector: 'app-gateway-info',
  templateUrl: './gateway-info.component.html',
  styleUrls: ['./gateway-info.component.css']
})
export class GatewayInfoComponent implements OnInit {

  gateways: any[] = [];
  emptyGateways   = false;

  constructor(
    private accountService: AccountService,
    private gtwService: GatewayService,
    private alertService: AlertService
  ) { }

  ngOnInit() {

    this.accountService.changeLocation({
      location: 'Gateways Info',
      description: 'Active gateways'
    });

    this.loadGateways();
  }

  loadGateways() {
    this.gtwService.getKey().subscribe((data: any[]) => {
      this.emptyGateways = (data.length < 1);
      this.gateways = data;
    });
  }

  removeGateway(key: string) {
    this.gtwService.removeGateway(key)
      .then(() => {
        const index = this.gateways.findIndex(gtw => gtw.gatewayKey === key);

        if (this.gateways.length > 0) {
          if (index >= 0) {
            if ((index - 1) >= 0) {
              localStorage.setItem('gtwKey', this.gateways[index - 1].gatewayKey);
              this.gateways.splice(index, 1);
            }else {
              localStorage.setItem('gtwKey', this.gateways[index + 1].gatewayKey);
              this.gateways.splice(index, 1);
            }
          }
        }else {
          localStorage.removeItem('gtwKey');
        }

        this.gtwService.toggleGateway(true);
        this.alertService.success('Gateway removed!');
        setTimeout(() => this.alertService.clear(), 2500);
      })
      .catch(err => {
        console.log(err);
      })
  }

}
