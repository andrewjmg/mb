import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  loading = false;
  showPhoto = false;
  progressPercentage = 0;

  constructor() { }

  ngOnInit() {
  }

  uploadPhoto() {

    const tout = setTimeout(() => {
      const interval = setInterval(() => {
        if (this.progressPercentage === 100) {
          clearInterval(interval);
          clearTimeout(tout);
          this.showPhoto = !this.showPhoto;
        }else {
          this.progressPercentage += 5;
        }
      }, 500);
    }, 300);

    this.loading = !this.loading;
  }

  reset() {
    this.loading = false;
    this.showPhoto = false;
    this.progressPercentage = 0;
  }

  /*dragover_handler(ev) {
    ev.preventDefault();
    // Set the dropEffect to move
    ev.dataTransfer.dropEffect = 'move';
  }

  drop_handler(ev) {
    ev.preventDefault();
    // Get the id of the target and add the moved element to the target's DOM
    const data = ev.dataTransfer.getData('image');
    console.log(ev.dataTransfer.items);
    // ev.target.appendChild(document.getElementById(data));
  }*/
}
