import { Component, OnInit } from '@angular/core';
import {GatewayService} from '../../../../services/gateway.service';
import {AccountService} from '../../../../services/account.service';
import {AlertService} from '../../../../shared/alert/_services/alert.service';

@Component({
  selector: 'app-gateway-update',
  templateUrl: './gateway-update.component.html',
  styleUrls: ['./gateway-update.component.css']
})
export class GatewayUpdateComponent implements OnInit {

  gateways: any[] = [];
  emptyGateways = false;


  constructor(
    private accountService: AccountService,
    private gtwService: GatewayService,
    private alertService: AlertService,
  ) {
  }

  ngOnInit() {
    this.accountService.changeLocation({
      location: 'Update Gateways',
      description: 'Set to active removed gateways'
    });

    this.loadGateways();
  }

  loadGateways() {
    this.gtwService.getRemovedGateways()
      .subscribe(data => {
        console.log(data);
        this.emptyGateways = (data.length > 0);
        this.gateways      = data;
      }, err => {
        console.log(err);
      });
  }

  activateGateway(key: string) {
    this.gtwService.activateGateway(key)
      .then(() => {
        const index = this.gateways.findIndex(gtw => gtw.$key === key);

        if (index >= 0) {
          if (!localStorage.getItem('gtwKey')) {
            localStorage.setItem('gtwKey', this.gateways[index].$key);
            this.gtwService.toggleGateway(true);
          }

          this.alertService.clear();
          this.alertService.success('Gateway activated!');

          this.gateways.splice(index, 1);
        }

        this.alertService.success('Gateway activated!');
        setTimeout(() => this.alertService.clear(), 2500);
      })
      .catch(err => {
        console.log(err);
      })
  }

}
