import { Component, OnInit, Input } from '@angular/core';
import {ReportService} from '../../../../../../services/report.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

  @Input() report: any;
  @Input() currentRange: any;
  daysRange: number;
  historyData: any[];
  kpiData: any;
  summaryData: any;
  planData: any;

  constructor(
    private reportService: ReportService,

    ) {
    this.reportService.changedRange$.subscribe(data => {
      this.daysRange    = data.range.value;
      this.currentRange = data.range;
      this.loadComponentsData(this.daysRange);
    });

  }

  ngOnInit() {
    this.daysRange = this.currentRange.value;
    this.loadComponentsData(this.daysRange);
  }

  onZoomIn(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange);
  }

  onZoomOut(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange);
  }

  onCustomData(dates) {
    console.log('onCustomData', dates)
    let name = this.report.$key;
    let type = name.split('_')[1];
    type = type.charAt(0).toUpperCase() + type.slice(1);
    name = 'subscription'
    this.reportService.getCustomData(name, dates.from, dates.to, type)
      .subscribe(data => {
        console.log(data);
        this.historyData = data.data.history || [];
        this.kpiData     = data.data.kpi || {};
        this.summaryData = data.data.summary || {};
        this.planData    = data.data.byPlan || [];
      });
  }

  onDeactivateCustomData(should) {
    if (should) {
      this.loadComponentsData(this.daysRange);
    }
  }

  loadComponentsData(range) {
    this.historyData = this.report.history ? this.report['history'][range] : [];
    this.kpiData     = this.report.kpi || {};
    this.summaryData = this.report.summary ? this.report.summary[range] : {};
    this.planData    = this.report.byPlan ? this.report.byPlan[range] : [];
  }

}
