import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LtvComponent } from './ltv.component';

describe('LtvComponent', () => {
  let component: LtvComponent;
  let fixture: ComponentFixture<LtvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LtvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LtvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
