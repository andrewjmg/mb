import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenuechurnComponent } from './revenuechurn.component';

describe('RevenuechurnComponent', () => {
  let component: RevenuechurnComponent;
  let fixture: ComponentFixture<RevenuechurnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevenuechurnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenuechurnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
