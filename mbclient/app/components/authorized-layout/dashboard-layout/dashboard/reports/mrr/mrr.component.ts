import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DragScroll } from 'angular2-drag-scroll';
import {ReportService} from '../../../../../../services/report.service';
import {CapitalizePipe} from '../../../../../../pipes/capitalize.pipe';
import {DashboardService} from '../../../../../../services/dashboard.service';
declare var Chartist;

@Component({
  selector: 'app-mrr',
  templateUrl: './mrr.component.html',
  styleUrls: ['./mrr.component.css']
})
export class MrrComponent implements OnInit {
  @ViewChild('nav', {read: DragScroll}) ds: DragScroll;
  @Input() report: any;
  @Input() currentRange: any;
  stackBarChart: any;
  stackBarLabels: string[] = [];
  stackBarSeries: any      = [];
  disableBtnLeft           = false;
  disableBtnRight          = false;
  applyFade                = false;
  daysRange: number;
  historyData: any[];
  kpiData: any;
  summaryData: any;
  planData: any;
  breakdownTableData: any[];
  breakdownChartData: any;

  constructor(
    private reportService: ReportService,
    private capitalize: CapitalizePipe,
    private dashboardService: DashboardService
  ) {
    this.reportService.changedRange$.subscribe(data => {
      this.daysRange    = data.range.value;
      this.currentRange = data.range;
      // this.updateByRange();
      this.loadComponentsData(this.daysRange);
    });

    this.dashboardService.onChangeSize
      .subscribe((should: boolean) => {
        // this.updateByRange();
        this.loadComponentsData(this.daysRange);
      });
  }

  ngOnInit() {
    // this.cleanVariables();
    this.daysRange = this.currentRange.value;
   /* const {table}  = this.getChartsData();
    this.setupStackBarSeries(table);*/
    this.loadComponentsData(this.daysRange);
    // this.loadStackedBarChart();

  }

  updateByRange() {
    this.cleanVariables();
    const {table} = this.getChartsData();
    this.setupStackBarSeries(table);
    this.stackBarChart.update({labels: this.stackBarLabels, series: this.stackBarSeries});
    this.loadComponentsData(this.daysRange)
  }

  getChartsData() {
    const {report, daysRange} = this;
    return {
      table: report['growth'],
      kpi: report['kpi'],
      byPlan: report.byPlan ? report['byPlan'][daysRange] : []
    };
  }


  setupStackBarSeries(table) {
    this.stackBarLabels = [];
    this.stackBarSeries = [];
    table.map(data => {
      const date = new Date(data.date);
      if (!this.stackBarLabels.find (x => x === data.month + data.year)) {
        this.stackBarLabels.push(data.month + data.year);
      }

      let position = 0;
      for (const val in data) {
        if (val !== 'month' && val !== 'year') {
          if (this.stackBarSeries[position] == null) {
            this.stackBarSeries[position] = [];
            this.stackBarSeries[position].push({
              meta: this.capitalize.transform(val),
              value: (data[val] / 100)
            });
          } else {
            this.stackBarSeries[position].push({
              meta: this.capitalize.transform(val),
              value: (data[val] / 100)
            });
          }
          position++;
        }
      }
    });
  }



  loadStackedBarChart() {
    this.stackBarChart = new Chartist.Bar('.ct-chart-bar', {
      labels: this.stackBarLabels,
      series: this.stackBarSeries,
    }, {
      stackBars: true,
      axisY: {
        labelInterpolationFnc: (value) => {
          return value >= 0
            ? `$${value}`
            : `-$${(value*-1)}`
        }
      },
      plugins: [
        Chartist.plugins.tooltip({
          class: 'tooltip-bg-mb',
          appendToBody: true,
          transformTooltipTextFnc: value => {
            if (value >= 0) {
              return `$${value}`
            } else {
              return `-$${(value*-1)}`
            }
          }
        })
      ],
      height: '250px',
      width: '100%'
    }).on('draw', data => {
      if (data.type === 'bar') {
        let style = 'stroke-width: 75px;';
        let { meta } = data;
        if (meta === 'New') {
          style += 'stroke: #2CBF6D;';
        } else if (meta === 'Churn') {
          style += 'stroke: #E16070;';
        } else if (meta === 'Contraction') {
          style += 'stroke: #EFC663;';
        } else if (meta === 'Expansion') {
          style += 'stroke: #7049A3;';
        } else if (meta === 'Change') {
          style += 'stroke: #41B7CC;';
        }
        data.element.attr({style});
      }
    });
  }


  cleanVariables() {
    this.stackBarLabels = [];
    this.stackBarSeries = [];
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  leftBoundStatus(reached: boolean) {
    this.disableBtnLeft = reached;
    this.applyFade      = reached;
  }

  rightBoundStatus(reached: boolean) {
     this.disableBtnRight = reached;
     this.applyFade       = !reached;
  }

  onZoomIn(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    // this.updateByRange();
    this.loadComponentsData(this.daysRange)
  }

  onZoomOut(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    // this.updateByRange();
    this.loadComponentsData(this.daysRange)
  }

  onCustomData(dates) {
    console.log(dates);
    this.reportService.getCustomData(this.report.$key, dates.from, dates.to)
      .subscribe(data => {
        console.log(data);
        this.historyData        = data.data.history || [];
        this.kpiData            = data.data.kpi || {};
        this.summaryData        = data.data.summary || {};
        this.planData           = data.data.byPlan || [];
        this.breakdownTableData = data.data.table || [];
        this.breakdownChartData = data.data.growth || [];
        this.setupStackBarSeries(this.breakdownChartData);
        this.loadStackedBarChart();
      });
  }

  onDeactivateCustomData(should) {
    if (should) {
      this.loadComponentsData(this.daysRange);
    }
  }

  loadComponentsData(range) {
    this.historyData        = this.report.history ? this.report['history'][range] : [];
    this.kpiData            = this.report.kpi || {};
    this.summaryData        = this.report.summary ? this.report.summary[range] : {};
    this.planData           = this.report.byPlan ? this.report.byPlan[range] : [];
    this.breakdownTableData = this.report.table ? this.report.table[range] : [];
    this.breakdownChartData = this.report.growth || [];
    this.setupStackBarSeries(this.breakdownChartData);
    this.loadStackedBarChart();
  }


}
