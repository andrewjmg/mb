import { Component, OnInit, Input } from '@angular/core';
import {ReportService} from '../../../../../../services/report.service';

@Component({
  selector: 'app-arpu',
  templateUrl: './arpu.component.html',
  styleUrls: ['./arpu.component.css']
})
export class ArpuComponent implements OnInit {
  @Input() report: any;
  @Input() currentRange: any;

  daysRange: number;
  historyData: any[];
  kpiData: any;
  summaryData: any;
  planData: any;


  constructor(
    private reportService: ReportService,
  ) {
    this.reportService.changedRange$.subscribe(data => {
      this.daysRange    = data.range.value;
      this.currentRange = data.range;
      this.loadComponentsData(this.daysRange);
    });
  }

  ngOnInit() {
    this.daysRange   = this.currentRange.value;
    this.loadComponentsData(this.daysRange)
  }

  onZoomIn(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange);
  }

  onZoomOut(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange);
  }

  onCustomData(dates) {
    this.reportService.getCustomData(this.report.$key, dates.from, dates.to)
      .subscribe(data => {
        console.log(data);
        this.historyData = data.data.history || [];
        this.kpiData     = data.data.kpi || {};
        this.summaryData = data.data.summary || {};
        this.planData    = data.data.byPlan || [];
      });
  }

  onDeactivateCustomData(should) {
    if (should) {
      this.loadComponentsData(this.daysRange);
    }
  }

  loadComponentsData(range) {
    this.historyData = this.report.history ? this.report['history'][range] : [];
    this.kpiData     = this.report.kpi || {};
    this.summaryData = this.report.summary ? this.report.summary[range] : {};
    this.planData    = this.report.byPlan ? this.report.byPlan[range] : [];
  }

}
