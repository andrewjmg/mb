import { Component, OnInit, Input } from '@angular/core';
import {ReportService} from '../../../../../../services/report.service';

declare var Chartist;

@Component({
  selector: 'app-arr',
  templateUrl: './arr.component.html',
  styleUrls: ['./arr.component.css']
})
export class ArrComponent implements OnInit {

  @Input() report: any;
  @Input() currentRange: any;
  daysRange: number;
  historyData: any[];
  summaryData: any;
  kpiData: any;
  planData: any

  constructor(
    private reportService: ReportService,
    ) {
    this.reportService.changedRange$.subscribe(data => {
      this.daysRange    = data.range.value;
      this.currentRange = data.range;
      this.loadComponentsData(this.daysRange)
    });

  }

  ngOnInit() {
    this.daysRange   = this.currentRange.value;
    this.loadComponentsData(this.daysRange)
  }

  onZoomIn(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange)
  }

  onZoomOut(range: any) {
    this.daysRange    = range.value;
    this.currentRange = range;
    this.loadComponentsData(this.daysRange)
  }

  onCustomData(dates) {
    this.reportService.getCustomData('mrr', dates.from, dates.to)
      .subscribe(data => {
        console.log(data);
        data             = data.data.arrReport;
        this.historyData = data.history || [];
        this.kpiData     = data.kpi || {};
        this.summaryData = data.summary || {};
        this.planData    = data.byPlan || [];
      });
  }

  onDeactivateCustomData(should) {
    if (should) {
      this.loadComponentsData(this.daysRange);
    }
  }

  loadComponentsData(range) {
    this.historyData = this.report.history ? this.report['history'][range] : [];
    this.kpiData     = this.report.kpi || {};
    this.summaryData = this.report.summary ? this.report.summary[range] : {};
    this.planData    = this.report.byPlan ? this.report.byPlan[range] : [];
  }


}
