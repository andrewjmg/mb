import { Component, OnInit, Input } from '@angular/core';
import {ReportService} from '../../../../../../services/report.service';

@Component({
  selector: 'app-fee',
  templateUrl: './fee.component.html',
  styleUrls: ['./fee.component.css']
})
export class FeeComponent implements OnInit {
  /*
   * Data received from parent (StatisticsComponent)
   */
  @Input() report: any;
  @Input() currentRange: any;

  /*
   * Variables which will be passed to each component (header, history, kpi)
   */
  daysRange: number;
  historyData: any[];
  kpiData: any;
  summaryData: any;
  // planData: any;


  constructor(
    private reportService: ReportService,
  ) {
    /*
     * Observable to listen range change in the dashboard
     */
    this.reportService.changedRange$.subscribe(data => {
      /*
       * Retrieves range object
       */
      this.daysRange    = data.range.value;
      this.currentRange = data.range;

      /*
       * Update data
       */
      this.loadComponentsData(this.daysRange);
    });
  }

  /*
   * Initializing daysRange variable (attribute) with currentRage which is passed from the parent component (StatisticsComponent)
   */
  ngOnInit() {
    this.daysRange   = this.currentRange.value;

    /*
     * Load Data
     */
    this.loadComponentsData(this.daysRange)
  }

  /*
   * Function onZoomIn, it'll be triggered when the <app-report-header> component emits event onZoomIn.
   */
  onZoomIn(range: any) {
    /*
     * Update range
     */
    this.daysRange    = range.value;
    this.currentRange = range;

    /*
     * Update data
     */
    this.loadComponentsData(this.daysRange);
  }

  /*
   * Function onZoomOut, it'll be triggered when the <app-report-header> component emits event onZoomOut
   */
  onZoomOut(range: any) {
    /*
     * Update range
     */
    this.daysRange    = range.value;
    this.currentRange = range;

    /*
     * Update data
     */
    this.loadComponentsData(this.daysRange);
  }

  /*
   * Function onCustomData, it'll be triggered when the <app-report-header> component emits event onCustomData
   */
  onCustomData(dates) {
    /*
     * Service (function) to retrieve data from Custom Dates,
     */
    this.reportService.getCustomData(this.report.$key, dates.from, dates.to)
      .subscribe(data => {
        console.log(data);
        /*
         * Assign retrieved data from service to each variable in this component.
         * Each time the variables are updated, the inner components will be updated as well.
         */
        this.historyData = data.data.history || [];
        this.kpiData     = data.data.kpi || {};
        this.summaryData = data.data.summary || {};
        // this.planData    = data.data.byPlan || [];
      });
  }

  /*
   * Function onDeactivateCustomData, it'll be triggered when the <app-report-header> component emits event onDeactivateCustomData
   */
  onDeactivateCustomData(should) {
    if (should) {
      /*
       * Just update data
       */
      this.loadComponentsData(this.daysRange);
    }
  }

  /*
   * Function to assign data to the variables of this component (historyData, kpiData, summaryData).
   * The assigned data depends on the range.
   * Data comes from the report passed by the paren (StatisticsComponent)
   */
  loadComponentsData(range) {
    this.reportService.verifyReport(this.report);
    console.log(this.report);
    this.historyData = this.report.history ? this.report['history'][range] : [];
    this.kpiData     = this.report.kpi || {};
    this.summaryData = this.report.summary ? this.report.summary[range] : {};
    // this.planData    = this.report.byPlan ? this.report.byPlan[range] : [];
  }

}
