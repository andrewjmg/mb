import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {dashboard} from '../../../../../shared/constants/dashboard';
import 'rxjs/Rx';
import {ReportService} from "../../../../../services/report.service";
import {GatewayService} from "../../../../../services/gateway.service";
import {environment} from "../../../../../../environments/environment";
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-kpi-dash',
  templateUrl: './kpi-dash.component.html',
  styleUrls: ['./kpi-dash.component.css']
})
export class KpiDashComponent implements OnInit {

  @Output() onChangeSize  = new EventEmitter<boolean>();

  reports: any[]       = [];
  staticReports: any[] = [];
  cardsOrder: any[]    = [];
  loadedReports        = false;
  toggleSize           = false;
  showReport           = false;
  showDropdown         = false;
  time_ranges          = dashboard.time_ranges;
  labels               = dashboard.card_labels;
  selected_range: any;
  selectedReport: any;

  constructor(
    private gtwService: GatewayService,
    private reportService: ReportService,
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private dragulaService: DragulaService) {
    this.reportService.showReport$.subscribe(data => {
      this.showReport = data.show;
    });
    this.selected_range = this.time_ranges[0];

    /*
     * Subscribing to Dragula Service, to know when an element has been dropped (changed of place).
     */
    dragulaService.drop.subscribe(data => {
      const nodes = Array.from(data[2].childNodes);
      this.cardsOrder = [];
      nodes.forEach(node => {
        if (node['id'] != null) {
          this.cardsOrder.push(node['id']);
        }
      });

      const uid = localStorage.getItem('uid');

      this.db.object(`/users/${uid}`)
        .update({order: this.cardsOrder})
        .then(() => {
          console.log('ordered');
        })
        .catch(err => {
          console.log(err);
        });
    }, err => {
      console.log(err);
    });

    this.reportService.showReport$.subscribe(data => {
      if (!data.show) {
        this.selectedReport = null;
      }
    });

    this.gtwService.realoadGateway$.subscribe(data => {
      this.reports = [];
      if (data) this.loadReports(this.reports);
    });

  }

  ngOnInit() {

    /*
     * Setting up currency
     */
    const gtwKey    = localStorage.getItem('gtwKey');
    const gtwObject = this.db.object(`/gateways/${gtwKey}/currency`);
    gtwObject.subscribe(data => {
      localStorage.setItem('currency', JSON.stringify({code: data.code, symbol: data.symbol}));
    }, err => {
      console.log(err);
    });

    setTimeout(() => this.loadReports(this.reports), 500);

    // this.loadReports(this.reports);

    /*
     * If user is signed in set this.user to it.
     */
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.db.list(`/users/${user.uid}/order`)
          .subscribe(data => {
            this.cardsOrder = data;
            if (this.cardsOrder.length > 0) {
              const interval = setInterval(() => {
                if (this.reports.length === this.cardsOrder.length) {
                  this.orderReports();
                  clearInterval(interval);
                }
              }, 100);
            }

          }, error => {
            console.log(error);
          });
      }
    });
  }

  loadReports(reportsArray: any[], range = null) {
    const uid    = localStorage.getItem('uid');
    this.db.object(`/users/${uid}/order`)
      .subscribe(data => {
        this.reportService.getReportNames(data.length > 0 ? data : null)
          .subscribe(reps => {
            this.loadRevenueReports(reportsArray, reps, range);
          });

      }, error => console.log(error));
  }


  loadRevenueReports(reportsArray: any[], reports: any[], range) {
    reports.map(report => {
      this.reportService.getRevenueReport(report.key, range = null ? range.value : this.selected_range.value)
        .subscribe(revenue => {
          this.reportService.getPercentageReport(report.key)
            .subscribe(percentage => {
              report.total = revenue.total;
              report.count = revenue.count;
              report.percentageChange = percentage.$value;
              const index = reportsArray.findIndex(r => r.key === report.key);

              if (index >= 0) {
                reportsArray[index] = report;
              }else {
                reportsArray.push(report);
              }
              this.loadedReports = true;
            });
        })
    });
  }

  orderReports() {
    let tempReport;
    for (let i = 0; i < this.cardsOrder.length; i++) {
      const index = this.reports.findIndex(report => {
        return report.key === this.cardsOrder[i]['$value']
      });
      if (index !== i && index > 0) {
        tempReport          = this.reports[i];
        this.reports[i]     = this.reports[index];
        this.reports[index] = tempReport;
      }
    }
  }

  changeSize() {
    this.toggleSize = !this.toggleSize;
    this.onChangeSize.emit(this.toggleSize);
  }

  toggleShow(rep) {
    // if (!(this.selectedReport === rep)) {}
    let firstTime;
    this.reportService.getSelectedReport(rep)
      .subscribe(report => {

        if (this.selectedReport === rep) {
          // firstTime = false;
          firstTime = true;
        }else {
          firstTime = true;
          this.selectedReport = rep;
        }
        console.log(report)
        this.reportService.toggleShowReport({
          show: true,
          report: report,
          range: this.selected_range,
          firstTime
        });
      });
  }

  changeRange($event, range) {
    $event.preventDefault();
    console.log(this.reports);
    this.selected_range = range;
    this.showDropdown = !this.showDropdown;
    if (this.showReport) {
      this.reportService.changeRange({range})
    }

    this.reports.map(rep => {
      this.reportService.getRevenueReport(rep.key, range = null ? range.value : this.selected_range.value)
        .subscribe(revenue => {
          this.reportService.getPercentageReport(rep.key)
            .subscribe(percentage => {
              rep.total = revenue.total;
              rep.percentageChange = percentage.$value;
              const index = this.reports.findIndex(r => r.key === rep.key);

              if (index >= 0) {
                this.reports[index] = rep;
              }else {
                this.reports.push(rep);
              }
            });
        })
    });

  }

}
