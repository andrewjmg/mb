import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {dashboard} from '../../../../../../shared/constants/dashboard';
import {ReportService} from "../../../../../../services/report.service";
import {NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-report-header',
  templateUrl: './report-header.component.html',
  styleUrls: ['./report-header.component.css']
})
export class ReportHeaderComponent implements OnInit {

  @Input() title: string;
  @Input() summary: number;
  @Input() currentRange: any;
  @Output() onZoomIn               = new EventEmitter<any>();
  @Output() onZoomOut              = new EventEmitter<any>();
  @Output() onCustomData           = new EventEmitter<any>();
  @Output() onDeactivateCustomData = new EventEmitter<any>();

  dateFrom: any;
  dateTo: any;
  report: any;
  customDataActivated = false;

  constructor(private reportService: ReportService, config: NgbDatepickerConfig) {
    this.reportService.changedRange$
      .subscribe(data => {
        this.currentRange = data.range;
      });

    const currentDate  = new Date();
    config.maxDate     = { year:  currentDate.getFullYear(), month: currentDate.getMonth() + 1, day: currentDate.getDate()};
    config.outsideDays = 'hidden';
  }

  ngOnInit() {
    this.report = dashboard.card_labels[this.title];
  }

  zoomIn() {
    let index = dashboard.time_ranges.findIndex(obj => obj.id === this.currentRange.id);
    if ((++index) <= (dashboard.time_ranges.length - 1)) {
        this.onZoomIn.emit(dashboard.time_ranges[index]);
    }

    this.deactivateCustomData();

  }

  zoomOut() {
    let index = dashboard.time_ranges.findIndex(obj => obj.id === this.currentRange.id);
    if ((--index) >= 0) {
        this.onZoomOut.emit(dashboard.time_ranges[index]);
    }

    this.deactivateCustomData()
  }

  generateCustomDate() {

    if (this.dateFrom && this.dateTo) {
      const {year: yearFrom, month: monthFrom, day: dayFrom} = this.dateFrom;
      const {year: yearTo, month: monthTo, day: dayTo} = this.dateTo;
      const from = new Date(yearFrom, monthFrom-1, dayFrom).getTime() / 1000;
      const to = new Date(yearTo, monthTo-1, dayTo).getTime() / 1000;
      this.customDataActivated = true;
      this.onCustomData.emit({from, to});
    }

  }

  deactivateCustomData() {
    if (this.customDataActivated) {
      this.customDataActivated = false;
      this.dateFrom            = null;
      this.dateTo              = null;
      this.onDeactivateCustomData.emit(true);
    }
  }



}
