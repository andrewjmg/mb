import {Component, Input, OnInit, OnChanges} from '@angular/core';
import * as moment from 'moment';
import * as extrapolation from 'extrapolate';
import {MbCurrencyPipe} from '../../../../../../pipes/mb-currency.pipe';
import {GatewayService} from '../../../../../../services/gateway.service';
import {ReportService} from '../../../../../../services/report.service';
import {AlertService} from '../../../../../../shared/alert/_services/alert.service';
import {DashboardService} from '../../../../../../services/dashboard.service';

declare var Chartist;

@Component({
  selector: 'app-history-chart',
  templateUrl: './history-chart.component.html',
  styleUrls: ['./history-chart.component.css']
})
export class HistoryChartComponent implements OnInit, OnChanges {

  @Input() history;
  @Input() reportName;
  @Input() range;

  lineChart: any;
  historyLabels: string[] = [];
  historySeries: any[]    = [];
  gateways: any[];
  gtwKeyCompare: string;
  gtwComparing: any;

  constructor(
    private currency: MbCurrencyPipe,
    private gtwService: GatewayService,
    private reportService: ReportService,
    private alertService: AlertService,
    private dashboardService: DashboardService
  ) {

    this.dashboardService.onChangeSize
      .subscribe((should: boolean) => {
        this.setupHistorySeries(this.history);
        this.linearExtrap();
      });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log(this.history);
    if (!this.history || this.history.length <= 1) {
      this.historySeries = [];
      this.historyLabels = [];
      this.loadLineChart();
      return false;
    }

    this.setupHistorySeries(this.history);
    this.linearExtrap();
    this.loadGateways();
  }

  loadGateways() {
    this.gtwService.getKey()
      .subscribe((data: any[]) => {
        const currentGtw = localStorage.getItem('gtwKey');
        const index      = data.findIndex(gtw =>  gtw.$key === currentGtw);

        if (index >= 0) {
          data.splice(index, 1);
        }

        this.gateways = data;
      });
  }

  setupHistorySeries(history) {
    this.gtwKeyCompare = null;
    this.gtwComparing  = null;
    this.historySeries = [{data: []}];
    this.historySeries[0].data = this.buildHistoryDataSeries(history);
  }

  buildHistoryDataSeries(data) {
    return data.map((obj, index) => {


      if (index === 0 || index === data.length - 1) {
        return {
          x: new Date(obj.x),
          y: obj.y
        };
      }else {
        if (data[index - 1].y === obj.y) {
          return {
            meta: {'change': false},
            x: new Date(obj.x),
            y: obj.y
          };
        }else {
          return {
            x: new Date(obj.x),
            y: obj.y
          };
        }
      }

    });
  }

  linearExtrap() {

    const data = this.historySeries[0].data;

    const linear       = new extrapolation();
    const extraDays    = [];
    const forecastData = [];
    data.map((obj) => {
      const x = moment(obj.x).valueOf();
      const y = obj.y;
      linear.given(x, y);
    });

    let date = data[data.length - 1].x;

    for (let i = 0; i < 3; i++) {
      date = moment(date).add(1, 'days').valueOf();
      extraDays.push(date);
    }

    forecastData.push({
      // meta: {'change': false},
      x: new Date(data[data.length - 1].x),
      y: data[data.length - 1].y
    });

    extraDays.map((time, index) => {
      if (index < 2) {
        forecastData.push({
          meta: {'change': false},
          x: new Date(time),
          y: linear.getLinear(time)
        });
      }else {
        forecastData.push({
          x: new Date(time),
          y: linear.getLinear(time)
        });
      }
    });

    this.historySeries.push({className: 'forecast-line', data: forecastData});
    this.loadLineChart();
  }

  compare(key: string) {
    this.gtwKeyCompare = key;
    this.setupComparison();
  }

  setupComparison() {
    this.reportService.toCompareWith(this.gtwKeyCompare, this.reportName, this.range)
      .subscribe(data => {
        if (data.length > 0) {
          this.gtwComparing = this.gateways.find(gtw => gtw.gatewayKey === this.gtwKeyCompare );
          this.historySeries.push({data: this.buildHistoryDataSeries(data)});
          this.loadLineChart();
        }else {
          this.alertService.clear();
          this.alertService.warn('Nothing to compare');
          setTimeout(() => this.alertService.clear(), 2500);
        }
      });
  }

  closeComparison() {
    this.setupHistorySeries(this.history);
    this.linearExtrap();
  }

  loadLineChart() {
    this.lineChart = new Chartist.Line('.ct-chart', {
      labels: this.historyLabels,
      series: this.historySeries
    }, {
      showArea: true,
      fullWidth: true,
      lineSmooth: Chartist.Interpolation.simple({
        fillHoles: false,
        showPoints: false
      }),
      axisX: {
        type: Chartist.FixedScaleAxis,
        divisor: 6,
        labelInterpolationFnc: (value) => {
          return moment(value).format('MMM D');
        }
      },
      axisY: {
        labelInterpolationFnc: (value) => {
          return this.setAxisY(value);
        }
      },
      plugins: [
        Chartist.plugins.tooltip({
          appendToBody: false,
          transformTooltipTextFnc: val => {
            val = val.split(',')[1];
            return this.setChartToolTip(val);
          }
        })
      ],
      height: '250px',
    }).on('draw', function (data) {
      if (data.type === 'point' && data.meta) {
        data.element.attr({
          style: 'display: none'
        });
      }
    });
  }

  setChartToolTip(val) {
    const {reportName} = this;
    if (this.reportName === 'churn' || this.reportName === 'revenuechurn') {
      return val + '%';
    } else if (reportName.includes('subscription') || reportName === 'transaction_fail') {
      return val;
    } else {
      return this.currency.transform(val);
    }
  }

  setAxisY(val) {
    const {reportName} = this;
    if (this.reportName === 'churn' || this.reportName === 'revenuechurn') {
      return val >= 0 ? `${val}%` : `-${(val * -1)}%`
    } else if (reportName.includes('subscription') || reportName === 'transaction_fail') {
      return val;
    } else {
      return this.currency.transform(val);
    }
  }

}
