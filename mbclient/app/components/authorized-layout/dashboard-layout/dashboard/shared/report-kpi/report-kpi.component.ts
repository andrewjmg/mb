import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-report-kpi',
  templateUrl: './report-kpi.component.html',
  styleUrls: ['./report-kpi.component.css']
})
export class ReportKpiComponent implements OnInit {

  @Input() report: string;
  @Input() total: any;
  @Input() percentage: any;
  @Input() timeRange: string;
  @Input() currency: boolean;

  constructor() { }

  ngOnInit() {
  }

}
