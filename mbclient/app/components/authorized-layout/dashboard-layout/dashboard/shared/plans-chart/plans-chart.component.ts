import { Component, OnInit, OnChanges, Input } from '@angular/core';

declare var Chartist;

@Component({
  selector: 'app-plans-chart',
  templateUrl: './plans-chart.component.html',
  styleUrls: ['./plans-chart.component.css']
})
export class PlansChartComponent implements OnInit, OnChanges {

  @Input() byPlan;
  @Input() reportName;
  @Input() range;

  pieChartLabels: string[] = [];
  pieChartSeries: any[]    = [];
  pieChart: any;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.pieChartLabels = [];
    this.pieChartSeries = [];

    if (this.byPlan && this.byPlan.length > 0) {
      this.setupPieChartSeries(this.byPlan);
    }

    this.loadPieChart();
  }

  setupPieChartSeries(byPlan) {
    byPlan.map(data => {
      this.pieChartLabels.push(data.percentage.toFixed(2));
      this.pieChartSeries.push({
        meta: data.planName,
        value: data.percentage.toFixed(2)
      });
    });
  }

  loadPieChart() {

    const data = {
      labels: this.pieChartLabels,
      series: this.pieChartSeries
    };

    const responsiveOptions = [
      ['screen and (min-width: 1200px)', {
        chartPadding: 30,
        labelOffset: 10,
        labelDirection: 'explode',
        /*labelInterpolationFnc: function(value) {
          return value + '%';
        }*/
      }],
      ['screen and (min-width: 1024px)', {
        labelOffset: 40,
        chartPadding: 20
      }]
    ];

    this.pieChart = new Chartist.Pie('.ct-slice-pie', data, {
      labelInterpolationFnc: (value) => {
        return value + '%';
      },
      plugins: [
        Chartist.plugins.tooltip({
          transformTooltipTextFnc: (data, foo) => {
            return data + '%';
          }
        })
      ]
    }, responsiveOptions);
  }

}
