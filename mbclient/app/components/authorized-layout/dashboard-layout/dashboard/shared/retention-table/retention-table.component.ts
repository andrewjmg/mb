import { Component, OnInit, Input } from '@angular/core';

declare var $;

@Component({
  selector: 'app-retention-table',
  templateUrl: './retention-table.component.html',
  styleUrls: ['./retention-table.component.css']
})
export class RetentionTableComponent implements OnInit {

  @Input() table;
  @Input() reportName;
  @Input() range;

  percentages: any[] = [];
  minPercentage: number;
  maxPercentage: number;

  constructor() { }

  ngOnInit() {
    this.setUpRetentionPercentages();
  }

  setUpRetentionPercentages() {
    this.table.forEach(d => {
      d.values.forEach(val => this.percentages.push(val));
    });

    this.minPercentage = this.percentages.reduce((a, b) => Math.min(a, b));
    this.maxPercentage = this.percentages.reduce((a, b) => Math.max(a, b));
  }


  assignHover($event) {

    const node            = $event.target;
    const xy              = node.classList[1];
    const regex           = /y(\d+)x(\d+)/;
    let [match, y, x]:any = regex.exec(xy);
    y                     = Number(y);
    x                     = Number(x);

    for (let i = y; i >= 0; i--) {
      $(`.y${i}x${x}`).addClass('hover-bg');
    }

    for (let j = x; j >= 0; j--) {
      $(`.y${y}x${j}`).addClass('hover-bg');
    }
  }

  detachHover($event) {
    $('.colored').removeClass('hover-bg');
  }

}
