import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionTableComponent } from './retention-table.component';

describe('RetentionTableComponent', () => {
  let component: RetentionTableComponent;
  let fixture: ComponentFixture<RetentionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetentionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
