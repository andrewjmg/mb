import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsDashComponent } from './statistics-dash.component';

describe('StatisticsDashComponent', () => {
  let component: StatisticsDashComponent;
  let fixture: ComponentFixture<StatisticsDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
