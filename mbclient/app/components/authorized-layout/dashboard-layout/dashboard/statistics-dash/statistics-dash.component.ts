import {Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef} from '@angular/core';
import {ReportService} from "../../../../../services/report.service";
import {Background} from '../../../../../_animations/background.service';
import {BackgroundAnimation} from '../../../../../_animations/background.animation';
import {dashboard} from '../../../../../shared/constants/dashboard';
import {AngularFireDatabase} from 'angularfire2/database';
import {GatewayService} from '../../../../../services/gateway.service';
import {Router} from '@angular/router';
declare var Chartist;
declare var Hammer;

@Component({
  selector: 'app-statistics-dash',
  templateUrl: './statistics-dash.component.html',
  styleUrls: ['./statistics-dash.component.css'],
  animations: BackgroundAnimation
})
export class StatisticsDashComponent implements OnInit, AfterViewInit {

  @ViewChild('hammerEl') hammerEl: ElementRef;

  toPrint: any;
  report: any;
  range: any;
  reports: any[] = [];
  selectedReport: any;
  generatingPdf = false;
  createdPdf    = false;
  errorPdf      = false;
  urlPdf: string;

  hammerConf: any;

  backgroundService: Background = new Background;

  constructor(
    private reportService: ReportService,
    private db: AngularFireDatabase,
    private gtwService: GatewayService,
    private router: Router
  ) {
    this.loadReports();
    this.selectedReport = 'messageDash';
    const time          = new Date();
    const currentTime   = time.getTime();
    this.reportService.showReport$
    .subscribe(data => {
      console.log(data);
      const finalTime = time.getTime();
      if (data.firstTime) {
        this.selectedReport = 'spinner';
        const rep           = data.report.$key;
        console.log('rep', rep)
        if (rep) {
          setTimeout(() => {
            this.selectedReport = rep;
            this.report         = data.report;
            this.range          = data.range;
          }, 500 - (finalTime - currentTime));
        }

      }else {

        this.backgroundService.toggleState();
        setTimeout(() => {
          this.backgroundService.toggleState();
        }, 500);

        this.report = data.report;
        this.range  = data.range;
      }
    });

    this.gtwService.realoadGateway$.subscribe(data => {
      if (data) this.selectedReport = 'messageDash';
    })
  }



  ngOnInit() {
    this.hammerConf = new Hammer.Manager(this.hammerEl.nativeElement);

    // Tap recognizer with minimal 2 taps
    this.hammerConf.add( new Hammer.Tap({ event: 'doubletap', taps: 2 }) );
    // Single tap recognizer
    this.hammerConf.add( new Hammer.Tap({ event: 'singletap' }) );

    /*
     * check hammer.js api documentation for direction values
     * https://hammerjs.github.io/api/
     */
    this.hammerConf.add( new Hammer.Swipe({event: 'swipeleft', direction: 2}));
    this.hammerConf.add( new Hammer.Swipe({event: 'swiperight', direction: 4}));
    this.hammerConf.get('doubletap').recognizeWith('singletap');
    this.hammerConf.get('singletap').requireFailure('doubletap');
  }

  ngAfterViewInit() {
    this.hammerConf.on("doubletap", () => {
      if (window.innerWidth <= 767) {
        this.toDashboard();
      }
    });

    this.hammerConf.on('swipeleft', () => {
      if (window.innerWidth <= 767) {
        this.nextReport();
      }
    });

    this.hammerConf.on('swiperight', () => {
      if (window.innerWidth <= 767) {
        this.lastReport();
      }
    });
  }


  exportReport() {
    this.generatingPdf = !this.generatingPdf;
    this.reportService.exportToPDF({report: this.report, range: this.range.value, reportName: this.report.$key})
      .subscribe(data => {
        const resp      = JSON.parse(data.body);
        this.urlPdf     = resp.image;
        this.createdPdf = !this.createdPdf;
      }, err => {
        console.log(err);
        this.errorPdf = !this.errorPdf;
        setTimeout(() => {
          this.generatingPdf = false;
          this.createdPdf    = false;
          this.errorPdf      = false
        }, 3000);
      });
  }

  openPdf() {
    window.open(this.urlPdf, '_blank');
    this.generatingPdf = false;
    this.createdPdf    = false;
    this.errorPdf      = false;
    this.urlPdf        = null;
  }

  loadReports() {
    const uid = localStorage.getItem('uid');
    this.db.object(`/users/${uid}/order`)
    .subscribe(data => {
      if (data.length > 0) {
        this.reports = data;
        this.reports.splice(0, 0, 'messageDash');
      }else {
        this.reports = Object.keys(dashboard.card_labels);
        this.reports.splice(0, 0, 'messageDash');
      }
    }, error => console.log(error));
  }

  lastReport() {
    const index = this.reports.findIndex(rep => this.selectedReport === rep);

    if (index > 0) {
      const rep = this.reports[index - 1];
      this.getReport(rep);

    }else if (index === 0) {
      this.selectedReport = 'spinner';
      setTimeout(() => {
        this.selectedReport = 'messageDash';
        this.report = null;
      }, 500);
    }
  }

  nextReport() {
    const index = this.reports.findIndex(rep => this.selectedReport === rep);

    if (index < this.reports.length - 1) {
      const rep = this.reports[index  + 1];
      this.getReport(rep);
    }
  }

  closeReport() {
    console.log('closed');
  }

  toDashboard() {
    this.reportService.goToDashboard(true);
  }

  getReport(rep) {
    const time          = new Date();
    const currentTime   = time.getTime();
    this.reportService.getSelectedReport(rep)
    .subscribe(data => {
      const finalTime     = time.getTime();
      this.selectedReport = 'spinner';
      setTimeout(() => {
        this.selectedReport = rep;
        this.report         = data;
        console.log(data);
      }, 500 - (finalTime - currentTime));

    }, error =>  console.log(error));
  }

  hideStatistics() {
    this.selectedReport = this.reports[0];
  }

}
