import { Component, OnInit } from '@angular/core';
import {EventFeedService} from '../../../../../services/event-feed.service';
import * as moment from 'moment';
import {GatewayService} from '../../../../../services/gateway.service';

@Component({
  selector: 'app-messages-dash',
  templateUrl: './messages-dash.component.html',
  styleUrls: ['./messages-dash.component.css']
})
export class MessagesDashComponent implements OnInit {

  events: any[] = [];

  constructor(
    private eventFeedService: EventFeedService,
    private gtwService: GatewayService
  ) {
    this.gtwService.realoadGateway$.subscribe(data => {
      if (data) {
        this.events = [];
        this.loadEventFeed();
      }
    });
  }

  ngOnInit() {
    setTimeout(() => this.loadEventFeed(), 500);
  }

  loadEventFeed() {
    this.eventFeedService.getEvents()
      .subscribe(data => {
        if (data.length > 0) {
          this.toRelativeTime(data);
          this.events = data;
        }
      }, err => {
        console.log(err);
      });
  }

  toRelativeTime(data: any[]) {
    data.map(event => {
      event.date = moment(event.date).valueOf();
      event.relative = moment(event.date).fromNow();
    });
  }

}
