import {Component, ElementRef, HostListener, OnInit, AfterViewInit, Renderer2, ViewChild} from '@angular/core';
import {ReportService} from "../../../../services/report.service";
import {DashboardService} from '../../../../services/dashboard.service';


declare var Chartist;
declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  @ViewChild('dashboardSide') dashboardSide: ElementRef;
  @ViewChild('reportSide') reportSide: ElementRef;
  dashboardParentElement: any;

  reports: any[];
  data: any;
  showStatistic = false;
  showDropdown  = false;
  changeSize    = false;
  constructor(
    private reportService: ReportService,
    private dashboardService: DashboardService,
    private renderer: Renderer2
    ) {
    this.reportService.showReport$.subscribe((data: any) => {
      if (window.innerWidth <= 767) {
        this.selectedReportOnMobile();
      }
    });

    this.reportService.toDashboard$.subscribe((should: boolean) => {
      if (should) {
        this.goToDashboard();
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(window.innerWidth);
    if (window.innerWidth > 767) {
      this.goToDashboard();
    }
  }

  ngOnInit() {

    this.reportService.showReport$.subscribe(data => {
      this.showStatistic = data.show;
    });

    this.data = {
      series: [
        [5, 2, 4, 2, 0]
      ]
    };
  }

  ngAfterViewInit() {
    this.dashboardParentElement = this.renderer.parentNode(this.dashboardSide.nativeElement);
  }

  onChangeSize(should: boolean) {
    console.log(should);
    this.changeSize = should;
    this.dashboardService.changeSize(should);
  }

  selectedReportOnMobile() {
    this.dashboardParentElement = this.renderer.parentNode(this.dashboardSide.nativeElement);
    this.renderer.removeChild(this.dashboardParentElement, this.dashboardSide.nativeElement);
    this.renderer.removeClass(this.reportSide.nativeElement, 'hide-report');
    this.renderer.addClass(this.reportSide.nativeElement, 'show-report');
  }

  goToDashboard() {
    this.renderer.removeClass(this.reportSide.nativeElement, 'show-report');
    this.renderer.addClass(this.reportSide.nativeElement, 'hide-report');
    this.renderer.removeChild(this.dashboardParentElement, this.reportSide.nativeElement);
    this.renderer.appendChild(this.dashboardParentElement, this.dashboardSide.nativeElement);
    this.renderer.appendChild(this.dashboardParentElement, this.reportSide.nativeElement);
  }

}
