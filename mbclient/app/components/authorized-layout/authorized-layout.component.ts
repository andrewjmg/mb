import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import {AngularFireDatabase} from "angularfire2/database";
import {Http} from "@angular/http";
import {UserService} from "../../services/user.service";
import {GatewayService} from "../../services/gateway.service";
import {ReportService} from "../../services/report.service";
import {dashboard} from "../../shared/constants/dashboard";
import 'rxjs/operator/map';
import * as md5 from 'md5';


declare var $;

@Component({
  selector: 'app-authorized-layout',
  templateUrl: './authorized-layout.component.html',
  styleUrls: ['./authorized-layout.component.css']
})
export class AuthorizedLayoutComponent implements OnInit {

  user: any;
  name: string;
  userProfile: any;
  showCollapse       = false;
  showProfileDrop    = false;
  gtw_key: string;
  reports: any[]  = [];
  gateways: any[] = [];

  constructor(
    private authService: AuthService,
    private db: AngularFireDatabase,
    private userService: UserService,
    private reportService: ReportService,
    private gtwService: GatewayService,
    private http: Http) { }

  ngOnInit() {
    const currentUser = this.authService.getMe().currentUser;
    this.db.object(`/users/${currentUser.uid}`)
    .subscribe(user => {
      this.user = user;
      this.name = user.name.substr(0, user.name.indexOf(' '));
    });

    const user          = JSON.parse(localStorage.getItem('u'));
    const email: string = user.email;
    const hash: string  = md5(email.toLowerCase().replace(/\s/g, ''));
    this.userService.getGravatar(hash)
      .subscribe(data => {
        this.userProfile = data.entry[0];
      }, err => {
        console.log(err);
      });

    this.gtwService.getKey()
      .subscribe((data: any[]) => {
        console.log(data);
        this.gateways = data;
        if (!localStorage.getItem('gtwKey') && data && data.length > 0) {
            this.gtw_key  = data[0].gatewayKey;
          localStorage.setItem('gtwKey', this.gtw_key);
        }
        this.loadReports();
      });

    this.gtwService.realoadGateway$.subscribe(data => {
      if (data) this.loadReports();
    });
  }

  loadReports() {
    this.reportService.getReportNames()
      .subscribe(reps => {
        if (reps.length > 0) {
          this.reports = reps;
        }else {
          this.reports = [];
        }
      });
  }

  signOut() {
    this.authService.signOut();
  }

  toggleCollapse() {
    this.showCollapse = !this.showCollapse
  }

  toggleProfileDrop() {
    this.showProfileDrop = !this.showProfileDrop;
  }

  selectReport(rep: string) {
    this.reportService.getSelectedReport(rep)
      .subscribe(report => {
        this.reportService.toggleShowReport({ show: true, report: report, range: dashboard.time_ranges[0], firstTime: true });
        this.showCollapse = !this.showCollapse;
      });
  }

  changeGateway(gtw) {
    this.gtw_key      = gtw;
    this.showCollapse = !this.showCollapse;
    localStorage.setItem('gtwKey', gtw);
    this.gtwService.toggleGateway(true);
    this.loadReports();
  }

}
