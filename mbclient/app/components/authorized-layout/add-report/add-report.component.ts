import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AddReport } from '../../../shared/constants/add-report';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-report',
  templateUrl: './add-report.component.html',
  styleUrls: ['./add-report.component.css']
})
export class AddReportComponent implements OnInit {

  reports: any[];
  counter = 0;
  selectAll = false;
  confirm = false;

  constructor( private router: Router, private location: Location ) {
    this.reports = AddReport.reports;
    for (const r of this.reports){
      if (r.checked) {
        this.counter++;
      }
    }
  }

  ngOnInit() {
  }

  selectReport(idx) {
    this.reports[idx].checked = !this.reports[idx].checked;
    if (this.reports[idx].checked) {
      this.counter++;
    }else {
      this.counter > 0 ? this.counter-- : this.counter = 0;
    }
    console.log(this.counter);
  }

  toggleAll() {
    this.selectAll = !this.selectAll;

    if (this.selectAll) {
      this.counter = this.reports.length;
      this.reports.map( report => report.checked = true);
    }else {
      this.counter = 0;
      this.reports.map( report => report.checked = false);
    }
  }

  confirmAdd() {
    this.confirm = !this.confirm;
    const tout = setTimeout(() => {
      this.router.navigate(['/dashboard']);
      clearTimeout(tout);
    }, 2500);
  }

  back(){
    this.location.back();
  }
}
