import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Alert} from '../../../interfaces/alert';
import {AngularFireDatabase} from 'angularfire2/database';
import {Http, RequestOptions, Headers} from '@angular/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import { RegisterService } from '../../../services/auth-services/register.service';
import { locale } from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: Observable<any>;
  email: string;
  password: string;
  remember = false;
  showAlert = false;
  formLogin: FormGroup;
  alert: Alert;
  loading = false;
  googleUser: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private db: AngularFireDatabase,
    private registerService: RegisterService,
    private http: Http
  ) {
    this.formLogin = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    this.alert = {type: '', message: '', show: false};
    setTimeout(() => this.showAlert = !this.showAlert, 3000);
  }

  loadCurrentUserClaims() {
    this.authService.getMe().currentUser.getIdToken(true).then(jwt => {
      const userObject = JSON.parse(atob(jwt.split('.')[1]));
      const {gateways} = userObject;
      this.setUserGateways(gateways);
    });
  }

  requestUserApplicationData(jwt) {
    const url = `${environment.apiUrl}/auth`;
    const Authorization = `Bearer ${jwt}`;
    const headers = new Headers({Authorization: Authorization});
    const options = new RequestOptions({ headers });
    this.http.post(url, {}, options).catch((error: any) => Observable.throw(error.json() || 'Server error'))
      .subscribe(res => {
        this.loadCurrentUserClaims();
      }, err => {
        this.alert = {
          type: 'danger',
          message: 'Wrong E-mail or password',
          show: true
        };
        this.loading = !this.loading;
        const tout = setTimeout(() => {
          this.alert.show = false;
          clearTimeout(tout);
        }, 2000);
      });
  }

  setUserGateways(gateways) {

    localStorage.setItem('gateways', JSON.stringify(gateways));

    if (gateways && gateways.length > 0) {
      localStorage.setItem('gtwKey', gateways[0].gatewayKey);
    }

    const uid = localStorage.getItem('uid');
    this.user = this.db.object(`/users/${uid}`);
    this.user.subscribe(user => {
      const u = { name: user.name, email: user.email };
      localStorage.setItem('u', JSON.stringify(u));
      const tout = setTimeout(() => {
        this.loading = !this.loading;
        this.router.navigate(['/dashboard']);
        clearTimeout(tout);
      }, 1000);
    }, err => {
      console.log(err);
    });
  }

  loginWithEmail( $event ) {
    $event.preventDefault();
    if (this.formLogin.valid) {
      this.loading = !this.loading;
      let tout;
      this.authService.loginWithEmailAndPassword(this.formLogin.value.email, this.formLogin.value.password)
      .then( data => {
        localStorage.setItem('uid', data.uid);
        this.authService.getMe().currentUser.getIdToken(true).then(jwt => {
          this.requestUserApplicationData(jwt);
        });
      })
      .catch( error => {
        console.log(error);
        switch (error['code']) {
          case 'auth/user-not-found':
          this.alert = {type: 'danger', message: 'Wrong E-mail or Password', show: true };
          this.loading = !this.loading;
          tout = setTimeout(() => {
            this.alert.show = false;
            clearTimeout(tout);
          }, 2000);
          break;

          case 'auth/wrong-password':
          this.alert = {type: 'danger', message: 'Wrong E-mail or Password', show: true };
          this.loading = !this.loading;
          tout = setTimeout(() => {
            this.alert.show = false;
            clearTimeout(tout);
          }, 2000);
          break;

          case 'auth/too-many-requests':
          this.alert = {type: 'danger', message: 'Server Error, please try later!', show: true};
          this.loading = !this.loading;
          tout = setTimeout(() => {
            this.alert.show = false;
            clearTimeout(tout);
          }, 2000);
          break;

          case 'auth/network-request-failed':
          this.alert = {
            type: 'danger',
            message: 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.',
            show: true
          };
          this.loading = !this.loading;
          tout = setTimeout(() => {
            this.alert.show = false;
            clearTimeout(tout);
          }, 2000);
          break;

          default:
            this.alert = {
              type: 'danger',
              message: 'Something went wrong',
              show: true
            };
            this.loading = !this.loading;
            tout = setTimeout(() => {
              this.alert.show = false;
              clearTimeout(tout);
            }, 2000);
          break;
        }
      });
    }
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle()
    .then( data => {
      console.log(data);
      this.authService.verifyGoogleUser(data.user)
      .subscribe(verify => {
        this.googleUser = data.user;
        if (verify.length < 1) {
          const user_obj  = this.db.object(`/users/${this.googleUser.uid}`);
          user_obj.set({email: this.googleUser.email, name: this.googleUser.displayName})
          .then((success) => {
            localStorage.clear();
            localStorage.setItem('uid', this.googleUser.uid);
            const userProfile = {firstName: this.googleUser.displayName, email: this.googleUser.email};
            this.router.navigate(['/register/plan']).then( () => {
              this.registerService.setUser( userProfile );
              const account = {user: userProfile};
              localStorage.setItem('account', JSON.stringify(account));
            });
          })
          .catch((error) => {
            console.log(error);
          });
        }else {

          localStorage.setItem('u', JSON.stringify({name: this.googleUser.displayName, email: this.googleUser.email}));
          localStorage.setItem('uid', this.googleUser.uid);
          this.router.navigate(['/dashboard']);
        }
      }, err => {
        console.log(err);
      });
    })
    .catch( error => {
      console.log(error);
    });
  }

  rememberMe() {
    this.remember = !this.remember;
  }

}
