import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { dashboard } from '../../shared/constants/dashboard';
import { CapitalizePipe } from '../../pipes/capitalize.pipe';
import * as extrapolation from 'extrapolate';
import * as moment from 'moment';
import {DolarCurrencyPipe} from '../../pipes/dolar-currency.pipe';

declare var Chartist;

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {

  report: any;
  range: any;
  reportName: any;
  historyChart: any;
  stackBarChart: any;
  pieChart: any;
  historyLabels: any[]  = [];
  historySeries: any[]  = [];
  stackBarLabels: any[] = [];
  stackBarSeries: any[] = [];
  pieChartLabels: any[] = [];
  pieChartSeries: any[] = [];
  dashboard = dashboard.card_labels;

  constructor(
    private activatedRoute: ActivatedRoute,
    private db: AngularFireDatabase,
    private currency: DolarCurrencyPipe,
    private capitalize: CapitalizePipe,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      const { id } = params;
      this.db.object(`/exports/${id}`)
        .subscribe(data => {
          const { report, range, reportName } = data;
          console.log(report);
          this.reportName         = reportName;
          this.report             = report;
          this.range              = range;
          this.report.name        = dashboard[report];
          this.setupHistorySeries(this.report.history[range]);
          this.linearExtrap();

          if (reportName === 'mrr') {
            this.setupStackBarSeries(this.report.growth);
            this.loadStackedBarChart();
          }

          if (reportName !== 'revenuechurn') {
            this.setupPieChartSeries(this.report.byPlan[range]);
            this.loadPieChart();
          }
        });

    });
  }

  setupHistorySeries(history) {
    this.historySeries = [{data: []}];
    this.historySeries[0].data = this.buildHistoryDataSeries(history);
  }

  setupStackBarSeries(data) {
    data.map(data => {
      const date = new Date(data.date);
      if (!this.stackBarLabels.find (x => x === data.month + data.year)) {
        this.stackBarLabels.push(data.month + data.year);
      }

      let position = 0;
      for (const val in data) {
        if (val !== 'month' && val !== 'year') {
          if (this.stackBarSeries[position] == null) {
            this.stackBarSeries[position] = [];
            this.stackBarSeries[position].push({
              meta: this.capitalize.transform(val),
              value: (data[val] / 100)
            });
          } else {
            this.stackBarSeries[position].push({
              meta: this.capitalize.transform(val),
              value: (data[val] / 100)
            });
          }
          position++;
        }
      }
    });
  }

  setupPieChartSeries(byPlan) {
    byPlan.map(data => {
      this.pieChartLabels.push(data.percentage.toFixed(2));
      this.pieChartSeries.push({
        meta: data.planName,
        value: data.percentage.toFixed(2)
      });
    });
  }

  buildHistoryDataSeries(data) {
    return data.map((obj, index) => {
      if (index === 0 || index === data.length - 1) {
        return {
          x: new Date(obj.x),
          y: obj.y
        };
      }else {
        if (data[index - 1].y === obj.y) {
          return {
            meta: {'change': false},
            x: new Date(obj.x),
            y: obj.y
          };
        }else {
          return {
            x: new Date(obj.x),
            y: obj.y
          };
        }
      }
    });
  }

  linearExtrap() {

    const data = this.historySeries[0].data;

    const linear       = new extrapolation();
    const extraDays    = [];
    const forecastData = [];
    data.map((obj) => {
      const x = moment(obj.x).valueOf();
      const y = obj.y;
      linear.given(x, y);
    });

    let date = data[data.length - 1].x;

    for (let i = 0; i < 3; i++) {
      date = moment(date).add(1, 'days').valueOf();
      extraDays.push(date);
    }

    forecastData.push({
      x: new Date(data[data.length - 1].x),
      y: data[data.length - 1].y
    });

    extraDays.map(time => {
      forecastData.push({
        x: new Date(time),
        y: linear.getLinear(time)
      });
    });

    this.historySeries.push({className: 'forecast-line', data: this.buildHistoryDataSeries(forecastData)});
    this.loadLineChart();
  }

  loadLineChart() {
    this.historyChart = new Chartist.Line('.ct-chart', {
      labels: this.historyLabels,
      series: this.historySeries
    }, {
      showArea: true,
      fullWidth: true,
      lineSmooth: Chartist.Interpolation.simple({
        fillHoles: false,
        showPoints: false
      }),
      axisX: {
        type: Chartist.FixedScaleAxis,
        divisor: 6,
        labelInterpolationFnc: (value) => {
          return moment(value).format('MMM D');
        }
      },
      axisY: {
        labelInterpolationFnc: (value) => {
          return this.currency.transform(value);
        }
      },
      plugins: [
        Chartist.plugins.tooltip({
          appendToBody: false,
          transformTooltipTextFnc: val => {
            val = val.split(',')[1];
            return this.currency.transform(val);
          }
        })
      ],
      height: '250px',
    }).on('draw', function (data) {
      if (data.type === 'point' && data.meta) {
        data.element.attr({
          style: 'display: none'
        });
      }
    });
  }

  loadStackedBarChart() {
    this.stackBarChart = new Chartist.Bar('.ct-chart-bar', {
      labels: this.stackBarLabels,
      series: this.stackBarSeries,
    }, {
      stackBars: true,
      axisY: {
        labelInterpolationFnc: (value) => {
          return value >= 0
            ? `$${value}`
            : `-$${(value*-1)}`
        }
      },
      plugins: [
        Chartist.plugins.tooltip({
          class: 'tooltip-bg-mb',
          appendToBody: true,
          transformTooltipTextFnc: value => {
            if (value >= 0) {
              return `$${value}`
            } else {
              return `-$${(value*-1)}`
            }
          }
        })
      ],
      height: '250px',
      width: '100%'
    }).on('draw', data => {
      if (data.type === 'bar') {
        let style = 'stroke-width: 75px;';
        let {meta} = data;
        if (meta === 'New') {
          style += 'stroke: #2CBF6D;';
        } else if (meta === 'Churn') {
          style += 'stroke: #E16070;';
        } else if (meta === 'Contraction') {
          style += 'stroke: #EFC663;';
        } else if (meta === 'Expansion') {
          style += 'stroke: #7049A3;';
        } else if (meta === 'Change') {
          style += 'stroke: #41B7CC;';
        }
        data.element.attr({style});
      }
    });
  }

  loadPieChart() {

    const data = {
      labels: this.pieChartLabels,
      series: this.pieChartSeries
    };

    const responsiveOptions = [
      ['screen and (min-width: 1200px)', {
        chartPadding: 30,
        labelOffset: 10,
        labelDirection: 'explode',
        /*labelInterpolationFnc: function(value) {
          return value + '%';
        }*/
      }],
      ['screen and (min-width: 1024px)', {
        labelOffset: 40,
        chartPadding: 20
      }]
    ];

    this.pieChart = new Chartist.Pie('.ct-slice-pie', data, {
      labelInterpolationFnc: (value) => {
        return value + '%';
      },
      plugins: [
        Chartist.plugins.tooltip({
          transformTooltipTextFnc: (data, foo) => {
            return data + '%';
          }
        })
      ]
    }, responsiveOptions);
  }

}
