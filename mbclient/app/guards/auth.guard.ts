import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { AuthService } from '../services/auth.service';
import 'rxjs/Rx';

@Injectable()
export class AuthGuard implements CanActivate {

  private userReplaySubject: ReplaySubject<any>;

  constructor ( private authService: AuthService, private router: Router ) {
    this.userReplaySubject = new ReplaySubject(1);
    this.authService.isLoggedIn().onAuthStateChanged(this.userReplaySubject);
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this.userReplaySubject.map( user => {
      if ( !user ) {
        this.router.navigate(['/login']);
      }
      return !!user;
    }).take(1);
  }
}
