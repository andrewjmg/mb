import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule, JsonpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import { DragScrollModule } from 'angular2-drag-scroll';
import { DragulaModule } from 'ng2-dragula';

// Routes
import { ROUTING } from './app.routes';

// Guards
import { AuthGuard } from './guards/auth.guard'
import { RegisterGuard } from './guards/register.guard';
import { GuestGuard } from './guards/guest.guard';

// Services
import { AuthService } from './services/auth.service';
import { ReportService } from './services/report.service';
import { AccountService } from './services/account.service';
import { GatewayService } from './services/gateway.service';
import { GatewayAuthService } from './services/gateway-auth.service';
import { UserService } from './services/user.service';
import { RegisterService } from './services/auth-services/register.service';
import { AlertService } from './shared/alert/_services/alert.service';
import { EventFeedService } from './services/event-feed.service';
import { DashboardService } from './services/dashboard.service';
import { ExportService } from './services/export.service';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { MonthDayPipe } from './pipes/month-day.pipe';
import { CurrencyPipe} from '@angular/common';
import { MbCurrencyPipe } from './pipes/mb-currency.pipe';
import { PercentagePipe } from './pipes/percentage.pipe';
import { MomentDatePipe } from './pipes/moment-date.pipe';

// Components

import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { LoginComponent } from './components/login-layout/login/login.component';
import { AuthorizedLayoutComponent } from './components/authorized-layout/authorized-layout.component';
import { DashboardComponent } from './components/authorized-layout/dashboard-layout/dashboard/dashboard.component';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { RegisterLayoutComponent } from './components/register-layout/register-layout.component';
import { DashboardLayoutComponent } from './components/authorized-layout/dashboard-layout/dashboard-layout.component';
import { AccountLayoutComponent } from './components/authorized-layout/account-layout/account-layout.component';
import { ProfileComponent } from './components/register-layout/profile/profile.component';
import { PlanComponent } from './components/register-layout/plan/plan.component';
import { BillingComponent } from './components/register-layout/billing/billing.component';
import { RegisterGatewayComponent } from './components/register-layout/gateway/gateway.component';
import { ConfirmationComponent } from './components/register-layout/confirmation/confirmation.component';

import { SuccessComponent } from './components/register-layout/success/success.component';
import { FirstTimeComponent } from './components/authorized-layout/first-time/first-time.component';
import { AddReportComponent } from './components/authorized-layout/add-report/add-report.component';

import { InformationComponent } from './components/authorized-layout/account-layout/information/information.component';
import { UploadComponent } from './components/authorized-layout/account-layout/upload/upload.component';
import { AccountComponent } from './components/authorized-layout/account-layout/account/account.component';
import { MenuComponent } from './components/authorized-layout/account-layout/menu/menu.component';
import { BillingUpdateComponent } from './components/authorized-layout/account-layout/billing-update/billing-update.component';
import { GatewayUpdateComponent } from './components/authorized-layout/account-layout/gateway-update/gateway-update.component';

import { StripeCallbackComponent } from './components/register-layout/gateway/stripe-callback/stripe-callback.component';
import { PaypalCallbackComponent } from './components/register-layout/gateway/paypal-callback/paypal-callback.component';
import { CentsPipe } from './pipes/cents.pipe';
import { BarSpinnerComponent } from './shared/bar-spinner/bar-spinner.component';
import { ThreeDotsSpinnerComponent } from './shared/three-dots-spinner/three-dots-spinner.component';
import { DotsSpinnerComponent } from './shared/dots-spinner/dots-spinner.component';
import { DefaultSpinnerComponent } from './shared/default-spinner/default-spinner.component';
import { KpiDashComponent } from './components/authorized-layout/dashboard-layout/dashboard/kpi-dash/kpi-dash.component';
import { MessagesDashComponent } from './components/authorized-layout/dashboard-layout/dashboard/messages-dash/messages-dash.component';
import { StatisticsDashComponent } from './components/authorized-layout/dashboard-layout/dashboard/statistics-dash/statistics-dash.component';
import { AlertComponent } from './shared/alert/alert.component';
import { TransactionComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/transaction/transaction.component';
import { MrrComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/mrr/mrr.component';
import { ArrComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/arr/arr.component';
import { SubscriptionComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/subscription/subscription.component';
import { ReportHeaderComponent } from './components/authorized-layout/dashboard-layout/dashboard/shared/report-header/report-header.component';
import { ReportKpiComponent } from './components/authorized-layout/dashboard-layout/dashboard/shared/report-kpi/report-kpi.component';
import { ArpuComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/arpu/arpu.component';
import { ChurnComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/churn/churn.component';
import { LtvComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/ltv/ltv.component';
import { RevenuechurnComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/revenuechurn/revenuechurn.component';
import { ProfileInfoComponent } from './components/authorized-layout/account-layout/profile-info/profile-info.component';
import { PlanInfoComponent } from './components/authorized-layout/account-layout/plan-info/plan-info.component';
import { BillingInfoComponent } from './components/authorized-layout/account-layout/billing-info/billing-info.component';
import { GatewayInfoComponent } from './components/authorized-layout/account-layout/gateway-info/gateway-info.component';
import { ProfileUpdateComponent } from './components/authorized-layout/account-layout/profile-update/profile-update.component';
import { GatewayAddComponent } from './components/authorized-layout/account-layout/gateway-add/gateway-add.component';
import { AddStripeCallbackComponent } from './components/authorized-layout/account-layout/gateway-add/add-stripe-callback/add-stripe-callback.component';
import { AddPaypalCallbackComponent } from './components/authorized-layout/account-layout/gateway-add/add-paypal-callback/add-paypal-callback.component';
import { ExportComponent } from './components/export/export.component';
import { DolarCurrencyPipe } from './pipes/dolar-currency.pipe';
import { HistoryChartComponent } from './components/authorized-layout/dashboard-layout/dashboard/shared/history-chart/history-chart.component';
import { PlansChartComponent } from './components/authorized-layout/dashboard-layout/dashboard/shared/plans-chart/plans-chart.component';
import { RetentionTableComponent } from './components/authorized-layout/dashboard-layout/dashboard/shared/retention-table/retention-table.component';
import { FeeComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/fee/fee.component';
import { RefundComponent } from './components/authorized-layout/dashboard-layout/dashboard/reports/refund/refund.component';

declare var Hammer: any;

export class MyHammerConfig extends HammerGestureConfig  {
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: "pan-y"
    });
    /*mc.get('swipe').set({
      direction: Hammer.DIRECTION_ALL,
      threshold: 1,
      velocity: 0.1
    });*/
    const singleTap = new Hammer.Tap({ event: 'singletap' });
    const doubleTap = new Hammer.Tap({event: 'doubletap', taps: 2});
    const tripleTap = new Hammer.Tap({event: 'tripletap', taps: 3 });
    mc.add([tripleTap, doubleTap, singleTap]);

    tripleTap.recognizeWith([doubleTap, singleTap]);
    doubleTap.recognizeWith(singleTap);

    doubleTap.requireFailure(tripleTap);
    singleTap.requireFailure([tripleTap, doubleTap]);

    return mc;
  }
}


@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    LoginComponent,
    AuthorizedLayoutComponent,
    DashboardComponent,
    LoginLayoutComponent,
    RegisterLayoutComponent,
    DashboardLayoutComponent,
    AccountLayoutComponent,
    ProfileComponent,
    PlanComponent,
    BillingComponent,
    RegisterGatewayComponent,
    ConfirmationComponent,
    StripeCallbackComponent,
    PaypalCallbackComponent,
    SuccessComponent,
    FirstTimeComponent,
    AddReportComponent,
    InformationComponent,
    UploadComponent,
    AccountComponent,
    MenuComponent,
    BillingUpdateComponent,
    GatewayUpdateComponent,
    CentsPipe,
    BarSpinnerComponent,
    ThreeDotsSpinnerComponent,
    DotsSpinnerComponent,
    DefaultSpinnerComponent,
    KpiDashComponent,
    MessagesDashComponent,
    StatisticsDashComponent,
    AlertComponent,
    TransactionComponent,
    MrrComponent,
    CapitalizePipe,
    MonthDayPipe,
    MbCurrencyPipe,
    ArrComponent,
    SubscriptionComponent,
    ReportHeaderComponent,
    ReportKpiComponent,
    PercentagePipe,
    ArpuComponent,
    ChurnComponent,
    LtvComponent,
    RevenuechurnComponent,
    ProfileInfoComponent,
    PlanInfoComponent,
    BillingInfoComponent,
    GatewayInfoComponent,
    ProfileUpdateComponent,
    MomentDatePipe,
    GatewayAddComponent,
    AddStripeCallbackComponent,
    AddPaypalCallbackComponent,
    ExportComponent,
    DolarCurrencyPipe,
    HistoryChartComponent,
    PlansChartComponent,
    RetentionTableComponent,
    FeeComponent,
    RefundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    TextMaskModule,
    DragScrollModule,
    DragulaModule,
    BrowserAnimationsModule,
    ROUTING
  ],
  providers: [
    AuthGuard,
    RegisterGuard,
    GuestGuard,
    AuthService,
    ReportService,
    AccountService,
    GatewayService,
    GatewayAuthService,
    UserService,
    RegisterService,
    AlertService,
    EventFeedService,
    DashboardService,
    ExportService,
    MonthDayPipe,
    CurrencyPipe,
    CapitalizePipe,
    MbCurrencyPipe,
    PercentagePipe,
    DolarCurrencyPipe,
    /*{
      // hammer instantion with custom config
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig ,
    },*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
