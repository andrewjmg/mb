import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dolarCurrency'
})
export class DolarCurrencyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let m;
    let quantity = (value / 100);
    if (quantity < 0) {
      quantity = quantity * -1;
      m = `- $${(quantity.toFixed(2))}`;
    } else {
      m = '$' + quantity;
    }
    return m;
  }

}
