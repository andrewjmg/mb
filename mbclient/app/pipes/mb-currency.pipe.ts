import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'mbCurrency'
})
export class MbCurrencyPipe implements PipeTransform {

  constructor(private angularCurrency: CurrencyPipe) {}

  transform(value: number, currency: string = 'USD'): any {

    const curr = JSON.parse(localStorage.getItem('currency'));

    let quantity;

    /*
     * If curr (currency) is not saved in localStorage, by default set USD as currency
     * with this if statement set USD as default and return the quantity
     */
    if (!curr || (curr && !curr.code)) {
      quantity = (value / 100);
      quantity = this.angularCurrency.transform(quantity, 'USD', true);
      /*if (quantity < 0) {
        quantity = quantity * -1;
        m = `- $${(quantity.toFixed(2))}`;
      } else {
        m = '$' + quantity;
      }*/
      return quantity;
    }

    switch (curr.code) {

      case 'USD':
        quantity = (value / 100);
        quantity = this.angularCurrency.transform(quantity, 'USD', curr.symbol);
        break;

      case 'EUR':
        quantity = (value / 100);
        quantity = this.angularCurrency.transform(quantity, 'EUR', curr.symbol);
        break;

      case 'MXN':
        quantity = (value / 100);
        quantity = this.angularCurrency.transform(quantity, 'MXN', curr.symbol);
        break;

      default:
        quantity = (value / 100);
        quantity = this.angularCurrency.transform(quantity, 'USD', curr.symbol);
        break;
    }

    return quantity;
  }

}
