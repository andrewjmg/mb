import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentage'
})
export class PercentagePipe implements PipeTransform {

  transform(value: any, decimal: boolean = null): any {

    let percentage;
    if (value > 0 || value < 0) {
      if (Math.trunc(value).toString().length > 2) {
        percentage = Math.trunc(value) + '%';
      }else {
        percentage = Number.isInteger(value) ? value + '%' : value.toFixed(1) + '%';
      }
    }else {
      percentage = '--';
    }

    return percentage;
  }

}
