import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'monthDay'
})
export class MonthDayPipe implements PipeTransform {

  monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

  transform(value: any, args?: any): any {

    const date = new Date(value);

    return this.monthNames[date.getMonth()] + ' ' + date.getUTCDate();
  }

}
