import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class DashboardService {

  private changeSizeSource = new Subject<boolean>();
  onChangeSize             = this.changeSizeSource.asObservable();

  changeSize(should: boolean) {
    this.changeSizeSource.next(should);
  }

  constructor() { }

}
