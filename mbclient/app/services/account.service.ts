import { Injectable } from '@angular/core';
import { Gateway, Account } from '../shared/types/account';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Rx';
import {Subject} from 'rxjs/Subject';
import {Http, Jsonp} from '@angular/http';
import * as md5 from 'md5';
import {environment} from '../../environments/environment';

@Injectable()
export class AccountService {

  private menuLocationSource = new Subject<any>();
  menuLocation$              = this.menuLocationSource.asObservable();

  constructor( private db: AngularFireDatabase, private jsonp: Jsonp, private http: Http ) { }

  changeLocation(location: any) {
    this.menuLocationSource.next(location);
  }

  create(accountId): Observable<any> {
    const pathRef = this.db.object(`accounts/${accountId}`);
    const account: Account = {
      created: { '.sv': 'timestamp'}
    };

    return Observable.fromPromise(pathRef.set(account));
  }

  addGateway(uid, name, id): Observable<any> {
    const pathRef = this.db.list('/gateways');

    const gw: Gateway = { uid, name, id };

    return Observable.fromPromise(pathRef.push(gw));
  }

  getBillingInformation() {
    const apiUrl = environment.apiUrl;
    return this.http.post(`${apiUrl}/customer/billing/info`, {uid: localStorage.getItem('uid')})
      .map(response =>  response.json());
  }

  getCustomerId() {
    const uid = localStorage.getItem('uid');
    return this.db.object(`/users/${uid}/customer_id`);
  }

  getCustomerInfo(data) {
    const apiUrl = environment.apiUrl;
    return this.http.post(`${apiUrl}/customer/info`, data).map(response => response.json());
  }

  newCard(data) {
    const apiUrl = environment.apiUrl;
    return this.http.post(`${apiUrl}/customer/new/card`, data).map(response => response.json());
  }

  getGravatar() {
    const user          = JSON.parse(localStorage.getItem('u'));
    const email: string = user.email;
    const hash: string  = md5(email.toLowerCase().replace(/\s/g, ''));
    return this.jsonp.get(`https://www.gravatar.com/${hash}.json?callback=JSONP_CALLBACK`).map((response: any) => {
      const entry: any[] = response.json().entry;
      let photo = null;
      if (entry && entry.length > 0) {
        if (entry[0].photos && entry[0].photos.length > 0) {
          photo = entry[0].photos[0];
        }
      }
      return photo;
    });
  }

}
