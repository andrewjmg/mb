import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class ExportService {

  constructor(
    private http: Http,
    private db: AngularFireDatabase,
  ) { }
}
