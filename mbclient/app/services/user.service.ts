import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/operator/map';
import {Jsonp} from "@angular/http";

@Injectable()
export class UserService {

  constructor( private afAuth: AngularFireAuth, private db: AngularFireDatabase, private jsonp: Jsonp) {}

  getMe() {
    return this.afAuth.auth;
  }

  getUser(): Observable<any> {
    return this.db.list('messages');
  }

  getGravatar(hash: string) {
    return this.jsonp.get(`https://www.gravatar.com/${hash}.json?callback=JSONP_CALLBACK`).map(response => response.json());
    // return this.http.get(`https://www.gravatar.com/${hash}.json`).map(response => response.json());
  }

}
