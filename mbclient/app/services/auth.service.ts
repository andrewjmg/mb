import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import {Http} from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/operator/map';

@Injectable()
export class AuthService {

  private user: any;

  constructor( private afAuth: AngularFireAuth, private router: Router, private db: AngularFireDatabase, private http: Http ) { }

  loginWithEmailAndPassword( email: string, password: string ) {
    return this.afAuth.auth.signInWithEmailAndPassword( email, password );
  }

  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup( new firebase.auth.GoogleAuthProvider );
  }

  verifyGoogleUser(user) {
    return this.db.list('/gateways', {
      query: {
        orderByChild: 'uid',
        equalTo: user.uid,
      }
    });
  }

  isLoggedIn() {
    return this.afAuth.auth;

  }

  getMe() {
    return this.afAuth.auth;
  }

  register( email: string, password: string ) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  signOut() {
    this.afAuth.auth.signOut()
    .then( () => {
      localStorage.clear();
      this.router.navigate(['/login']);
    });
  }

}
