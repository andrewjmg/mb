import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {Http} from '@angular/http';
import 'rxjs/Rx';
import {environment} from '../../../environments/environment';

@Injectable()
export class RegisterService {

  private registerData = {
    user: {},
    plan: {},
    billing: {},
    gateway: {}
  };

  private registerUserSource = new Subject<any>();

  stepInfo$ = this.registerUserSource.asObservable();

  constructor( private http: Http ) {
  }

  nextStep(data: any) {
    this.registerUserSource.next(data);
  }

  setUser(user: any) {
    this.registerData.user = user;
  }

  setPlan(plan: any) {
    this.registerData.plan = plan;
  }

  setBilling(billing: any) {
    this.registerData.billing = billing;
  }

  setGateway(gateway: any) {
    this.registerData.gateway = gateway;
  }

  getRegisterData() {
    return this.registerData;
  }

  getPlans() {
    return this.http.get('http://localhost:4300/api/register/plans')
      .map( response => response.json() );
  }

  createCustomer(data: any) {
    return this.http.post('http://localhost:4300/api/register/customer/create', {
      email: data.email,
      source: data.source
    }).map(response => response.json());
  }

  createSubscription(data: any) {
    return this.http.post('http://localhost:4300/api/register/customer/subscription', {
      customer: data.customer,
      plan_id: data.plan_id
    }).map(response => response.json());
  }

  connectStripe() {
    const url = environment.stripe.oauthUrl;
    return this.http.get(url);
  }

  registerWithEmail(data) {
    const url = `${environment.apiUrl}/register/account`;
    return this.http.post(url, data).map(response => response.json());
  }

}
