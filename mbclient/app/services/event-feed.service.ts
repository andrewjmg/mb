import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class EventFeedService {

  constructor(private db: AngularFireDatabase) { }

  getEvents() {
    const gtwKey = localStorage.getItem('gtwKey');
    return this.db.object(`/streams/${gtwKey}`);
  }

}
