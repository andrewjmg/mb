import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {Subject} from "rxjs/Subject";
import 'rxjs/operator/map';
import {environment} from '../../environments/environment';
import {dashboard} from '../shared/constants/dashboard';

@Injectable()
export class ReportService {

  private showReportSource     = new Subject<any>();
  private changeRangeSource    = new Subject<any>();
  private selectedReportSource = new Subject<any>();
  private goToDashboardSource  = new Subject<boolean>();

  showReport$     = this.showReportSource.asObservable();
  changedRange$   = this.changeRangeSource.asObservable();
  selectedReport$ = this.selectedReportSource.asObservable();
  toDashboard$    = this.goToDashboardSource.asObservable();

  constructor( private db: AngularFireDatabase, private http: Http ) { }

  toggleShowReport(data: any) {
    this.showReportSource.next(data);
  }

  setSelectedreport(data: any) {
    this.selectedReportSource.next(data);
  }

  goToDashboard(should: boolean) {
    this.goToDashboardSource.next(should)
  }

  changeRange(data: any) {
    this.changeRangeSource.next(data);
  }

  getRevenueReport(report, range): Observable<any> {
    const gtwKey = localStorage.getItem('gtwKey');
    return this.db.object(`/reports/${gtwKey}/${report}/summary/${range}`);
  }

  getPercentageReport(report) {
    const gtwKey = localStorage.getItem('gtwKey');
    return this.db.object(`/reports/${gtwKey}/${report}/kpi/monthAgo/percentageChange`);
  }

  getSelectedReport(report) {
    const gtwKey = localStorage.getItem('gtwKey');
    return this.db.object(`/reports/${gtwKey}/${report}`);
  }

  getReportNames(ordered: any[] = null) {
    const gtwKey = localStorage.getItem('gtwKey');
    return this.http.get(`${environment.firebase.databaseURL}/reports/${gtwKey}.json?shallow=true`)
      .map(response => {
        const availableRep = [];
        const resp         = response.json();
        const keys         = (resp) ? Object.keys(resp) : [];

        if (keys.length > 0 && gtwKey) {

          let reports = [];

          if (ordered && ordered.length > 0 && ordered.length === keys.length) {
            ordered.map(rep => {
              const index = keys.findIndex(k =>  rep === k);
              if (index >= 0) {
                reports.push(rep);
              }
            });

          }else {
            reports = keys;
          }

          if (reports.length > 0) {
            reports.map(rep => {
              if (dashboard.card_labels[rep]) {
                dashboard.card_labels[rep].key = rep;
                availableRep.push(dashboard.card_labels[rep]);
              }
            });
          }
        }

        return availableRep;
      });
  }

  toCompareWith(gtwKey: string, report: string, range: number) {
    return this.db.object(`/reports/${gtwKey}/${report}/history/${range}`);
  }

  exportToPDF(data) {
    return this.http.post(`${environment.apiUrl}/services/pdf`, data).map(response => response.json());
  }

  getCustomData(report, from, to, type?) {
    console.log('at getCustomData', report, from, to, type)
    const gtwKey = localStorage.getItem('gtwKey');
    let url = `${environment.apiUrl}/reports/custom?gatewayKey=${gtwKey}&report=${report}&from=${from}&to=${to}`
    if (type) {
      url += `&type=${type}`;
    }
    return this.http.get(url)
      .map(response => response.json());

  }

  verifyReport(report) {
    this.verifySummary(report.summary);
    this.verifyHistory(report.history);
    this.verifyKpi(report.kpi);

    if (report.$key !== 'fee' && report.$key !== 'refund') {
      this.verifyByPlan(report.byPlan);
    }

  }

  private verifySummary(summary) {
    if (summary) {
      if (summary['7']) {
        if (!summary['7'].average) {
          summary['7'].average = 0;
        }

        if (!summary['7'].total) {
          summary['7'].total = 0;
        }
      }else {
        summary['7'] = {average: 0, total: 0};
      }

      if (summary['14']) {
        if (!summary['14'].average) {
          summary['14'].average = 0;
        }

        if (!summary['14'].total) {
          summary['14'].total = 0;
        }
      }else {
        summary['14'] = {average: 0, total: 0};
      }

      if (summary['30']) {
        if (!summary['30'].average) {
          summary['30'].average = 0;
        }

        if (!summary['30'].total) {
          summary['30'].total = 0;
        }
      }else {
        summary['30'] = {average: 0, total: 0};
      }
    }
  }

  private verifyHistory(history) {
    if (history) {
      if (history['7'] && !history['7'].length) {
        history['7'] = [];
      }

      if (history['14'] && !history['14'].length) {
        history['14'] = [];
      }

      if (history['30'] && !history['30'].length) {
        history['30'] = [];
      }
    }
  }

  private verifyKpi(kpi) {

    if (kpi) {
      if (!kpi.current) {
        kpi.current = 0;
      }

      if (kpi.monthAgo) {
        if (!kpi.monthAgo.difference) {
          kpi.monthAgo.difference = 0;
        }

        if (!kpi.monthAgo.percentageChange) {
          kpi.monthAgo.percentageChange = 0;
        }

        if (!kpi.monthAgo.total) {
          kpi.monthAgo.total = 0;
        }

      }else {
        kpi.monthAgo = { difference: 0, percentageChange: 0, total: 0 };
      }

      if (kpi.threeMonthsAgo) {
        if (!kpi.threeMonthsAgo.difference) {
          kpi.threeMonthsAgo.difference = 0;
        }

        if (!kpi.threeMonthsAgo.percentageChange) {
          kpi.threeMonthsAgo.percentageChange = 0;
        }

        if (!kpi.threeMonthsAgo.total) {
          kpi.threeMonthsAgo.total = 0;
        }

      }else {
        kpi.threeMonthsAgo = { difference: 0, percentageChange: 0, total: 0 };
      }

      if (kpi.sixMonthsAgo) {
        if (!kpi.sixMonthsAgo.difference) {
          kpi.sixMonthsAgo.difference = 0;
        }

        if (!kpi.sixMonthsAgo.percentageChange) {
          kpi.sixMonthsAgo.percentageChange = 0;
        }

        if (!kpi.sixMonthsAgo.total) {
          kpi.sixMonthsAgo.total = 0;
        }

      }else {
        kpi.sixMonthsAgo = { difference: 0, percentageChange: 0, total: 0 };
      }

      if (kpi.twelveMonthsAgo) {
        if (!kpi.twelveMonthsAgo.difference) {
          kpi.twelveMonthsAgo.difference = 0;
        }

        if (!kpi.twelveMonthsAgo.percentageChange) {
          kpi.twelveMonthsAgo.percentageChange = 0;
        }

        if (!kpi.twelveMonthsAgo.total) {
          kpi.twelveMonthsAgo.total = 0;
        }

      }else {
        kpi.twelveMonthsAgo = { difference: 0, percentageChange: 0, total: 0 };
      }
    }

  }

  private verifyByPlan(byPlan) {
    if (byPlan) {
      if (byPlan['7'] && !byPlan['7'].length) {
        byPlan['7'] = [];
      }

      if (byPlan['14'] && !byPlan['14'].length) {
        byPlan['14'] = [];
      }

      if (byPlan['30'] && !byPlan['30'].length) {
        byPlan['30'] = [];
      }
    }
  }

}
