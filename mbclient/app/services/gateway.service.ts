import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { Gateway } from '../shared/models/gateway';
import {Subject} from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';


@Injectable()
export class GatewayService {

  private toggleGatewaySource = new Subject<boolean>();
  realoadGateway$             = this.toggleGatewaySource.asObservable();

  constructor( private db: AngularFireDatabase, private http: Http ) { }

  toggleGateway(should: boolean) {
    this.toggleGatewaySource.next(should);
  }

  getKey() {

    return this.db.list('/gateways', {
      query: {
        orderByChild: 'uid',
        equalTo: localStorage.getItem('uid'),
      }
    }).map(data => {
      const gateways = [];

      data.map(gtw => {

        if (!gtw.deleted) {
          gateways.push({
            name: gtw.name,
            $key: gtw.$key,
            deleted: gtw.deleted
          });
        }
      });

      return gateways;
    });
  }

  getRemovedGateways() {
    return this.db.list('/gateways', {
      query: {
        orderByChild: 'uid',
        equalTo: localStorage.getItem('uid'),
      }
    }).map(data => {
      const gateways = [];

      data.map(gtw => {

        if (gtw.deleted) {
          gateways.push({
            name: gtw.name,
            $key: gtw.$key,
            deleted: gtw.deleted
          });
        }
      });

      return gateways;
    });
  }

  getSelectedKey() {
    return this.db.object(`/users/${localStorage.getItem('uid')}/selected_gtw`);

  }

  list( uid ): Observable<Gateway[]>{
    return this.db.list('gateways', {
      query: {
        orderByChild: 'uid',
        equalTo: uid
      }
    });
    // .map(results => Gateway.fromJsonArray).do(console.log)
    /*.filter(results => results && results.length > 0)
     .map(results => Gateway.fromJsonArray)
     .do(console.log);*/
  }

  addGateway( uid, name, id, access_token, type ): Observable<any> {
    const pathRef = this.db.list('gateways');
    const gw = { uid, name, id, access_token, type,  deleted: false };
    return Observable.fromPromise(pathRef.push(gw));
  }

  registerGateway(uid, name, id, access_token, type, currency ): Observable<any> {
    const pathRef = this.db.list('gateways');
    const gw = { uid, name, id, access_token, type,  currency, deleted: false };
    return Observable.fromPromise(pathRef.push(gw));
  }

  removeGateway(gtwKey: string) {
    return this.db.object(`/gateways/${gtwKey}`)
      .update({deleted: true});
  }

  activateGateway(gtwKey: string) {
    return this.db.object(`/gateways/${gtwKey}`)
      .update({deleted: false});
  }

  getTransactions( uid, gateWay, accessToken ) {
    const params: URLSearchParams = new URLSearchParams();

    params.set('uid', uid);
    params.set('gateway', gateWay);
    params.set('accessToken', accessToken);

    // let url = `${config.mbApi}/api/gateways/transactions`;
    const url = '';
    return this.http.get(url, {search: params});
  }

}
