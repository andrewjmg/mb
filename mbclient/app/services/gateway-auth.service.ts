import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class GatewayAuthService {

  constructor( private http: Http, private afAuth: AngularFireAuth ) { }

  connectStripe(): Observable<any> {
    // let url = `${config.mbApp}/api/auth/stripe`;
    return this.http.get('');
  }

}
