import { Routes, RouterModule } from '@angular/router';

// Guards
import { AuthGuard } from './guards/auth.guard';

// Authentication components
// import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { LoginComponent } from './components/login-layout/login/login.component';
import { RegisterLayoutComponent } from './components/register-layout/register-layout.component';
import { ProfileComponent } from './components/register-layout/profile/profile.component';
import { PlanComponent } from './components/register-layout/plan/plan.component';
import { BillingComponent } from './components/register-layout/billing/billing.component';
import { RegisterGatewayComponent } from './components/register-layout/gateway/gateway.component';
import { ConfirmationComponent } from './components/register-layout/confirmation/confirmation.component';
import { SuccessComponent } from './components/register-layout/success/success.component';

/********************** Authorized components ***********************************************************/
import { AuthorizedLayoutComponent } from './components/authorized-layout/authorized-layout.component';

import { FirstTimeComponent } from './components/authorized-layout/first-time/first-time.component';

import { AddReportComponent } from './components/authorized-layout/add-report/add-report.component';

// Dashboard
import { DashboardLayoutComponent } from './components/authorized-layout/dashboard-layout/dashboard-layout.component';
import { DashboardComponent } from './components/authorized-layout/dashboard-layout/dashboard/dashboard.component';

import { InformationComponent } from './components/authorized-layout/account-layout/information/information.component';
import { AccountComponent } from './components/authorized-layout/account-layout/account/account.component';
import { UploadComponent } from './components/authorized-layout/account-layout/upload/upload.component';

// Acount
import { AccountLayoutComponent } from './components/authorized-layout/account-layout/account-layout.component';
import {MenuComponent} from './components/authorized-layout/account-layout/menu/menu.component';
import {BillingUpdateComponent} from './components/authorized-layout/account-layout/billing-update/billing-update.component';
import {GatewayUpdateComponent} from './components/authorized-layout/account-layout/gateway-update/gateway-update.component';
import {RegisterGuard} from './guards/register.guard';
import {GuestGuard} from './guards/guest.guard';
import {StripeCallbackComponent} from './components/register-layout/gateway/stripe-callback/stripe-callback.component';
import {PaypalCallbackComponent} from './components/register-layout/gateway/paypal-callback/paypal-callback.component';
import {ProfileInfoComponent} from './components/authorized-layout/account-layout/profile-info/profile-info.component';
import {ProfileUpdateComponent} from './components/authorized-layout/account-layout/profile-update/profile-update.component';
import {PlanInfoComponent} from './components/authorized-layout/account-layout/plan-info/plan-info.component';
import {BillingInfoComponent} from './components/authorized-layout/account-layout/billing-info/billing-info.component';
import {GatewayInfoComponent} from './components/authorized-layout/account-layout/gateway-info/gateway-info.component';
import {GatewayAddComponent} from './components/authorized-layout/account-layout/gateway-add/gateway-add.component';
import {AddStripeCallbackComponent} from './components/authorized-layout/account-layout/gateway-add/add-stripe-callback/add-stripe-callback.component';
import {AddPaypalCallbackComponent} from './components/authorized-layout/account-layout/gateway-add/add-paypal-callback/add-paypal-callback.component';
import { ExportComponent } from './components/export/export.component';


const ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
  {
    path: 'login',
    component:  LoginLayoutComponent,
    children: [
      { path: '', component: LoginComponent },
    ]
  },
  {
    path: 'register',
    component: RegisterLayoutComponent,
    canActivate: [GuestGuard],
    children: [
      { path: '', component: ProfileComponent },
      { path: 'plan', component: PlanComponent,  },
      { path: 'billing', component: BillingComponent,  },
      { path: 'gateway', component: RegisterGatewayComponent,  },
      { path: 'confirmation', component: ConfirmationComponent,  },
      { path: 'success', component: SuccessComponent,  }
    ]
  },
  { path: 'settings/gateways/stripe/callback', component: StripeCallbackComponent },
  { path: 'settings/gateways/paypal/callback', component: PaypalCallbackComponent },
  {
    path: 'services/pdf/:id',
    component: ExportComponent
  },
  {
    path: '',
    component: AuthorizedLayoutComponent,
    canActivate: [ AuthGuard ],
    children: [
      {
        path: 'first',
        component: FirstTimeComponent
      },
      {
        path: 'reports/add',
        component: AddReportComponent
      },
      {
        path: 'dashboard',
        component: DashboardLayoutComponent,
        children: [
          { path: '', component: DashboardComponent }
        ]
      },
      {
        path: 'account',
        component: AccountLayoutComponent,
        children: [
          { path: '', component: MenuComponent },
          { path: 'profile', component: ProfileInfoComponent },
          { path: 'profile/update', component: ProfileUpdateComponent },
          // { path: 'profile/upload', component: UploadComponent },
          { path: 'plan', component: PlanInfoComponent },
          { path: 'billing', component: BillingInfoComponent },
          { path: 'billing/update', component: BillingUpdateComponent },
          { path: 'gateway', component: GatewayInfoComponent },
          { path: 'gateway/update', component: GatewayUpdateComponent },
          { path: 'gateway/add', component: GatewayAddComponent },
          { path: 'gateway/stripe/callback', component: AddStripeCallbackComponent },
          { path: 'gateway/paypal/callback', component: AddPaypalCallbackComponent }
        ]
      }
    ]
  }
];

export const ROUTING = RouterModule.forRoot(ROUTES, {useHash: false} );
