export interface Account {
    created: Object | string;
    gateways?: Gateway[]
}

export interface Gateway {
    uid: string,
    name: string,
    id: string
}