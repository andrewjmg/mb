import {Observable} from 'rxjs/Rx';

export class Gateway {

    constructor(public $key: string,
                public uid: string,
                public name: string,
                public id: string,
                public access_token) {

    }

    static fromJson({$key, uid, name, id, access_token}) {
        return new Gateway($key, uid, name, id, access_token);
    }

    static fromJsonArray(json: any[]): Gateway[] {
        return json.map(Gateway.fromJson);
    }

}
