import {Observable} from 'rxjs/Rx';

export class Revenue {

    constructor(public $key: string,
                public charges: Object,
                public history: Object) {

    }

    static fromJson({$key, charges, history}) {
        return new Revenue($key, charges, history);
    }

    static fromJsonArray(json: any[]): Revenue[] {
        return json.map(Revenue.fromJson);
    }

}
