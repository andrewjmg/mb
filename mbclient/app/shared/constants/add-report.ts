export const AddReport = {
  reports: [
    {
      id: 1,
      title: 'Net Revenue',
      description: 'Shows you a history of all failed charges',
      checked: false
    },
    {
      id: 2,
      title: 'Monthly Recurring Revenue',
      description: 'Shows you a history of all subscription upgrades',
      checked: false
    },
    {
      id: 3,
      title: 'Fees',
      description: 'Shows you a history of all failed charges',
      checked: false
    },
    {
      id: 4,
      title: 'Shows you a history of all failed charges',
      description: 'Shows you a history of all subscription upgrades',
      checked: false
    },
    {
      id: 5,
      title: 'Annual Run Rate',
      description: 'Shows you a history of all canceled subscriptions',
      checked: true
    }
  ]
};
