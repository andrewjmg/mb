export const RegisterConstants = {
  profile: {
    title: 'Profile Information',
    step: 1
  },
  plan: {
    title: 'Plan Selection',
    step: 2
  },
  billing: {
    title: 'Billing Information',
    step: 3
  },
  gateway: {
    title: 'Payment Gateway',
    step: 4
  },
  confirm: {
    title: 'Confirmation',
    msg: 'Does everything look correct?',
    step: 0
  },
  success: {
    step: 5
  },
  numSteps: '4',
  countries: [
    {
      id: 1,
      a2: 'mx',
      country: 'México'
    },
    {
      id: 2,
      as: 'nw',
      country: 'New York'
    }
  ],
  gateways: [
    {
      id: 1,
      name: 'paypal',
      displayName: 'PayPal'
    },
    {
      id: 5,
      name: 'stripe',
      displayName: 'Stripe'
    }
  ]
};
