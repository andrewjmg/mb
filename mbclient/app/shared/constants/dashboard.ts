export const dashboard = {
  time_ranges: [
    {id: 0, value: 30, name: 'Past Month'},
    {id: 1, value: 14, name: 'Last Two Weeks'},
    {id: 2, value: 7, name: 'Last Week'}
  ],
  card_labels: {
    transaction_success: {
      name: 'Net Revenue Report',
      abbreviation: 'Net Revenue'
    },
    transaction_fail: {
      name: 'Failed Charges',
      abbreviation: 'Failed Charges'
    },
    mrr: {
      name: 'Monthly Revenue Report',
      abbreviation: 'MRR'
    },
    arr: {
      name: 'Annual Run Rate',
      abbreviation: 'ARR'
    },
    arpu: {
      name: 'Average Revenue Per User',
      abbreviation: 'ARPU'
    },
    ltv: {
      name: 'Lifetime Value',
      abbreviation: 'LTV'
    },
    churn: {
      name: 'User Churn',
      abbreviation: 'Churn'
    },
    revenuechurn: {
      name: 'Revenue Churn',
      abbreviation: 'Revenue Churn'
    },
    subscription_new: {
      name: 'New Subscriptions',
      abbreviation: 'New Subscriptions'
    },
    subscription_upgrade: {
      name: 'Subscription Upgrades',
      abbreviation: 'Upgrades'
    },
    subscription_downgrade: {
      name: 'Subscription Downgrades',
      abbreviation: 'Downgrades'
    },
    subscription_cancel: {
      name: 'Subscription Cancellations',
      abbreviation: 'Cancellations'
    },
    fee: {
      name: 'Fees',
      abbreviation: 'Fees'
    },
    refund: {
      name: 'Refunds',
      abbreviation: 'Refunds'
    }
  }
}
