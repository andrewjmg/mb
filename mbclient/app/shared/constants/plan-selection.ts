export const PlanSelection = {
  plans: {
    monthly: {
      type: 'monthly',
      list:
        [
          {
            id: 1,
            cost: 0,
            description: 'Basic'
          },
          {
            id: 2,
            cost: 25,
            description: 'Plus'
          },
          {
            id: 3,
            cost: 99,
            description: 'Pro'

          },
        ]
    },
    yearly: {
      type: 'yearly',
      list:
        [
          {
            id: 1,
            cost: 0,
            description: 'Basic'
          },
          {
            id: 2,
            cost: 99,
            description: 'Plus'
          },
          {
            id: 3,
            cost: 149,
            description: 'Pro'

          },
        ]
    },
  }
};
