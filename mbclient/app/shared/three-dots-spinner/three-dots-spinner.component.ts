import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-three-dots-spinner',
  template: `
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  `,
  styleUrls: ['./three-dots-spinner.component.css']
})
export class ThreeDotsSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
