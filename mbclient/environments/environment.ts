// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  apiUrl: 'http://localhost:4300/api',
  firebase: {
    apiKey: 'AIzaSyAOFWqg7dEOAhiopgHxgq4RRv0D61up0X0',
    authDomain: 'fsaas-8d8e7.firebaseapp.com',
    databaseURL: 'https://fsaas-8d8e7.firebaseio.com',
    projectId: 'fsaas-8d8e7',
    storageBucket: 'fsaas-8d8e7.appspot.com',
    messagingSenderId: '127972656561'
  },
  stripe: {
    oauthUrl: 'https://connect.stripe.com/oauth/authorize?response_type=code&amp;client_id=ca_ANU567fBY1TL0vO3XjVFijZ9spcQdnEy&amp;scope=read_write&redirect_uri=http://localhost:4200/settings/gateways/stripe/callback',
    addOAuth: 'https://connect.stripe.com/oauth/authorize?response_type=code&amp;client_id=ca_ANU567fBY1TL0vO3XjVFijZ9spcQdnEy&amp;scope=read_write&redirect_uri=http://localhost:4200/account/gateway/stripe/callback'
  },
  paypal:
    {
      oauthUrl: 'https://www.sandbox.paypal.com/signin/authorize?client_id=AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy&response_type=code&scope=openid&redirect_uri=http://sdev.metricbeast.io/settings/gateways/paypal/callback',
      addOAuth: 'https://www.sandbox.paypal.com/signin/authorize?client_id=AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy&response_type=code&scope=openid&redirect_uri=http://sdev.metricbeast.io/account/gateway/paypal/callback',
      sandbox_url: "https://www.sandbox.paypal.com/signin/authorize",
      live_url: "https://www.paypal.com/signin/authorize",
      client_id: 'AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy',
      scope: "openid",
      redirect_uri: 'http://sdev.metricbeast.io/settings/gateways/paypal/callback', // redirect_uri: "http://sdev-api.metricbeast.io/api/gateways/paypal/token",
      redirect_add_uri: 'http://sdev.metricbeast.io/account/gateway/paypal/callback'
    }
};
