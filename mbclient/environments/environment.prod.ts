export const environment = {
  production: true,
  apiUrl: 'https://api.metricbeast.io/api',
  firebase: {
    apiKey: 'AIzaSyAOFWqg7dEOAhiopgHxgq4RRv0D61up0X0',
    authDomain: 'fsaas-8d8e7.firebaseapp.com',
    databaseURL: 'https://fsaas-8d8e7.firebaseio.com',
    projectId: 'fsaas-8d8e7',
    storageBucket: 'fsaas-8d8e7.appspot.com',
    messagingSenderId: '127972656561'
  },
  stripe : {
    oauthUrl: 'https://connect.stripe.com/oauth/authorize?response_type=code&amp;client_id=ca_ANU567fBY1TL0vO3XjVFijZ9spcQdnEy&amp;scope=read_write&redirect_uri=http://sdev.metricbeast.io/settings/gateways/stripe/callback',
    addOAuth: 'https://connect.stripe.com/oauth/authorize?response_type=code&amp;client_id=ca_ANU567fBY1TL0vO3XjVFijZ9spcQdnEy&amp;scope=read_write&redirect_uri=http://sdev.metricbeast.io/account/gateway/stripe/callback'
  },
  paypal:
    {
      oauthUrl: 'https://www.sandbox.paypal.com/signin/authorize?client_id=AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy&response_type=code&scope=openid&redirect_uri=http://sdev.metricbeast.io/settings/gateways/paypal/callback',
      addOAuth: 'https://www.sandbox.paypal.com/signin/authorize?client_id=AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy&response_type=code&scope=openid&redirect_uri=http://sdev.metricbeast.io/account/gateway/paypal/callback',
      sandbox_url: "https://www.sandbox.paypal.com/signin/authorize",
      live_url: "https://www.paypal.com/signin/authorize",
      client_id: 'AWkbzrkhtVO9FUhWTF3TU69ZdUtp8Hk3TiUT0BxkXF93ftNL1I-eRs5JQDbxk5ch2H0bfCiWqbDRMQcy',
      scope: "openid",
      redirect_uri: 'http://sdev.metricbeast.io/settings/gateways/paypal/callback', // redirect_uri: "http://sdev-api.metricbeast.io/api/gateways/paypal/token"
      redirect_add_uri: 'http://sdev.metricbeast.io/account/gateway/paypal/callback'
    }
};
