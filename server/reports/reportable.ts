import * as moment from 'moment';
import {database} from 'firebase-admin';


export class Reportable {
    _schema: string;
    _ref: string;
    _entity: string;
    _accountId = null;
    _uid: any;
    daysRange;
    gatewayKey;
    from;
    to;
    custom;

    meta: {
        params: {
            from: number,
            to: number
        },
        created: number
    };


    constructor(obj) {

    }

    public sanitize() {
        let object = Object.assign({}, this);
        delete object._schema;
        delete object._entity;
        delete object._ref;
        delete object._accountId;
        delete object._uid;
        delete object.daysRange;
        delete object.gatewayKey;
        delete object.from;
        delete object.to;
        delete object.custom;
        
        return object;

    }
}
