import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ETransactionStatus, IRevenueReport} from './../models/abstracts';
import * as moment from 'moment';
import {dataTypes, Sequelize} from "sequelize";
import {sequelize} from "../models/index";
import * as config from "./../config/config";
import {Helper} from "../lib/helper";
import {ReportsUtils} from "../lib/reports-utils";

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class RevenueChurn extends Reportable {

  summary = {
    total: null
  };
  kpi = {
    current: null,
    monthToDate: {
      total: null
    },
    monthAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  history = [];
  byPlan = [];

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    const schemaRef = database().ref(`reports/${this.gatewayKey}/revenuechurn`);
    return schemaRef.transaction(() => {

      database().ref(`reports/${this.gatewayKey}/revenuechurn/summary/${this.daysRange}`)
        .set(this.summary)
      database().ref(`reports/${this.gatewayKey}/revenuechurn/kpi`)
        .set(this.kpi);
      database().ref(`reports/${this.gatewayKey}/revenuechurn/history/${this.daysRange}`)
        .set(this.history);
      database().ref(`reports/${this.gatewayKey}/revenuechurn/byPlan/${this.daysRange}`)
        .set(this.byPlan);
    });
  }

  public generate(args = null): Promise<any> {
    const today = Helper.today();
    this.daysRange = args.daysRange;
    const promises = [
      this.updateRevenueChurnTotal(today),
      this.updateKpi(),
      this.updateHistory()
    ];
    return Promise.all(promises).then(promise => {
      this.save();
    });
  }

  public generateCustom(from: number, to: number) : Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;

    const promises = [
      this.updateRevenueChurnTotal(Helper.today()),
      this.updateKpi(),
      this.updateHistory()
    ];

    return Promise.all(promises).then(_ => {
      const {summary, kpi, history, byPlan} = this;
      return {
        summary,
        kpi,
        history,
        byPlan
      };
    });
  }

  public updateRevenueChurnTotal(today: string): Promise<any> {
    const query = `
      SELECT
        COALESCE(SUM(revenueChurnRate), 0) as total,
        planId,
        planName
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
      AND date='${today}'
      GROUP BY planId
    `;

    return sequelize.query(query, SELECT)
      .then(revenuechurn => {
        const total = revenuechurn.reduce((acc, plan) => plan.total + acc, 0);
        this.summary.total = total;
        return this.updateByPlan(revenuechurn, total);
      });
  }

  public updateKpi(): Promise<any> {
    const query = `
      SELECT COALESCE(SUM(revenueChurnRate), 0) as total
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
    `;

    return ReportsUtils.updateKpis(query, {exactDates: true}).then(kpi => {
      this.kpi = kpi;
    });
  }

  public updateHistory(): Promise<any> {
    const {from, to} = this;
    const query = `
    SELECT
      COALESCE(SUM(Datawarehouse.revenueChurnRate), 0) as amount,
      calendar_table.dt as date
    FROM calendar_table
    LEFT JOIN (SELECT * FROM Datawarehouse WHERE gatewayKey='${this.gatewayKey}') Datawarehouse
    ON Datawarehouse.date = calendar_table.dt
    WHERE 
      calendar_table.dt BETWEEN
        FROM_UNIXTIME(${from})
      AND
        FROM_UNIXTIME(${to}) GROUP BY calendar_table.dt;
    `;
    return ReportsUtils.getHistory(query)
      .then(history => {
        this.history = history;
      });
  }

  public updateByPlan(plans, total: number): Promise<any> {
    this.byPlan = plans.map(plan => {
      plan.percentage = ((plan.total / total) * 100)
      return plan
    }).filter(plan => plan.total !== 0);;
    return Promise.resolve();
  }
}
