import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ETransactionStatus, IRevenueReport} from './../models/abstracts';
import * as moment from 'moment';
import {dataTypes, Sequelize} from "sequelize";
import {sequelize} from "../models/index";
import * as config from "./../config/config";
import {Helper} from "../lib/helper";
import {ReportsUtils} from "../lib/reports-utils";

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class UserChurn extends Reportable {

  summary = {
    total: null
  };
  kpi = {
    current: null,
    monthToDate: {
      total: null
    },
    monthAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  history = [];
  byPlan = [];

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    const schemaRef = database().ref(`reports/${this.gatewayKey}/churn`);
    return schemaRef.transaction(() => {

      database().ref(`reports/${this.gatewayKey}/churn/summary/${this.daysRange}`)
        .set(this.summary)
      database().ref(`reports/${this.gatewayKey}/churn/kpi`)
        .set(this.kpi);
      database().ref(`reports/${this.gatewayKey}/churn/history/${this.daysRange}`)
        .set(this.history);
      database().ref(`reports/${this.gatewayKey}/churn/byPlan/${this.daysRange}`)
        .set(this.byPlan);
    });
  }

  public generate(args = null): Promise<any> {
    this.daysRange = args.daysRange;
    const today = Helper.today();
    const promises = [
      this.updateChurnTotal(today),
      this.updateKpi(),
      this.updateHistory()
    ];
    return Promise.all(promises).then(promise => {
      this.save();
    });
  }

  public generateCustom(from: number, to: number): Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;
    const promises = [
      this.updateChurnTotal(Helper.today()),
      this.updateKpi(),
      this.updateHistory()
    ];
    return Promise.all(promises).then(promise => {
      const {summary, kpi, history, byPlan} = this;
      return {
        summary,
        kpi,
        history,
        byPlan
      };
    });
  }

  public updateChurnTotal(today: string): Promise<any> {
    const query = `
      SELECT
        COALESCE(SUM(churnRate), 0) as total,
        planId,
        planName
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
      AND date='${today}'
      GROUP BY planId
    `;

    return sequelize.query(query, SELECT)
      .then(churn => {
        const total = churn.reduce((acc, plan) => plan.total + acc, 0);
        this.summary.total = total;
        return this.updateByPlan(churn, total);
      });
  }

  public updateKpi(): Promise<any> {
    const query = `
      SELECT COALESCE(SUM(churnRate), 0) as total
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
    `;

    return ReportsUtils.updateKpis(query, {exactDates: true}).then(kpi => {
      this.kpi = kpi;
    });
  }

  public updateHistory(): Promise<any> {
    const query = `
    SELECT
      COALESCE(SUM(Datawarehouse.churnRate), 0) as amount,
      calendar_table.dt as date
    FROM calendar_table
    LEFT JOIN (SELECT * FROM Datawarehouse WHERE gatewayKey='${this.gatewayKey}') Datawarehouse
    ON Datawarehouse.date = calendar_table.dt
    WHERE 
      calendar_table.dt BETWEEN
        FROM_UNIXTIME(${this.from}) 
      AND
        FROM_UNIXTIME(${this.to})  GROUP BY calendar_table.dt;
    `;
    return ReportsUtils.getHistory(query)
      .then(history => {
        this.history = history;
      });
  }

  private groupNewSubscriptionsCountByMonth() : Promise<any> {
    const query = `
      SELECT
        YEAR(FROM_UNIXTIME(date)) as year,
        MONTHNAME(FROM_UNIXTIME(date)) as month,
        COALESCE(SUM(new_count),0) as count
      FROM MRR
      WHERE gatewayKey = '${this.gatewayKey}'
      AND DATE_ADD(Now(), Interval -1 YEAR) 
      GROUP BY YEAR(FROM_UNIXTIME(date)), MONTH(FROM_UNIXTIME(date))
      ORDER BY year, month DESC
    `;

    return sequelize.query(query, SELECT);
  }

  private groupActiveCountByMonth() : Promise<any> {
    const query = `
    SELECT
       Subscriptions.gatewayName AS gatewayName,
       Subscriptions.gatewayKey AS gatewayKey,
       Subscriptions.gatewayId AS gatewayId,
             YEAR(FROM_UNIXTIME(date)) AS year,
            MONTHNAME(FROM_UNIXTIME(date)) AS month,
        CAST(from_unixtime(Subscriptions.date) AS date) AS DATE,
       Subscriptions.planId AS planId,
       Subscriptions.planName AS planName,
       SUM(Subscriptions.planAmount) AS total,
       count(*) AS count_all
    FROM Subscriptions 
    WHERE (Subscriptions.date IN (select max(Subscriptions.date) FROM Subscriptions GROUP BY Subscriptions.gatewayId) 
      AND (Subscriptions.status <> 'Inactive')) 
    GROUP BY
       YEAR(FROM_UNIXTIME(date)), MONTH(FROM_UNIXTIME(date))
          ORDER BY year, month DESC;
          `;

    return sequelize.query(query, SELECT);
  }

  private updateByPlan(plans, total: number): Promise<any> {
    this.byPlan = plans.map(plan => {
      plan.percentage = ((plan.total / total) * 100)
      return plan
    }).filter(plan => plan.total !== 0);
    return Promise.resolve();
  }
}
