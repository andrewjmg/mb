import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ETransactionStatus, IRevenueReport} from './../models/abstracts';
import * as moment from 'moment';
import {dataTypes, Sequelize} from "sequelize";
import {sequelize} from "../models/index";
import * as config from "./../config/config";
import {ReportsUtils} from "../lib/reports-utils";
import {Helper} from '../lib/helper';

const successStatus = ETransactionStatus[ETransactionStatus.Success];
const failStatus = ETransactionStatus[ETransactionStatus.Fail];

export class Transaction extends Reportable {

  charges = {
    total: null,
    count: null,
    average: null
  };
  kpi = {
    monthToDate: {
      total: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  recent = [];
  revenue = {};
  byPlan = [];
  type;

  // refunds, etc come later

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
    if (obj.type == successStatus) {
      this.type = successStatus;
    } else if (obj.status == failStatus) {
      this.type = failStatus;
    }
  }

  public save(): Promise<any> {
    console.log('Saving reports to Firebase.');
    const schemaRef = database().ref(`reports/${this.gatewayKey}/transaction`);
    const type = `transaction_${this.type.toLowerCase()}`;
    return schemaRef.transaction(() => {

      database().ref(`reports/${this.gatewayKey}/${type}/summary/${this.daysRange}`)
        .set(this.charges);

      database().ref(`reports/${this.gatewayKey}/${type}/kpi`)
        .set(this.kpi);

      if (this.type == successStatus) {
        database().ref(`reports/${this.gatewayKey}/${type}`)
          .update({recent: this.recent});
      }

      database().ref(`reports/${this.gatewayKey}/${type}/history/${this.daysRange}`)
        .set(this.revenue);

      if (this.type == successStatus) {
        database().ref(`reports/${this.gatewayKey}/${type}/byPlan/${this.daysRange}`)
          .set(this.byPlan);
      }
    });
  }

  public generate(args = null): Promise<any> {
    this.daysRange = args.daysRange;
    ({from: this.from, to: this.to } = Helper.getDatesRanges(this.daysRange));

    const promises = [
      this.updateChargesCount(),
      this.updateChargesAverage(),
      this.updateChargesTotal(),
      this.updateChargesHistory(),
      this.updateKpi(),
      this.updateRecent(config.settings.recentHistoryLimit),
      this.updateByPlan()
    ];

    return Promise.all(promises).then(promise => {
      this.save();
    });
  }

  public generateCustom(from: number, to: number) : Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;

    const promises = [
    this.updateChargesCount(),
    this.updateChargesAverage(),
    this.updateChargesTotal(),
    this.updateChargesHistory(),
    this.updateKpi(),
    this.updateRecent(config.settings.recentHistoryLimit),
    this.updateByPlan()
    ];

    return Promise.all(promises).then(_ => {
      const {charges: summary, kpi, recent, revenue: history, byPlan} = this;
      return {
        summary,
        kpi,
        recent,
        history,
        byPlan
      };
    });
  }

  public updateByPlan(): Promise<any> {
    const {type} = this;
    const query = `
      SELECT COUNT(*) as count, s.planId as planId,s.planName as planName ,sum(t.amount) as total
      FROM Transactions t JOIN Invoices i ON i.gatewayId = t.gatewayInvoiceId
      JOIN Subscriptions s ON s.gatewayId = i.gatewaySubscriptionid
      WHERE t.gatewayKey = '${this.gatewayKey}'
      AND t.status = '${type}'
      AND (t.date BETWEEN '${this.from}' AND '${this.to}')
      GROUP BY s.planId, s.planName`;

      return sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
        .then(transactions => {
          let sum = transactions.reduce((acc, t) => acc + t.total, 0);
          this.byPlan = transactions.map(row => {
            let {planName, total, count, planId} = row;
            let percentage = ((total / sum) * 100);
            return {
              count,
              planName,
              planId,
              total,
              percentage
            };
          });
        });
  }

  public updateRecent(limit: number = 10): Promise<any> {
    const {type} = this;
    let query = `
      SELECT *
      FROM Transactions
      WHERE gatewayKey = '${this.gatewayKey}'
      AND status='${type}'
      ORDER BY date DESC
      LIMIT ${limit}
    `;
    return sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
      .then(transactions => {
        this.recent = transactions;
    });
  }

  public updateKpi(): Promise<any> {
    const {type} = this;
    const total = type === successStatus ? 'SUM(amount)' : 'COUNT(*)';
    let query = `
    SELECT ${total} as total
    FROM Transactions
    WHERE gatewayKey = '${this.gatewayKey}'
    AND status='${type}'
    `;
    return ReportsUtils.updateKpis(query).then(kpi => {
      this.kpi = kpi;
    });
  }

  public updateChargesCount(): Promise<any> {
    const {type} = this;
    let query = `
      SELECT count(id) as count
      FROM Transactions
      WHERE gatewayKey = '${this.gatewayKey}'
      AND status='${type}'
      AND (date BETWEEN '${this.from}' AND '${this.to}')
    `;

    return sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
      .then(transactions => {
        this.charges.count = transactions[0].count || 0;
      });
  }

  public updateChargesAverage(): Promise<any> {
    const {type} = this;
    let query = `
      SELECT AVG(amount) as average
      FROM Transactions
      WHERE gatewayKey = '${this.gatewayKey}'
      AND status='${type}'
      AND (date BETWEEN '${this.from}' AND '${this.to}')
    `;
    return sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
      .then(transactions => {
        this.charges.average = Math.round(transactions[0].average) || 0;
    });
  }

  public updateChargesTotal(): Promise<any> {
    const {type} = this;
      let query = `
      SELECT SUM(amount) as total
      FROM Transactions
      WHERE gatewayKey = '${this.gatewayKey}'
      AND status='${type}'
      AND (date BETWEEN '${this.from}' AND '${this.to}')
    `;
    return sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
      .then(transactions => {
        this.charges.total = transactions[0].total || 0;
    });
  }

  public updateChargesHistory(): Promise<any> {
    const {type} = this;
    let amount = type == successStatus ? 'SUM(amount)' : 'COUNT(*)';
    let query = `
      SELECT   DATE(FROM_UNIXTIME(date)) as date,
      ${amount} as amount
      FROM Transactions
      WHERE gatewayKey = '${this.gatewayKey}'
      AND status='${type}'
      AND (date BETWEEN '${this.from}' AND '${this.to}')
      GROUP BY DATE(FROM_UNIXTIME(date))
    `;

    return ReportsUtils.getHistory(query)
      .then(history => {
        this.revenue = history;
    });
  }
}
