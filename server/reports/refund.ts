import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {models, sequelize} from "../models/index";
import * as config from "./../config/config";
import {ReportsUtils} from "../lib/reports-utils";
import {Helper} from '../lib/helper';

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class Refund extends Reportable {

  summary = {
    total: null,
    count: null
  };
  kpi = {
    monthToDate: {
      total: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  history;

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    let schemaRef = database().ref(`reports/${this.gatewayKey}/refunds`);
    return schemaRef.transaction(() => {
      database().ref(`reports/${this.gatewayKey}/refund/summary/${this.daysRange}`)
        .set(this.summary);

      database().ref(`reports/${this.gatewayKey}/refund/kpi`)
        .set(this.kpi);

      database().ref(`reports/${this.gatewayKey}/refund/history/${this.daysRange}`)
        .set(this.history);
    });
  }

  public generate(args = null): Promise<any> {
    this.daysRange = args.daysRange;
    ({from: this.from, to: this.to } = Helper.getDatesRanges(this.daysRange));

    const promises = [
      this.updateSummary(),
      this.updateHistory(),
      this.updateKpi()
    ];

    return Promise.all(promises).then(promise => {
      this.save();
    });
  }

  public generateCustom(from: number, to: number) : Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;

    const promises = [
      this.updateSummary(),
      this.updateKpi(),
      this.updateHistory()
    ];

    return Promise.all(promises).then(_ => {
      const {summary, kpi, history} = this;
      return {
        summary,
        kpi,
        history
      };
    });
  }

  public updateSummary() : Promise<any> {
    const {from, to} = this;
    const query = `
      SELECT 
        COALESCE(COUNT(*), 0) as count,
        COALESCE(SUM(amount), 0) as amount
      FROM Refunds
      WHERE gatewayKey='${this.gatewayKey}'
      AND date BETWEEN ${from} AND ${to}
    `;
    return sequelize.query(query, SELECT)
      .then(results => {
        const [refunds] = results;
        this.summary.total = refunds.total;
        this.summary.count = refunds.count;
      });
  }

  public updateHistory() : Promise<any> {
    const {from, to} = this;
    const query = `
      SELECT
        COALESCE(SUM(amount), 0) as amount,
        calendar_table.dt as date
      FROM calendar_table
      LEFT JOIN (SELECT * FROM Refunds WHERE gatewayKey='${this.gatewayKey}') R
      ON FROM_UNIXTIME(R.date) = calendar_table.dt
      WHERE 
        calendar_table.dt BETWEEN
          FROM_UNIXTIME(${from})
        AND
          FROM_UNIXTIME(${to}) GROUP BY calendar_table.dt;
    `;
    return ReportsUtils.getHistory(query)
      .then(history => {
        this.history = history;
      });
  }

  public updateKpi() : Promise<any> {
    const query = `
      SELECT 
        COALESCE(SUM(amount), 0) as total
      FROM Refunds
      WHERE gatewayKey='${this.gatewayKey}'
      AND Type='New'
    `;
    return ReportsUtils.updateKpis(query)
      .then(kpi => {
        this.kpi = kpi;
      });
  }
}
