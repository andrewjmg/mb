import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ESubscriptionStatus, ESubscriptionInterval} from './../models/abstracts';
import * as moment from 'moment';
import {Transaction} from '../models/transaction';
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import * as config from "./../config/config";
import {ReportsUtils} from "../lib/reports-utils";
import {Helper} from '../lib/helper';

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class Stream extends Reportable {

  streams = [];


  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    const schemaRef = database().ref(`streams/${this.gatewayKey}/`);
    return schemaRef.transaction(() => {
      database().ref(`streams/${this.gatewayKey}/`)
        .set(this.streams);
    });
  }

  public generate(args = null): Promise<any> {
    this.gatewayKey = args.gatewayKey;
    this.daysRange = args.daysRange;
    ({from: this.from, to: this.to} = Helper.getDatesRanges(this.daysRange));

    const promises = [
      this.updateStream(this.from, this.to, this.gatewayKey)
    ];

    return Promise.all(promises).then(promise => {
      this.save();
    });
  }


  public updateStream(from: number, to: number, gatewayKey): Promise<any> {
    const query = `
      SELECT
        *
      FROM Streams
      WHERE 
        gatewayKey = '${this.gatewayKey}'
        AND date BETWEEN
        from_unixtime(${from})
        AND
        from_unixtime(${to})
        ORDER by date DESC;
    `;

    return sequelize.query(query, SELECT).then(results => {
      return results.map(row => {
        this.streams.push({
          date: new Date(row.date).toISOString(),
          event: `Processed ${row.entityType} in the amount of ${Helper.currencyFormat(row.amount, row.currency)}`
        });
      })
    });
  }
}
