import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ETransactionStatus, IRevenueReport} from './../models/abstracts';
import * as moment from 'moment';
import {dataTypes, Sequelize} from "sequelize";
import {sequelize} from "../models/index";
import * as config from "./../config/config";
import {Helper} from "../lib/helper";
import {ReportsUtils} from "../lib/reports-utils";

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class Arr extends Reportable {

  summary = {
    total: null
  };
  kpi = {
    current: null,
    monthToDate: {
      total: null
    },
    monthAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  byPlan = [];
  table = [];
  history = [];
  
  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    const schemaRef = database().ref(`reports/${this.gatewayKey}/arr`);
    return schemaRef.transaction(() => {
      database().ref(`reports/${this.gatewayKey}/arr/summary/${this.daysRange}`)
        .set(this.summary);
      database().ref(`reports/${this.gatewayKey}/arr/history/${this.daysRange}`)
        .set(this.history);
      database().ref(`reports/${this.gatewayKey}/arr/kpi`)
        .set(this.kpi);
      database().ref(`reports/${this.gatewayKey}/arr/byPlan/${this.daysRange}`)
        .set(this.byPlan);
    });
  }

  public generate(gatewayKey: string, daysRange: number, data): Promise<any> {
    this.gatewayKey = gatewayKey;
    this.daysRange = daysRange;
    this.updateArrSummary(data.summary);
    this.updateArrKpi(data.kpi);
    this.updateByPlan(data.byPlan);
    this.updateHistory(data.history);
    return this.save();
  }

  public generateCustom(data) {
    this.custom = true;
    this.updateArrSummary(data.summary);
    this.updateArrKpi(data.kpi);
    this.updateByPlan(data.byPlan);
    this.updateHistory(data.history);
    const {summary, kpi, history, byPlan} = this;
    return {summary, kpi, history, byPlan}
  }

  public updateArrSummary(summary) {
    summary.total = summary.total * 12;
    delete summary.average;
    this.summary = summary;
  }

  public updateHistory(history) {
    this.history = history.map(day => {
      day.y = day.y * 12;
      return day;
    });
  }

  public updateArrKpi(kpi) {
    this.kpi = kpi;
    this.kpi.current = kpi.current * 12;

    this.kpi.monthAgo.total = kpi.monthAgo.total * 12;
    this.kpi.monthAgo.difference = kpi.monthAgo.difference * 12;

    this.kpi.threeMonthsAgo.total = kpi.threeMonthsAgo.total * 12;
    this.kpi.threeMonthsAgo.difference = kpi.threeMonthsAgo.difference * 12;

    this.kpi.sixMonthsAgo.total = kpi.sixMonthsAgo.total * 12;
    this.kpi.sixMonthsAgo.difference = kpi.sixMonthsAgo.difference * 12;

    this.kpi.twelveMonthsAgo.total = kpi.twelveMonthsAgo.total * 12;
    this.kpi.twelveMonthsAgo.difference = kpi.twelveMonthsAgo.difference * 12;
  }

  public updateByPlan(byPlan) {
    this.byPlan = byPlan.map(plan => {
      plan.total = plan.total * 12;
      return plan;
    });
  }
}
