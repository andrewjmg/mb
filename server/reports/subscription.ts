import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ESubscriptionStatus, ESubscriptionInterval} from './../models/abstracts';
import * as moment from 'moment';
import {Transaction} from '../models/transaction';
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import * as config from "./../config/config";
import {ReportsUtils} from "../lib/reports-utils";
import {Helper} from '../lib/helper';

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class Subscription extends Reportable {

  subscriptions = {
    total: null,
    count: null,
    average: null
  };
  kpi = {
    monthToDate: {
      total: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  history;
  byPlan = [];
  type = null;
  type_match = null;

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
    if (obj.type === 'New') {
      this.type_match = 'new_count'
    } else if (obj.type === 'Upgrade') {
      this.type_match = 'expansion_count'
    } else if (obj.type === 'Downgrade') {
      this.type_match = 'contraction_count'
    } else if (obj.type === 'Cancel') {
      this.type_match = 'churn_count'
    }
  }

  public save(): Promise<any> {
    let type = `subscription_${this.type.toLowerCase()}`;
    let schemaRef = database().ref(`reports/${this.gatewayKey}/${type}`);
    return schemaRef.transaction(() => {
      database().ref(`reports/${this.gatewayKey}/${type}/summary/${this.daysRange}`)
        .set(this.subscriptions);

      database().ref(`reports/${this.gatewayKey}/${type}/kpi`)
        .set(this.kpi);

      database().ref(`reports/${this.gatewayKey}/${type}/history/${this.daysRange}`)
        .set(this.history);

      database().ref(`reports/${this.gatewayKey}/${type}/byPlan/${this.daysRange}`)
        .set(this.byPlan);
    });
  }

  public generate(args = null): Promise<any> {
    this.daysRange = args.daysRange;
    ({from: this.from, to: this.to } = Helper.getDatesRanges(this.daysRange));

    const promises = [
      this.updateSummary(),
      this.updateHistory(),
      this.updateKpi()
    ];

    return Promise.all(promises).then(promise => {
      this.save();
    });
  }

  public generateCustom(from: number, to: number) : Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;

    const promises = [
      this.updateSummary(),
      this.updateKpi(),
      this.updateHistory()
    ];

    return Promise.all(promises).then(_ => {
      const {subscriptions: summary, kpi, history, byPlan} = this;
      return {
        summary,
        kpi,
        history,
        byPlan
      };
    });
  }

  public updateSummary() : Promise<any> {
    const {from, to, type} = this;
    const query = `
      SELECT 
        planId,
        planName,
        COUNT(*) as count,
        SUM(planAmount) as amount
      FROM Subscriptions
      WHERE gatewayKey='${this.gatewayKey}'
      AND Type='${type}'
      AND date BETWEEN ${from} AND ${to}
      GROUP BY planId
    `;
    return sequelize.query(query, SELECT)
      .then(subscriptions => {
        const total = subscriptions.reduce((acc, plan) => plan.count + acc, 0);
        this.subscriptions.total = total;
        return this.updateByPlan(subscriptions, total);
      });
  }

  public updateHistory() : Promise<any> {
    const {from, to, type_match} = this;
    const query = `
      SELECT
        COALESCE(SUM(MRR.${type_match}), 0) as amount,
        calendar_table.dt as date
      FROM calendar_table
      LEFT JOIN (SELECT * FROM MRR WHERE gatewayKey='${this.gatewayKey}') MRR
      ON MRR.castedDate = calendar_table.dt
      WHERE 
        calendar_table.dt BETWEEN
          FROM_UNIXTIME(${from})
        AND
          FROM_UNIXTIME(${to}) GROUP BY calendar_table.dt;
    `;
    return ReportsUtils.getHistory(query)
      .then(history => {
        this.history = history;
      });
  }

  public updateKpi() : Promise<any> {
    const {type} = this;
    const query = `
      SELECT 
        COUNT(*) as total
      FROM Subscriptions
      WHERE gatewayKey='${this.gatewayKey}'
      AND Type='${type}'
    `;
    return ReportsUtils.updateKpis(query)
      .then(kpi => {
        this.kpi = kpi;
      });
  }

  private updateByPlan(subscriptions, total: number): Promise<any> {
    this.byPlan = subscriptions.map(row => {
      let {planName, planId, count, amount} = row;
      let percentage = ((count / total) * 100);
      return {
        planId,
        planName,
        count,
        amount,
        percentage
      };
    });
    return Promise.resolve();
  }
}
