import {Reportable} from './reportable';
import {database} from 'firebase-admin';
import {isNull, isNullOrUndefined} from 'util';
import {ETransactionStatus, IRevenueReport} from './../models/abstracts';
import * as moment from 'moment';
import {dataTypes, Sequelize} from "sequelize";
import {sequelize} from "../models/index";
import * as config from "./../config/config";
import {Helper} from "../lib/helper";
import {ReportsUtils} from "../lib/reports-utils";
import {Arr} from "./arr";

const SELECT = {type: sequelize.QueryTypes.SELECT};

export class Mrr extends Reportable {

  summary = {
    total: null,
    average: null
  };
  kpi = {
    current: null,
    monthToDate: {
      total: null
    },
    monthAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    threeMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    sixMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    },
    twelveMonthsAgo: {
      total: null,
      difference: null,
      percentageChange: null
    }
  };
  table = [];
  byPlan = [];
  history = [];
  growth = [];

  constructor(obj = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(): Promise<any> {
    const schemaRef = database().ref(`reports/${this.gatewayKey}/mrr`);
    return schemaRef.transaction(() => {

      database().ref(`reports/${this.gatewayKey}/mrr/summary/${this.daysRange}`)
        .set(this.summary)

      database().ref(`reports/${this.gatewayKey}/mrr/history/${this.daysRange}`)
        .set(this.history);

      database().ref(`reports/${this.gatewayKey}/mrr/byPlan/${this.daysRange}`)
        .set(this.byPlan);

      database().ref(`reports/${this.gatewayKey}/mrr/kpi`)
        .set(this.kpi);

      database().ref(`reports/${this.gatewayKey}/mrr/growth`)
        .set(this.growth);

      database().ref(`reports/${this.gatewayKey}/mrr/table/${this.daysRange}`)
        .set(this.table);
    })
    .then(() => {
      let {kpi, summary, history, byPlan} = this;
      const arr = new Arr();
      return arr.generate(this.gatewayKey, this.daysRange, {kpi, summary, history, byPlan});
    });
  }

  public generate(args = null): Promise<any> {
    this.daysRange = args.daysRange;
    ({from: this.from, to: this.to } = Helper.getDatesRanges(this.daysRange));
    
    const promises = [
      this.updateMrrTotal(),
      this.updateKpi(),
      this.updateHistory(),
      this.updateMrrGrowth(),
      this.updateMrrTable()
    ];
    return Promise.all(promises).then(promise => {
      return this.save();
    });
  }

  public generateCustom(from: number, to: number) : Promise<any> {
    this.from = from;
    this.to = to;
    this.custom = true;

    const promises = [
      this.updateMrrTotal(),
      this.updateKpi(),
      this.updateHistory(),
      this.updateMrrGrowth(),
      this.updateMrrTable()
    ];

    return Promise.all(promises).then(_ => {
      const {summary, kpi, table, byPlan, history, growth} = this;
      const arr = new Arr();
      const arrReport = arr.generateCustom({summary, kpi, byPlan, history});
      return {
        summary,
        kpi,
        table,
        byPlan,
        history,
        growth,
        arrReport
      };
    });
  }

  public updateKpi(): Promise<any> {
    let query = `
      SELECT SUM(mrrTotal) as total
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
    `;
    return ReportsUtils.updateKpis(query, {fromUnixTime: true}).then(kpi => {
      this.kpi = kpi;
    });
  }

  public updateMrrTotal(): Promise<any> {
    const query = `
      SELECT
        SUM(mrrTotal) as total,
        planId,
        planName
      FROM Datawarehouse
      WHERE gatewayKey = '${this.gatewayKey}'
      AND (date BETWEEN FROM_UNIXTIME(${this.from}) AND FROM_UNIXTIME(${this.to}))
      GROUP BY planId
    `;

    return sequelize.query(query, SELECT)
      .then(mrr => {
        let total = mrr.reduce((acc,plan) => acc + plan.total, 0);
        this.summary.total = total || 0;
        return Promise.all([this.updateMrrByPlan(mrr, total), this.updateMrrAverage(total)]);
      });
  }

  public updateMrrAverage(total: number) : Promise<any> {
    let average = total / this.daysRange;
    this.summary.average = Number(average.toFixed(2));
    return Promise.resolve();
  }

  public updateHistory() : Promise<any> {
    const {from, to} = this;
    const query = `
    SELECT
      COALESCE(SUM(Datawarehouse.mrrTotal), 0) as amount,
      calendar_table.dt as date
    FROM calendar_table
    LEFT JOIN (SELECT * FROM Datawarehouse WHERE gatewayKey='${this.gatewayKey}') Datawarehouse
    ON Datawarehouse.date = calendar_table.dt
    WHERE
      calendar_table.dt BETWEEN
        FROM_UNIXTIME(${from})
      AND
        FROM_UNIXTIME(${to}) GROUP BY calendar_table.dt;
    `;
    return ReportsUtils.getHistory(query)
      .then(history => {
        this.history = history;
      });
  }

  public updateMrrGrowth(): Promise<any> {
    const aYearAgo = moment().utc().subtract(1, 'year').unix();
    const query = `
      SELECT
        YEAR(FROM_UNIXTIME(date)) as year,
        MONTHNAME(FROM_UNIXTIME(date)) as month,
        COALESCE(SUM(new),0) as new,
        COALESCE(SUM(expansion),0) as expansion,
        COALESCE(SUM(contraction),0) as contraction,
        COALESCE(SUM(churn),0) as churn
      FROM MRR
      WHERE gatewayKey = '${this.gatewayKey}'
      AND date BETWEEN ${aYearAgo} AND ${this.to}
      GROUP BY YEAR(FROM_UNIXTIME(date)), MONTH(FROM_UNIXTIME(date))
      ORDER BY year, month DESC
    `;

    return sequelize.query(query, SELECT)
      .then(mrr => {
        this.growth = mrr.map(result => {
          let obj = {
            year: result.year,
            month: result.month,
            new: result.new || 0,
            expansion: result.expansion || 0,
            contraction: result.contraction || 0,
            churn: result.churn || 0,
            change: null
          };
          obj.change = obj.new + obj.expansion + obj.contraction + obj.churn;
          return obj;
        });
      });
  }

  public updateMrrTable() : Promise<any> {
    const query = `
      SELECT
        DATE(FROM_UNIXTIME(date)) as date,
        COALESCE(SUM(total), 0) as total,
        COALESCE(SUM(new), 0) as new,
        COALESCE(SUM(expansion), 0) as expansion,
        COALESCE(SUM(contraction), 0) as contraction,
        COALESCE(SUM(churn), 0) as churn
      FROM MRR
      WHERE gatewayKey = '${this.gatewayKey}'
      AND (date BETWEEN '${this.from}' AND '${this.to}')
      GROUP BY castedDate
    `;
    return sequelize.query(query, SELECT)
      .then(table => {
        this.table = table.map(day => {
          day.date = new Date(day.date).toISOString();
          let {expansion, contraction, churn} = day;
          day.change = day.new + expansion + contraction + churn;
          return day;
        });
      });
  }

  private updateMrrByPlan(mrrByPlan, total: number) : Promise<any> {
    this.byPlan = mrrByPlan.map(plan => {
      plan.percentage = ((plan.total / total) * 100)
      return plan
    }).filter(plan => plan.total !== 0);
    return Promise.resolve();
  }
}
