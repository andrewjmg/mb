import {StripeGateway} from './stripe-gateway';

export class GatewayAdapter {
    private _gw;

    constructor(gatewayConfig) {
        switch (gatewayConfig.key) {
            case "stripe": {
                this._gw = new StripeGateway(gatewayConfig);
                break;
            }
            default: {
                // Handle error of invalid GW
            }
        }
    }

    public getAdapter()
    {
        return this._gw;
    }

}

export interface IGatewayAdapter {
    authenticate(credentials);
}

