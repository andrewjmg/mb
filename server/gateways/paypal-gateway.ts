/**
 * Created by henry_ollarves on 6/19/17.
 */
import {IGatewayAdapter} from './gateway-adapter';
import {
  EInvoiceStatus, ESubscriptionStatus,
  ESubscriptionInterval, ESubscriptionType, ECurrency
} from "../models/abstracts";
import {Fee} from './../models/fee';
import {Invoice} from './../models/invoice';
import {Refund} from './../models/refund';
import {Subscription} from './../models/subscription';
import {Transaction} from './../models/transaction';
import * as moment from 'moment';


const accounting = require('accounting');


export class PaypalGateway implements IGatewayAdapter {

  private paypal;
  private _gatewayConfig;
  public name = 'PaypalGateway';
  public authenticate = null;

  public static mapDataToFee(response, gatewayKey) : Fee {
    const fee = new Fee();
    return fee;
  }

  public static mapDataToInvoice(response, gatewayKey) : Invoice {
    const invoice = new Invoice();

    invoice.gatewayName = this.name; // Paypal, Stripe, etc.
    invoice.gatewayKey = gatewayKey; // The firebase key for this gw
    invoice.gatewayId = response.subscr_id;  // The internal reference ID for this object at the gw. eg. Paypal Tx ID
    invoice.gatewayOriginalObject = JSON.stringify(response);
    invoice.date = moment().utc().unix(); // Should be date the message was received?
    invoice.amountDue = (response.amount3 * 100).toFixed(0); // Need to change in case overdue payment
    invoice.attemptCount = null; // PP doesn't keep track of retries
    invoice.attempted = 1; // Paypal doenst keep track of attempts.
    invoice.gatewayTransactionId = response.txn_id;
    invoice.currency = ECurrency[ECurrency[response.mc_currency.toLowerCase()]];
    invoice.amount = (response.mc_gross * 100).toFixed(0);
    invoice.planId = response.item_number;
    invoice.periodStart = moment(response.subscrb_date).utc().unix(); // Start date of the plan.
    invoice.periodEnd = moment(response.subscrb_date).add(30, 'days').utc().unix();
    invoice.gatewaySubscriptionId = response.subscr_id;

    switch (response.txn_type) {
      case "subscr_payment":
        invoice.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
      default:
        invoice.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
    }

    return invoice;
  }  

  public static mapDataToRefund(response, gatewayKey) : Refund {
    const refund = new Refund();
    return refund;
  }

  public static mapDataToSubscription(response, gatewayKey) : Subscription {
    const subscription = new Subscription();

    subscription.gatewayName = this.name; // Paypal, Stripe, etc.
    subscription.gatewayKey = gatewayKey; // The firebase key for this gw
    subscription.gatewayId = response.subscr_id;  // The internal reference ID for this object at the gw. eg. Paypal Tx ID
    subscription.gatewayOriginalObject = JSON.stringify(response);
    subscription.date = moment().utc().unix(); // Should be date the message was received?
    subscription.periodStart = moment(response.subscrb_date).utc().unix(); // Start date of the plan.
    subscription.periodEnd = moment(response.subscrb_date).add(30, 'days').utc().unix(); // Hardcoding 1 month since paypal doesn't seem to provide next period.
    subscription.planId = response.item_number;
    subscription.planName = response.item_name;
    subscription.planAmount = (response.amount3 * 100).toFixed(0); // amount3 is standard payment in pp. 2 and 1 are trials.
    subscription.currency = response.mc_currency.toLowerCase();

    if (response.bodyType === "updated") {
      subscription.previousPlanId = response.previous_attributes.planId;
      subscription.previousPlanName = response.previous_attributes.planName;
      subscription.previousPlanAmount = response.previous_attributes.planAmount;
      if (subscription.previousPlanAmount < subscription.planAmount) {
        subscription.type = ESubscriptionType[ESubscriptionType.Upgrade];
      } else if (subscription.previousPlanAmount > subscription.planAmount) {
        subscription.type = ESubscriptionType[ESubscriptionType.Downgrade];
      }
    } else if (response.bodyType == "created") {
      subscription.type = ESubscriptionType[ESubscriptionType.New];
    } else if (response.bodyType == "suspended") {
      subscription.type = ESubscriptionType[ESubscriptionType.Suspended];
    } else {
      subscription.type = ESubscriptionType[ESubscriptionType.Other];
    }

    switch (response.period3) {
      case "1 M":
        subscription.planInterval = ESubscriptionInterval[ESubscriptionInterval.Month];
        break;
      default:
        subscription.planInterval = null;
        break;
    }

    switch (response.bodyType) {
      case "CREATED":
        subscription.status = ESubscriptionStatus[ESubscriptionStatus.Active];
        break;
      default:
        subscription.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
    }
    return subscription;
  }

  public static mapDataToTransaction(response, gatewayKey) : Transaction {
    const transaction = new Transaction();
    return transaction;
  }

  constructor(gatewayConfig) {
    this._gatewayConfig = gatewayConfig.paypal;
  }
}

