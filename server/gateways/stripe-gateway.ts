import {IGatewayAdapter} from './gateway-adapter';
import {Transaction} from '../models/transaction';
import * as Abstracts from './../models/abstracts';
import {
  ECurrency, ETransactionStatus, IInvoice, EInvoiceStatus, ESubscriptionStatus,
  ESubscriptionInterval, ESubscriptionType
} from "../models/abstracts";
import {GatewayCustomer} from './../models/gateway-customer';
import * as moment from 'moment';
import * as config from '../config/config';
import {IGatewayCustomer} from "../models/abstracts";
import {isNullOrUndefined} from "util";
import {Invoice} from './../models/invoice';
import {Subscription} from './../models/subscription';
import {Fee} from './../models/fee';
import {Refund} from './../models/refund';
import {Helper} from './../lib/helper';
import {database} from 'firebase-admin';
import {eventHandler} from '../lib/event-handler';

const accounting = require('accounting');


export class StripeGateway implements IGatewayAdapter {
  private _stripe;
  private _gatewayConfig;
  public name = 'stripe';

  constructor(gatewayConfig) {
    this._gatewayConfig = gatewayConfig;
    this._stripe = this.authenticate(gatewayConfig.access_token);
  }

  public authenticate(credentials) {
    return require('stripe')(credentials);
  }

  public static mapDataToFee(response, gatewayKey) {
    let fee = new Fee();

    fee.gatewayName = this.name;
    fee.gatewayKey = gatewayKey;
    fee.gatewayId = response.id;
    fee.date = response.created;
    fee.gatewayOriginalObject = JSON.stringify(response);
    fee.gatewayTransactionId = response.source;
    fee.amount = response.fee;
    fee.transactionAmount = response.amount;
    fee.type = response.type;
    fee.refunded = response.refunded;

    return fee;
  }

  public static mapDataToSubscription(response, gatewayKey) {
    let subscription = new Subscription();
    /**
     * Todo:
     * - The gatewayid is not populating.
     * - Need to populate old tx id on updates
     *
     */

    subscription.gatewayName = this.name;
    subscription.gatewayKey = gatewayKey;
    subscription.gatewayId = response.object.id;
    subscription.gatewayOriginalObject = JSON.stringify(response);
    subscription.date = response.object.created;
    subscription.periodStart = response.object.current_period_start;
    subscription.periodEnd = response.object.current_period_end;
    subscription.planId = response.object.plan.id;
    subscription.planName = response.object.plan.name;
    subscription.planAmount = response.object.plan.amount;
    subscription.currency = response.object.plan.currency;


    if (response.bodyType == "updated") {
      const previousPlan = response.previous_attributes.plan || {};
      subscription.previousPlanId = previousPlan.id;
      subscription.previousPlanName = previousPlan.name;
      subscription.previousPlanAmount = previousPlan.amount;

      if (subscription.previousPlanAmount < subscription.planAmount) {
        subscription.type = ESubscriptionType[ESubscriptionType.Upgrade];
      } else if (subscription.previousPlanAmount > subscription.planAmount) {
        subscription.type = ESubscriptionType[ESubscriptionType.Downgrade];
      } else if (subscription.previousPlanAmount > subscription.planAmount) {
        subscription.type = ESubscriptionType[ESubscriptionType.Other];
      } else {
        subscription.type = ESubscriptionType[ESubscriptionType.Other];
      }

    } else if (response.bodyType == "deleted") {
      subscription.type = ESubscriptionType[ESubscriptionType.Cancel];
      subscription.previousPlanId = response.object.plan.id;
      subscription.previousPlanName = response.object.plan.name;
      subscription.previousPlanAmount = response.object.plan.amount;
    } else if (response.bodyType == "created") {
      subscription.type = ESubscriptionType[ESubscriptionType.New];
    }

    switch (response.object.plan.interval) {
      case "month":
        subscription.planInterval = ESubscriptionInterval[ESubscriptionInterval.Month];
        break;
      case "year":
        subscription.planInterval = ESubscriptionInterval[ESubscriptionInterval.Year];
        break;
      case "week":
        subscription.planInterval = ESubscriptionInterval[ESubscriptionInterval.Week];
        break;
      default:
        subscription.planInterval = null;
        break;
    }

    switch (response.object.status) {
      case "active":
        subscription.status = ESubscriptionStatus[ESubscriptionStatus.Active];
        break;
      default:
        subscription.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
    }

    if (response.bodyType === "deleted") {
      subscription.status = ESubscriptionStatus[ESubscriptionStatus.Inactive];
    }

    return subscription;
  }

  private static mapDataToRefund(response, gatewayKey) {
    let refund = new Refund();
    /**
     * Todo:
     * - The gatewayid is not populating.
     * - Need to populate old tx id on updates
     *
     */
    //const res
    refund.gatewayName = this.name;
    refund.gatewayKey = gatewayKey;
    refund.gatewayId = response.refund.id;
    refund.gatewayOriginalObject = JSON.stringify(response);
    refund.date = response.refund.created;
    refund.currency = response.refund.currency;
    refund.status = response.refund.status;
    refund.reason = response.refund.reason;
    refund.amount = response.refund.amount;
    refund.balanceTransaction = response.refund.balance_transaction;
    refund.refundReceiptNumber = response.refund.receipt_number;
    refund.gatewayTransactionId = response.data.id;
    refund.gatewayCustomerId = response.data.customer;
    refund.gatewayInvoiceId = response.data.invoice;

    return refund;
  }

  // This returns an array of refunds, unsaved
  public static mapDataToRefunds(data, gatewayKey) {
    let refunds = data.refunds.data
      .map(refund => StripeGateway.mapDataToRefund({data, refund}, gatewayKey));
    return refunds;
  }

  public static mapDataToInvoice(response, gatewayKey) {
    let invoice = new Invoice();

    invoice.gatewayName = this.name;
    invoice.gatewayKey = gatewayKey;
    invoice.gatewayId = response.id;
    invoice.amountDue = response.amount_due;
    invoice.attemptCount = response.attemptedCount;
    invoice.attempted = response.attempted;
    invoice.gatewayTransactionId = response.charge;
    invoice.currency = ECurrency[ECurrency[response.currency]];
    invoice.date = response.date;
    invoice.amount = response.total;
    invoice.periodStart = response.period_start
    invoice.periodEnd = response.period_end
    invoice.gatewaySubscriptionId = response.subscription;
    let plan = response.lines.data[0].plan;
    invoice.planId = plan.id;
    switch (plan.interval) {
      case "month":
        invoice.interval = ESubscriptionInterval[ESubscriptionInterval.Month];
        break;
      case "year":
        invoice.interval = ESubscriptionInterval[ESubscriptionInterval.Year];
        break;
      default:
        invoice.interval = null;
        break;
    }
    switch (response.closed) {
      case "closed":
        invoice.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
      default:
        invoice.status = EInvoiceStatus[EInvoiceStatus.Closed];
        break;
    }

    return invoice;
  }

  public static mapDataToTransaction(response, gatewayKey) {
    let transaction = new Transaction();

    // Core properties
    transaction.gatewayName = this.name; // stripe
    transaction.gatewayKey = gatewayKey; // reference to firebase
    transaction.gatewayId = response.id; // Identifier at gateway
    transaction.amount = response.amount; // Storing as int (no decimals)
    transaction.currency = ECurrency[ECurrency[response.currency]];
    transaction.date = response.created;
    transaction.description = response.description;
    transaction.gatewayInvoiceId = response.invoice;
    switch (response.status) {
      case "succeeded":
        transaction.status = ETransactionStatus[ETransactionStatus.Success];
        break;
      case "failed":
        transaction.status = ETransactionStatus[ETransactionStatus.Fail];
        break;
      default:
        transaction.status = null;
        break;
    } // status
    switch (response.object) {
      case "charge":
        transaction.type = Abstracts.ETransactionType[Abstracts.ETransactionType.Charge];
        break;
      default:
        transaction.type = null;
        break; // for good habits.
    } // typ
    return transaction; // Internal object from model.
  }

  // Done. Returns fee object
  public static retrieveAndMapFee(accountId, transactionId,  gatewayKey) : Promise<Fee> {
    return this.getUserStripeAccessToken(accountId)
      .then(access_token => this.retrieveTransaction({access_token, transactionId}))
      .then(transaction => {
        const fee = this.mapDataToFee(transaction, gatewayKey);
        return fee;
      });
  }

  private static getUserStripeAccessToken(accountId: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      database().ref('gateways').orderByChild('id').equalTo(accountId).limitToFirst(1)
        .once('value', snapshot => {
          if (!snapshot.exists()) {
             console.warn('Webhook received for unexisting user at database, tracked by account %s', accountId);
             return reject(Error('WebhookError: Stripe account not found'));
          }
          const snapshotObject = snapshot.val();
          const gatewayKey = Object.keys(snapshotObject)[0];
          const gateway = snapshotObject[gatewayKey];
          const access_token = gateway['access_token'];
          return resolve(access_token);
        });
    });
  }

  private static retrieveTransaction({access_token, transactionId}) : Promise<any> {
    return require('stripe')(access_token)
      .balance.retrieveTransaction(transactionId);
  }

  public getStripeResources() {
    const {_gatewayConfig: {gatewayKey}} = this;
    this.paginateAndSave('invoice.payment_succeeded', gatewayKey);
    this.paginateAndSave('customer.subscription.created', gatewayKey);
    this.paginateAndSave('customer.subscription.updated', gatewayKey);
    this.paginateAndSave('customer.subscription.deleted', gatewayKey);
    this.paginateAndSave('charge.succeeded', gatewayKey);
    this.paginateAndSave('charge.failed', gatewayKey);
    this.paginateAndSave('charge.refunded', gatewayKey);
    this.paginateTransactions(gatewayKey);
  }

  private mapStripeResource(type, resource, gatewayKey) {
    switch (type) {
      case "customer.subscription.created":
      case "customer.subscription.updated":
      case "customer.subscription.deleted":
        return StripeGateway.mapDataToSubscription(resource, gatewayKey);
      case "invoice.payment_succeeded":
        return StripeGateway.mapDataToInvoice(resource, gatewayKey);
      case "charge.succeeded":
        return StripeGateway.mapDataToTransaction(resource, gatewayKey);
      case "charge.failed":
        resource.status = 'failed';
        return StripeGateway.mapDataToTransaction(resource, gatewayKey);
      case "charge.refunded":
        return StripeGateway.mapDataToRefunds(resource, gatewayKey);
    }
  }

  private paginateTransactions(gatewayKey, last_id?) {
    console.info('Paginating transactions', gatewayKey, last_id);

    let req_params = {limit: 250};

    if (last_id !== null) {
      req_params['starting_after'] = last_id;
    }

    this._stripe.balance.listTransactions(req_params, (err, resources) => {
      const {data} = resources;
      let page = data.map(tx => StripeGateway.mapDataToFee(tx, gatewayKey)).map(fee => fee.save());
      Promise.all(page).then(_ => {
        setTimeout(_ => {
          if (resources.has_more) {
            this.paginateTransactions(gatewayKey, data[data.length - 1].id);
          }
        }, 350);
      })
    });
  }

  private paginateAndSave(type, gatewayKey, last_id?) {
    console.info('Paginating', type, gatewayKey, last_id);

    let req_params = { limit: 250, type };

    if (last_id !== null) {
      req_params['starting_after'] = last_id;
    }

    this._stripe.events.list(
      req_params,
      (err, resources) => {
        const {data} = resources;
        let page = data.map(r => {
          r = type.includes('subscription') ?  r.data : r.data.object;
          if (type === 'customer.subscription.created') {
            r.bodyType = 'created';
          } else if (type === 'customer.subscription.updated') {
            r.bodyType = 'updated';
          } else  if (type === 'customer.subscription.deleted') {
            r.bodyType = 'deleted';
          }
          // We're saving a single resource
          if (!type.includes('refund')) {
            const saved = this.mapStripeResource(type, r, gatewayKey).save();
            if (type.includes('invoice')) {
              saved.then(invoice => {
                eventHandler.emit('invoice:created:adjustment', invoice);
              });
            }
            return;
          } else if (type === 'charge.refunded') {
          // We're saving a charge.refunded case which may include multiple refunds
            let refunds = this.mapStripeResource(type, r, gatewayKey);
            return Promise.all(refunds.map(r => r.save()));
          }
        });
        Promise.all(page).then(_ => {
          setTimeout(_ => {
            if (resources.has_more) {
              this.paginateAndSave(type, gatewayKey, data[data.length - 1].id);
            }
          }, 350);
        });
      }
    );
  }
}
