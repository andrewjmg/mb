#!/usr/bin/env node
import App from './../app';
import * as http from 'http';
import * as debug from 'debug';

debug('ts-express:server');

const port = normalizePort(process.env.PORT || 4300);
App.set('port', port);

// Create the server
const server = http.createServer(App);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// Normalzie port
function normalizePort(val: number | string): number | string | boolean {
    let port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) return val;
    else if (port >= 0) return port;
    else return false;
}

// On error listener
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    if (error.syscall !== 'listen') throw error;
    let bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port;


    // Handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// Event listener
function onListening(): void {
    let addr = server.address();
    let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}