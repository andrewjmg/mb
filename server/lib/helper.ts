import * as moment from 'moment';
const chance = require('chance');
import {Invoice} from '../models/invoice';
import {IInvoice, ESubscriptionInterval} from '../models/abstracts';
import {eventHandler} from '../lib/event-handler';

export class Helper {

  public static currencyFormat(amount, currency) {
    let fc = null;

    switch (currency) {
      case 'usd':
        fc = `\$${amount / 100}`;
        break;
      default:
        fc = `\$${amount / 100}`;
    }

    return fc;
  }

  public static unixToSql(date) {
    return moment.unix(date).utc().format("YYYY-MM-DD HH:mm:ss");
  }

  public static sqlToUnix(date) {
    return moment(date).utc().unix();
  }

  static timestamp() {
    return new Date().valueOf();
  }

  public static dateToUnix(date) {
    return moment().utc().unix();
  }

  public static dateToMoment(date) {
    return moment(date);
  }

  public static getDatesRanges(daysRange: number) {
    const to = moment().utc().unix();
    const from = moment.unix(to).utc().subtract(daysRange, 'days').unix();
    return {from, to};
  }

  public static today(): string {
    return moment().utc().format('YYYY-MM-DD');
  }

  public static kpiDatesRanges() {
    // current
    const currentTo = moment().utc().unix();
    const currentFrom = moment.unix(currentTo).utc().subtract(30, 'd').unix();
    // 1 month ago
    const monthTo = moment().utc().subtract(30, 'd').unix();
    const monthFrom = moment.unix(monthTo).utc().subtract(30, 'd').unix();
    const twoMonthsTo = moment().utc().subtract(60, 'd').unix();
    const twoMonthsFrom = moment.unix(twoMonthsTo).utc().subtract(30, 'd').unix();
    // 3 months ago
    const threeMonthsTo = moment().utc().subtract(90, 'd').unix();
    const threeMonthsFrom = moment.unix(threeMonthsTo).utc().subtract(30, 'd').unix();
    // 4 months ago
    const fourMonthsTo = moment().utc().subtract(90, 'd').unix();
    const fourMonthsFrom = moment.unix(fourMonthsTo).utc().subtract(30, 'd').unix();
    // 5 months ago
    const fiveMonthsTo = moment().utc().subtract(150, 'd').unix();
    const fiveMonthsFrom = moment.unix(fiveMonthsTo).utc().subtract(30, 'd').unix();
    // 6 months ago
    const sixMonthsTo = moment().utc().subtract(180, 'd').unix();
    const sixMonthsFrom = moment.unix(sixMonthsTo).utc().subtract(30, 'd').unix();
    // 7 months ago
    const sevenMonthsTo = moment().utc().subtract(210, 'd').unix();
    const sevenMonthsFrom = moment.unix(sevenMonthsTo).utc().subtract(30, 'd').unix();
    // 8 months ago
    const eightMonthsTo = moment().utc().subtract(240, 'd').unix();
    const eightMonthsFrom = moment.unix(eightMonthsTo).utc().subtract(30, 'd').unix();
    // 9 months ago
    const nineMonthsTo = moment().utc().subtract(270, 'd').unix();
    const nineMonthsFrom = moment.unix(nineMonthsTo).utc().subtract(30, 'd').unix();
    // 10 months ago
    const tenMonthsTo = moment().utc().subtract(300, 'd').unix();
    const tenMonthsFrom = moment.unix(tenMonthsTo).utc().subtract(30, 'd').unix();
    // 11 months ago
    const elevenMonthsTo = moment().utc().subtract(330, 'd').unix();
    const elevenMonthsFrom = moment.unix(elevenMonthsTo).utc().subtract(30, 'd').unix();
    // 12 months ago
    const twelveMonthsTo = moment().utc().subtract(365, 'd').unix();
    const twelveMonthsFrom = moment.unix(twelveMonthsTo).utc().subtract(30, 'd').unix();

    return {
      currentFrom, currentTo,
      monthFrom, monthTo,
      twoMonthsFrom, twoMonthsTo,
      threeMonthsFrom, threeMonthsTo,
      fourMonthsFrom, fourMonthsTo,
      fiveMonthsFrom, fiveMonthsTo,
      sixMonthsFrom, sixMonthsTo,
      sevenMonthsFrom, sevenMonthsTo,
      eightMonthsFrom, eightMonthsTo,
      nineMonthsFrom, nineMonthsTo,
      tenMonthsFrom, tenMonthsTo,
      elevenMonthsFrom, elevenMonthsTo,
      twelveMonthsFrom, twelveMonthsTo
    }
  }

  public static kpiDates() {
    // current
    const today = moment().utc().format('YYYY-MM-DD');
    const monthAgo = moment().utc().subtract(30, 'days').format('YYYY-MM-DD');
    const threeMonthsAgo = moment().utc().subtract(90, 'days').format('YYYY-MM-DD');
    const sixMonthsAgo = moment().utc().subtract(180, 'days').format('YYYY-MM-DD');
    const aYearAgo = moment().utc().subtract(365, 'days').format('YYYY-MM-DD');
    return {today, monthAgo, threeMonthsAgo, sixMonthsAgo, aYearAgo};
  }

  public static generateAdjustmentInvoices(invoice: Invoice) {
    let {interval} = invoice;
    let adjustmentsCount;
    if (interval == ESubscriptionInterval[ESubscriptionInterval.Year]) {
      adjustmentsCount = 12;
    } else if (interval == ESubscriptionInterval[ESubscriptionInterval.Month]) {
      return;
    } else {
      console.error('Interval wasnt matched, returning');
      return;
    }
    const invoices = Array.from(Array(adjustmentsCount).keys()).map(i => { 
      let newInvoice = new Invoice();
      const newDate = moment.unix(invoice.date).add(i, 'months').unix();
      newInvoice.interval = ESubscriptionInterval[ESubscriptionInterval.Month];
      newInvoice.gatewayName = invoice.gatewayName;
      newInvoice.gatewayKey = invoice.gatewayKey;
      newInvoice.gatewayId = invoice.gatewayId;
      newInvoice.gatewaySubscriptionId = invoice.gatewaySubscriptionId;
      newInvoice.gatewayTransactionId = invoice.gatewayTransactionId;
      newInvoice.amount = invoice.amount;
      newInvoice.amountDue = invoice.amountDue;
      newInvoice.status = invoice.status;
      newInvoice.currency = invoice.currency;
      newInvoice.planId = invoice.planId;
      newInvoice.periodStart = invoice.periodStart;
      newInvoice.periodEnd = invoice.periodEnd;
      newInvoice.attempted = invoice.attempted;
      newInvoice.date = newDate;
      newInvoice.adjustment = true;
      return newInvoice.save();
    });
    Promise.all(invoices).then(_ => {
      console.info('Generated', adjustmentsCount,
        'adjustment invoices for invoice', invoice.gatewayId,
        'gatewayKey', invoice.gatewayKey
      );
      eventHandler.emit('invoice:adjustments:processed');
    })
      .catch(err => {
        console.error('Failure while creating adjustment invoices', err);
        eventHandler.emit('invoice:adjustments:failed');
      });
  }
}


export class guid {
  public static hash() {
    return new chance().hash({length: 4}); // Todo: Increase this for production.
  }
}


