import * as admin from 'firebase-admin';
import {isNull} from "util";


function firebaseAuthMiddleware(req, res, next) {
    const authorization = req.header('Authorization');
    if (authorization) {
        let token = authorization.split(' ');
        admin.auth().verifyIdToken(token[1])
            .then((decodedToken) => {
                res.locals.user = decodedToken;
                next();
            })
            .catch(err => {
                // Todo: Roles?
                next(err);
            });
    } else {
        next(false);
    }
}

export {firebaseAuthMiddleware};
