import * as moment from 'moment';
import {sequelize} from "../models/index";
import {Helper} from "./helper";

const SELECT = {type: sequelize.QueryTypes.SELECT};


export class ReportsUtils {

  private static getKpi(query, from, to, options): Promise<Number> {
    let {fromUnixTime} = options;
    let datesRange = fromUnixTime ?
        `AND (date BETWEEN FROM_UNIXTIME(${from}) AND FROM_UNIXTIME(${to}))`
      : `AND (date BETWEEN '${from}' AND '${to}')`;
    query = `${query}
     ${datesRange}
    `;
    return sequelize.query(query, SELECT).then(result => {
      result = result.shift() || {};
      return result.total || 0;
    });
  }

  private static getKpiByDates(query, date) : Promise<any> {
    query = `
      ${query}
      AND date='${date}'
    `;
    return sequelize.query(query, SELECT).then(result => {
      result = result.shift() || {};
      return result.total || 0;
    });
  }

  public static updateKpis(baseQuery: string, options?) : Promise<any> {
    options = options || {};
    const kpi = {
      current: null,
      monthToDate: {
        total: null
      },
      monthAgo: {
        total: null,
        difference: null,
        percentageChange: null
      },
      threeMonthsAgo: {
        total: null,
        difference: null,
        percentageChange: null
      },
      sixMonthsAgo: {
        total: null,
        difference: null,
        percentageChange: null
      },
      twelveMonthsAgo: {
        total: null,
        difference: null,
        percentageChange: null
      }
    };
    let {
      currentFrom, currentTo,
      monthFrom, monthTo,
      threeMonthsFrom, threeMonthsTo,
      sixMonthsFrom, sixMonthsTo,
      twelveMonthsFrom, twelveMonthsTo
    } = Helper.kpiDatesRanges();
    let {
      today, monthAgo, threeMonthsAgo, sixMonthsAgo, aYearAgo
    } = Helper.kpiDates();
    let {exactDates} = options;
    let promises;
    if (exactDates) {
      promises = [
        this.getKpiByDates(baseQuery, today),
        this.getKpiByDates(baseQuery, monthAgo),
        this.getKpiByDates(baseQuery, threeMonthsAgo),
        this.getKpiByDates(baseQuery, sixMonthsAgo),
        this.getKpiByDates(baseQuery, aYearAgo),
      ];
    } else {
      promises = [
        this.getKpi(baseQuery, currentFrom, currentTo, options),
        this.getKpi(baseQuery, monthFrom, monthTo, options),
        this.getKpi(baseQuery, threeMonthsFrom, threeMonthsTo, options),
        this.getKpi(baseQuery, sixMonthsFrom, sixMonthsTo, options),
        this.getKpi(baseQuery, twelveMonthsFrom, twelveMonthsTo, options)
      ];
    }
    return Promise.all(promises)
      .then(results => {
        let [current, monthAgo, threeMonthsAgo, sixMonthsAgo, twelveMonthsAgo] = results;
        kpi.current = current;
        kpi.monthAgo.total = monthAgo;
        kpi.threeMonthsAgo.total = threeMonthsAgo;
        kpi.sixMonthsAgo.total = sixMonthsAgo;
        kpi.twelveMonthsAgo.total = twelveMonthsAgo;

        kpi.monthAgo.difference = kpi.current - kpi.monthAgo.total;
        kpi.monthAgo.percentageChange = (kpi.monthAgo.difference / kpi.monthAgo.total) * 100;
        kpi.monthAgo.percentageChange = isFinite(kpi.monthAgo.percentageChange) ? kpi.monthAgo.percentageChange : false;

        kpi.threeMonthsAgo.difference = kpi.current - kpi.threeMonthsAgo.total;
        kpi.threeMonthsAgo.percentageChange = (kpi.threeMonthsAgo.difference / kpi.threeMonthsAgo.total) * 100;
        kpi.threeMonthsAgo.percentageChange = isFinite(kpi.threeMonthsAgo.percentageChange) ? kpi.threeMonthsAgo.percentageChange : false;

        kpi.sixMonthsAgo.difference = kpi.current - kpi.sixMonthsAgo.total;
        kpi.sixMonthsAgo.percentageChange = (kpi.sixMonthsAgo.difference / kpi.sixMonthsAgo.total) *100;
        kpi.sixMonthsAgo.percentageChange = isFinite(kpi.sixMonthsAgo.percentageChange) ? kpi.monthAgo.percentageChange : false;

        kpi.twelveMonthsAgo.difference = kpi.current - kpi.twelveMonthsAgo.total;
        kpi.twelveMonthsAgo.percentageChange = (kpi.twelveMonthsAgo.difference / kpi.twelveMonthsAgo.total) * 100;
        kpi.twelveMonthsAgo.percentageChange = isFinite(kpi.twelveMonthsAgo.percentageChange) ? kpi.monthAgo.percentageChange : false;

        return kpi;
      });
  }

  public static getHistory(query: string) : Promise<any> {
    return sequelize.query(query, SELECT).then(results => {
      return results.map(row => {
        return {
          x: new Date(row.date).toISOString(),
          y: row.amount
        };
      });
    });
  }
}



