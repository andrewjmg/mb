// Need to abstract this.
import { firebaseConfig } from "./../../server/config/config";
import * as admin from 'firebase-admin';
import { database, initializeApp } from 'firebase-admin';
import * as firebase from 'firebase';

admin.initializeApp({
    credential: admin.credential.cert(firebaseConfig.privateKey),
    databaseURL: firebaseConfig.databaseURL
});

firebase.initializeApp(firebaseConfig);                          // Auth / General Use);