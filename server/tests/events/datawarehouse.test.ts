import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
const dbObj = require('./../db');
import {updateDatawarehouse} from './../../events/datawarehouse-events';
import {eventHandler} from '../../lib/event-handler';


chai.use(chaiAsPromised);

describe('Events object', function () {
  this.timeout(10000);

  it('should trigger datawarehouse update', function (done) {
    const data = {
      gatewayKey: '-KvdMBY_WbeNKptNOwlQ'

    };
    updateDatawarehouse(data);
    eventHandler.on('datawarehouse:update:finished', done);
  });
});
