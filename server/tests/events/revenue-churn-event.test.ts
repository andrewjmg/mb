import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as fbInit from './../../lib/firebase-init';
const dbObj = require('./../db');
import * as events from './../../events/report-events';


chai.use(chaiAsPromised);
fbInit; // Bandaid.

describe('Events object', function () {
  this.timeout(5000);

  it('should trigger a revenue churn event', function (done) {
    const data = {
      gatewayKey: '-KvdMBY_WbeNKptNOwlQ'

    };

    events.revenueChurnReport(data);

    setTimeout(() => done(), 3000);

  });
});
