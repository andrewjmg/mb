import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as fbInit from './../../lib/firebase-init';
const dbObj = require('./../db');
import * as events from './../../events/report-events';
import {ETransactionStatus} from './../../models/abstracts';

chai.use(chaiAsPromised);
fbInit; // Bandaid.

const fail = ETransactionStatus[ETransactionStatus.Fail];
const success = ETransactionStatus[ETransactionStatus.Success];
const gatewayKey = '-KvdMBY_WbeNKptNOwlQ';

describe('Events object', function () {
  this.timeout(5000);

  it('should trigger a Transaction event', function (done) {
    this.timeout(15000);

    let failedChargesData = {
      gatewayKey,
      type: fail
    };

    let successChargesData = {
      gatewayKey,
      type: success
    }

    events.transactionReport(failedChargesData);
    events.transactionReport(successChargesData);

    setTimeout(() => done(), 14500);

  });
});
