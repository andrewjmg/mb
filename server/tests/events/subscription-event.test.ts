import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as fbInit from './../../lib/firebase-init';
const dbObj = require('./../db');
import * as events from './../../events/report-events';


chai.use(chaiAsPromised);
fbInit; // Bandaid.

describe('Subscription event', function () {
  this.timeout(10000);

  it('should trigger a subscription event', function (done) {
    const types = ['New', 'Upgrade', 'Downgrade', 'Cancel'];
    types.forEach(type => {
      let data = {
        gatewayKey: '-KvdMBY_WbeNKptNOwlQ',
        type
      };

      events.subscriptionReport(data);
    })

    setTimeout(() => done(), 5000);

  });
});
