export = {
    "users": [{
        "authType": 1,
        "email": "test@test.com",
        "firstName": "Test",
        "lastName": "Test",
        "meta": {
            "created": 1490110369674,
            "id": "a1b2c",
            "accountId": "acc123"
        },
        "password": "something-hashed",
        "gateways": [{
            id: "g1w2a",
            name: "Stripe",
            auth: {
                key: "sk_test_hX1CX9Ce4z4loo6KQXZU4N5I"
            }

        }]
    }],
    "accounts": {
        "acc123": {
            "transactions": [
                {
                    date: 1412312312,
                    status: "success",
                    type: "charge",
                    currency: "usd",
                    amount: 1499,
                    description: "Some descr..."
                }
            ]
        }
    }
};