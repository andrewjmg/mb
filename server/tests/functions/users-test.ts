import * as sinon from 'sinon';
import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
//import {database} from 'firebase-admin';
//import * as firebase from 'firebase';
const chance = require('chance');
chai.use(chaiAsPromised);

const ff = require('./../../../functions/index');

describe('Users Firebase Model', function () {

    it('test - playground', function (done) {

        const req = {
            query: {
                text: 'input'
            }
        };

        const res = {
            redirect: (code, url) => {
                assert.equal(code, 303);
                assert.equal(url, 'new_ref');
                done();
            }
        };

        ff.addMessage(req, res);

        setTimeout(function () {
            done()
        }, 2000)
    });

});
