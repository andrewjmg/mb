import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as fbInit from './../../lib/firebase-init';
import {IUser, EAuthType} from "../../models/abstracts";
import {Gateway} from "../../models/gateway";
const dbObj = require('./../db');


fbInit // This is a bandaid for now.
chai.use(chaiAsPromised);

describe('Gateway Firebase Model', function () {
    it('should add a gw to an account', function (done) {
        this.timeout(4000);

        let gw = new Gateway();
        //gw._accountId = dbObj.users[0].meta.accountId;
        gw.name = dbObj.users[0].gateways[0].name;
        gw.id = dbObj.users[0].gateways[0].id;

        let promise = gw.save();

        promise.then(res => {
            expect(promise).to.eventually.be.fulfilled;
            setTimeout(() => {
                // console.log(res)

                done()
            }, 2000);
        }).catch(err => {
                throw new TypeError(err)
            }
        );
    });
    it('should get a gw from an account', function (done) {
        this.timeout(4000);

    });
});
