import * as chai from 'chai';
import {expect, assert, should} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as fbInit from './../../lib/firebase-init';
import {Transaction} from '../../models/transaction';
import {
    ITransaction,
    ETransactionStatus,
    ETransactionType,
    ECurrency,
    EEntity,
    EAuthType
} from "../../models/abstracts";
const dbObj = require('./../db');

chai.use(chaiAsPromised);
fbInit; // Bandaid.

describe('Transaction Firebase Model', function () {
/*    it('should create an empty transaction object', function (done) {
        const tx = new Transaction();
        assert.equal(true, (tx instanceof Transaction));
        done();
    });
    it('should create a transaction and store it, with its relations', function (done) {
        this.timeout(5000);
        const txRaw: ITransaction = <ITransaction>dbObj.accounts.acc123.transactions[0];
        const tx = new Transaction(txRaw);
        tx.meta.accountId = dbObj.users[0].meta.accountId; // We inject the accountid id.

        const promise = tx.save();

        promise.then(res => {
            expect(tx.meta.key).to.not.be.null;
            expect(promise).to.eventually.be.fulfilled;
        }).catch(err => {
                throw new TypeError(err);
            }
        ).then(done, done);
    });
    it('should get many transactions', function (done) {
        this.timeout(5000);
        const tx = new Transaction(<ITransaction>dbObj.accounts.acc123.transactions[0]);
        tx.meta.accountId = dbObj.users[0].meta.accountId;

        // Store 1
        tx.save().then(res => {
        }).catch(err => {
                throw new TypeError(err);
            }
        ).then(res => {
            setTimeout(function () {

                new EntityManager({accountId: dbObj.users[0].meta.accountId}).getAll('Transaction')
                    .then(txs => {
                        expect(txs).to.be.a('object');
                    }).catch(err => {
                    throw new TypeError(err)
                }).then(done, done);
            }, 3000);
        });

    });
    it('should get one transaction', function (done) {
        this.timeout(5000);
        const tx = new Transaction(<ITransaction>dbObj.accounts.acc123.transactions[0]);
        tx.meta.accountId = dbObj.users[0].meta.accountId;

        // Store 1
        tx.save().then(res => {

            setTimeout(function () {
                new EntityManager({accountId: dbObj.users[0].meta.accountId})
                    .get('Transaction', tx.meta.key).then(tx => {
                    expect(tx.amount).to.be.a("number");
                    expect(tx).to.be.a('object');
                    done();
                }).catch(err => {
                    throw new TypeError(err)
                });
            }, 3000);

        }).catch(err => {
            throw new TypeError(err);
        });
    });*/
});
