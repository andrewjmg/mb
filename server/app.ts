import * as express from 'express';
import {json, urlencoded} from 'body-parser';
import * as path from 'path';
import * as compression from 'compression';
import * as admin from './lib/firebase-init';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as cors from 'cors';
import * as fbInit from './lib/firebase-init';
import * as morgan from 'morgan';
import * as stripeRouter from './routes/stripe';
import * as paypalRouter from './routes/paypal';
import * as registerRouter from './routes/register';
import * as customerRouter from './routes/customer';
import * as servicesRouter from './routes/services';
import * as reportsRouter from './routes/report';
import * as authRouter from './routes/auth';
import * as session from 'express-session';
import * as sequelize from './lib/mysql';
import {eventHandler} from './lib/event-handler';
import {transactionReport, subscriptionReport, generateRevenueReports} from './events/report-events';
import {updateDatawarehouse} from './events/datawarehouse-events';
import {Helper} from './lib/helper';
import {entityStream} from './events/stream-events';
import {generateStreamReport} from './events/report-events';

const validator = require('express-validator');

fbInit;

class App {
  public express: express.Application;
  public eventHandler;

  constructor() {
    this.express = express();
    this.mysql();
    this.events();
    this.middleware();
    this.routes();

    if (this.express.get('env') === 'production') {
      // In production mode run application from dist folder
      //this.express.use(this.express.static(path.join(__dirname, '/../client')));
    }
  }

  private events(): void {
    eventHandler.on("charge:succeeded", transactionReport);
    eventHandler.on("subscription:created", subscriptionReport);
    eventHandler.on("subscription:updated", subscriptionReport);
    eventHandler.on("subscription:cancelled", subscriptionReport);
    eventHandler.on("invoice:created", updateDatawarehouse);
    eventHandler.on("report:subscription:finished", updateDatawarehouse);
    //eventHandler.on("datawarehouse:update:finished", generateRevenueReports);

    // Stream / Feed Even ts
    eventHandler.on("subscription:created", entityStream);
    eventHandler.on("invoice:created", entityStream);
    eventHandler.on("transaction:created", entityStream);
    // Publish Stream to FB
    eventHandler.on("stream:added", generateStreamReport);

    eventHandler.on("invoice:created:adjustment", Helper.generateAdjustmentInvoices);
  }

  private mysql(): void {
    sequelize
      .authenticate()
      .then(function(err) {
        console.log('Connection has been established successfully.');
      })
      .catch(function (err) {
        console.log('Unable to connect to the database:', err);
      });
  }

  private middleware(): void {
    this.express.use(morgan('tiny'));
    this.express.disable('x-powered-by');
    this.express.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
    this.express.use(cors());
    this.express.use(morgan('dev'));
    this.express.use(session({
      secret: 'mbsaasrulez',
      resave: true,
      saveUninitialized: true,
      cookie: {secure: false}
    }));
    this.express.use(bodyParser.json({limit: '50mb'}));
    this.express.use(bodyParser.urlencoded({extended: true}));
    this.express.use(validator());
    this.express.use(json());
    this.express.use(compression());
    this.express.use(urlencoded({extended: true}));

    this.express.use(passport.initialize());
    this.express.use(passport.session());

  }

  private routes(): void {
    const basePath = '/api';
    this.express.use(basePath + '/auth', authRouter.default());
    this.express.use(basePath + '/gateways/stripe', stripeRouter.default());
    this.express.use(basePath + '/gateways/paypal', paypalRouter.default());
    this.express.use(basePath + '/register/', registerRouter.default());
    this.express.use(basePath + '/customer/', customerRouter.default());
    this.express.use(basePath + '/services', servicesRouter.default());
    this.express.use(basePath + '/reports', reportsRouter.default());

    // Other routes
    // 404
    this.express.use((req, res, next) => {
      let url = req.protocol + '://' + req.get('host') + req.originalUrl;
      let err = new Error(`404 Not Found. Requested URL: ${url}`);
      next(err);
    });

    // 500
    this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
      res.status(err.status || 500);
      console.log(err)
      res.json({
        env: this.express.get('env'),
        error: err.code,
        message: err.message
      });
    });

  }
}

export default new App().express;
