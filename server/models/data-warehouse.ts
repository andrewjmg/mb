import {IDatawarehouse, ESubscriptionInterval} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';
import * as moment from "moment";
import {eventHandler} from "../lib/event-handler";

const SELECT = {type: sequelize.QueryTypes.SELECT};

export default class Datawarehouse extends Persistable implements Datawarehouse {

  gatewayId: string;
  planId: string;
  date: string;
  arrTotal: number;
  mrrTotal: number;
  activeCount: number;
  arpuTotal: number;
  cancelationsCount: number;
  ltvTotal: number;
  revenueChurnRate: number;

  constructor(obj: IDatawarehouse = null) {
    super(obj);

    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Datawarehouse.model().sync({force: config.forceDb}).then(sync => {
        Datawarehouse.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public static model(): SequelizeStatic.Model<IDatawarehouse> {
    return sequelize.define<IDatawarehouse>("Datawarehouse", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      planId: {type: Sequelize.DATE, allowNull: false},
      date: {type: Sequelize.TEXT, allowNull: false},
      mrrTotal: {type: Sequelize.INTEGER, allowNull: false},
      arrTotal: {type: Sequelize.INTEGER, allowNull: false},
      activeCount: {type: Sequelize.INTEGER, allowNull: false},
      arpuTotal: {type: Sequelize.DECIMAL, allowNull: false},
      cancelationsCount: {type: Sequelize.INTEGER, allowNull: false},
      churnRate: {type: Sequelize.DECIMAL, allowNull: false},
      ltvTotal: {type: Sequelize.DECIMAL, allowNull: false},
      revenueChurnRate: {type: Sequelize.DECIMAL, allowNull: false},
    }, {
      indexes: [
         {
           type: 'unique', 
           fields: ['gatewayKey', 'planId', 'date'],
           name: 'Datawarehouse_Unique_Index'
         }
      ],
      classMethods: {},
      timestamps: false
    });
  }

  public static updateRecords(gatewayKey: string) : Promise<any> {
    const today = moment().utc().unix();
    const date = moment().utc().format('YYYY-MM-DD');
    const monthAgo = moment().utc().subtract(30, 'days').unix();
    const monthAgoCasted = moment().utc().subtract(30,'days').format('YYYY-MM-DD');

    return Promise.all([
      this.getInvoicesData(gatewayKey, monthAgo, today),
      this.getChurnCount(gatewayKey, monthAgo, today),
      this.mrrAMonthAgo(gatewayKey, monthAgoCasted),
      this.getCancelationsLast30days(gatewayKey, monthAgo, today)
    ])
    .then(this.zipByPlanId)
    .then(records => this.saveRecords(gatewayKey, date, records))
    .then(_ => {
      eventHandler.emit("datawarehouse:update:finished");
      return Promise.resolve();
    });
  }

  // mrrTotal, arrTotal, activeCount, arpuTotal
  private static getInvoicesData(gatewayKey: string, from: number, to: number) : Promise<any> {
    const query = `
      SELECT
        planId,
        COALESCE(count(*), 0) as activeCount,
        COALESCE(SUM(amount), 0) as mrrTotal
      FROM Invoices
      WHERE periodEnd BETWEEN ${from} AND ${to}
      AND gatewayKey = '${gatewayKey}'
      AND Invoices.interval = '${ESubscriptionInterval[ESubscriptionInterval.Month]}'
      GROUP BY planId
    `;
    return sequelize.query(query, SELECT);
  }

  // cancelationsCount, churnRate, ltvTotal
  private static getCancelationsCount(gatewayKey: string, from: number, to: number) : Promise<any> {
    const query = `
      SELECT
        planId,
        planName,
        COALESCE(count(*), 0) as cancelationsCount
      FROM CANCELATIONS_BY_PLAN
      WHERE castedDate BETWEEN FROM_UNIXTIME(${from}) AND FROM_UNIXTIME(${to})
      AND gatewayKey = '${gatewayKey}'
      GROUP BY planId
    `;
    return sequelize.query(query, SELECT);
  }

  // revenueChurnRate
  private static getChurnCount(gatewayKey: string, from: number, to: number) : Promise<any> {
    const query = `
      SELECT
        planId,
        planName,
        COALESCE(SUM(churn), 0) + COALESCE(SUM(contraction), 0) as revenueChurn
      FROM MRR
      WHERE date BETWEEN ${from} AND ${to}
      AND gatewayKey = '${gatewayKey}'
      GROUP BY planId
    `;
    return sequelize.query(query, SELECT);
  }

  private static getCancelationsLast30days(gatewayKey: string, from: number, to: number) : Promise<any> {
    const query = `
      SELECT
        planId,
        COALESCE(count(*), 0) as count
        FROM CANCELATIONS_BY_PLAN
        WHERE gatewayKey='${gatewayKey}'
        AND date BETWEEN ${from} AND ${to}
        GROUP BY planId
    `;
    return sequelize.query(query, SELECT);
  }

  // revenueChurnRate
  private static mrrAMonthAgo(gatewayKey: string, monthAgo: string) {
    const query = `
      SELECT
        planId,
        COALESCE(mrrTotal, 0) as mrrTotal
      FROM Datawarehouse
      WHERE date='${monthAgo}'
      AND gatewayKey='${gatewayKey}'
      GROUP BY planId
    `;
    return sequelize.query(query, SELECT);
  }

  private static zipByPlanId(gatewayData) {
    let [invoicesData, churnRates, mrrMonthAgo, cancelationsLast30] = gatewayData;
    let plans = churnRates.map(e => e.planId);
    return plans.map(planId => {
      // Invoices
      let mrrRecord = invoicesData.find(r => r.planId === planId) || {};
      let {mrrTotal = 0, activeCount = 0} = mrrRecord;
      // Churn counts
      let churnCountRecord = churnRates.find(r => r.planId === planId) || {};
      let {planName, revenueChurn = 0} = churnCountRecord;
      // MRR Month Ago
      let mrrMonthAgoRecord = mrrMonthAgo.find(r => r.planId === planId) || {};
      let {mrrTotal: mrrTotalMonthAgo = 0} = mrrMonthAgoRecord;
      // Cancelations last 30
      let cancelsLast30 = cancelationsLast30.find(r => r.planId === planId) || {};
      let {count: cancelsCountLast30 = 0} = cancelsLast30;
      // Calcs
      let arrTotal = mrrTotal * 12;
      let arpuTotal = activeCount ? (mrrTotal / activeCount ) : 0;
      let churnRate = activeCount ? (cancelsCountLast30 / activeCount) : 0;
      let ltvTotal = churnRate ? (arpuTotal / churnRate) : 0;
      let revenueChurnRate = mrrTotalMonthAgo ? ((revenueChurn / mrrTotalMonthAgo) * -1) : 0;

      return {
        planId,
        planName,
        mrrTotal,
        arrTotal,
        activeCount,
        arpuTotal,
        cancelsCountLast30,
        churnRate,
        ltvTotal,
        revenueChurnRate
      };
    });
  }

  private static saveRecords(gatewayKey: string, date: string, records) {
    let datawareHouseRecords = records.map(record => {
      record.gatewayKey = gatewayKey;
      record.date = date;
      let newValues = `
        ${record.mrrTotal},
        ${record.arrTotal},
        ${record.activeCount},
        ${record.arpuTotal},
        ${record.cancelsCountLast30},
        ${record.churnRate},
        ${record.ltvTotal},
        ${record.revenueChurnRate}
      `;
      const onDuplicateKeyUpdate = `
        ON DUPLICATE KEY UPDATE
          mrrTotal=${record.mrrTotal},
          arrTotal=${record.arrTotal},
          activeCount=${record.activeCount},
          arpuTotal=${record.arpuTotal},
          cancelationsCount=${record.cancelsCountLast30},
          churnRate=${record.churnRate},
          ltvTotal=${record.ltvTotal},
          revenueChurnRate=${record.revenueChurnRate}
      `;
      const fields = `
        (
          date,
          gatewayKey,
          planId,
          planName,
          mrrTotal,
          arrTotal,
          activeCount,
          arpuTotal,
          cancelationsCount,
          churnRate,
          ltvTotal,
          revenueChurnRate
        )
      `;
      const query = `
        INSERT INTO Datawarehouse
        ${fields}
        VALUES (
          '${record.date}',
          '${record.gatewayKey}',
          '${record.planId}',
          '${record.planName}',
          ${newValues}
        )
        ${onDuplicateKeyUpdate}
      `;
      return sequelize.query(query);
    });
    return Promise.all(datawareHouseRecords);
  };
}

