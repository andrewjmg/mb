import * as Abstracts from './abstracts';
import {ISubscription} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';


export class Subscription extends Persistable implements Abstracts.ISubscription {

  type;
  date;
  periodStart;
  periodEnd;
  planId;
  planName;
  planAmount;
  currency;
  planInterval;
  status;
  previousPlanId;
  previousPlanName;
  previousPlanAmount;

  constructor(obj: ISubscription = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Subscription.model().sync({force: config.forceDb}).then(sync => {
        Subscription.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public static model(): SequelizeStatic.Model<ISubscription> {
    return sequelize.define<ISubscription>("Subscription", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      gatewayId: {type: Sequelize.TEXT, allowNull: true},
      type: {type: Sequelize.TEXT, allowNull: false},
      date: {type: Sequelize.INTEGER, allowNull: false},
      periodStart: {type: Sequelize.INTEGER, allowNull: false},
      periodEnd: {type: Sequelize.INTEGER, allowNull: false},
      planId: {type: Sequelize.TEXT, allowNull: true},
      planName: {type: Sequelize.TEXT, allowNull: true},
      planAmount: {type: Sequelize.INTEGER, allowNull: false},
      currency: {type: Sequelize.TEXT, allowNull: false},
      planInterval: {type: Sequelize.TEXT, allowNull: true},
      status: {type: Sequelize.TEXT, allowNull: false},
      previousPlanId: {type: Sequelize.TEXT, allowNull: true},
      previousPlanName: {type: Sequelize.TEXT, allowNull: true},
      previousPlanAmount: {type: Sequelize.INTEGER, allowNull: true}
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

