import * as Abstracts from './abstracts';
import {IRefund} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';


export class Refund extends Persistable implements Abstracts.IRefund {

  amount;
  balanceTransaction;
  currency;
  date;
  status;
  reason;
  refundReceiptNumber;
  gatewayTransactionId;
  gatewayCustomerId;
  gatewayInvoiceId;

  constructor(obj: IRefund = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Refund.model().sync({force: config.forceDb}).then(sync => {
        Refund.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public static model(): SequelizeStatic.Model<IRefund> {
    return sequelize.define<IRefund>("Refund", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      gatewayId: {type: Sequelize.TEXT, allowNull: false},
      date: {type: Sequelize.INTEGER, allowNull: false},
      currency: {type: Sequelize.TEXT, allowNull: false},
      status: {type: Sequelize.ENUM, values: ['Pending', 'Succeeded', 'Failed', 'Cancelled','Other'], allowNull: false},
      reason: {type: Sequelize.ENUM, values: ['Duplicate', 'Fraudulent', 'Requested By Customer','Other'], allowNull: true},
      amount: {type: Sequelize.INTEGER, allowNull: false },
      balanceTransaction: {type: Sequelize.TEXT, allowNull: true},
      refundReceiptNumber: {type: Sequelize.TEXT, allowNull: true},
      gatewayTransactionId: {type: Sequelize.TEXT, allowNull: false},
      gatewayCustomerId: {type: Sequelize.TEXT, allowNull: false},
      gatewayInvoiceId: {type: Sequelize.TEXT, allowNull: true},
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

