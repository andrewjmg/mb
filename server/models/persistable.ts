import {database} from 'firebase-admin';
import {IPersistable} from "./abstracts";

export class Persistable implements IPersistable{
  gatewayName;
  gatewayKey;
  gatewayOriginalObject;
  gatewayId;

  constructor(obj = null) {
  }

  public sanitize() {
    let object = Object.assign({}, this);
    return object;
  }
}
