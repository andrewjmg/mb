export class Core {

    constructor() {
    }

    save() {
        console.log("We don't really want to save to core. This is just a demo.");
    }

    static fromJson(json) {
        let core: Core = Object.assign(new Core(), JSON.parse(json));
        return core;
    };
}