import * as Abstracts from './abstracts';
import {IFee} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';


export class Fee extends Persistable implements Abstracts.IFee {

  type;
  amount;
  date;
  transactionAmount;
  gatewayTransactionId;
  refunded;

  constructor(obj: IFee = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Fee.model().sync({force: config.forceDb}).then(sync => {
        Fee.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public static model(): SequelizeStatic.Model<IFee> {
    return sequelize.define<IFee>("Fee", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: false},
      gatewayTransactionId: {type: Sequelize.TEXT, allowNull: false},
      gatewayId: {type: Sequelize.TEXT, allowNull: false},
      // TODO proper enums at here and at mapper
      type: {type: Sequelize.ENUM, values: ['Refund', 'Charge', 'Other'], allowNull: true},
      date: {type: Sequelize.INTEGER, allowNull: false},
      amount: {type: Sequelize.INTEGER, allowNull: false},
      transactionAmount: {type: Sequelize.INTEGER, allowNull: false},
      refunded: {type: Sequelize.BOOLEAN, allowNull: false, defaultValue: 0}
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

