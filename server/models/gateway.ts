import {IGateway, IMeta, EEntity, EAuthType} from './abstracts';
import {Persistable} from "./persistable";
import {database} from 'firebase-admin';
import {isNullOrUndefined} from "util";
import * as firebase from 'firebase-admin';
import {guid} from './../lib/helper';


export class Gateway implements IGateway {
    name =  null;
    id = null;
    _key;
    _created;

    constructor(obj: IGateway = null) {
        Object.assign(this, obj);
    }

    public get(key: string = null): Promise<Gateway> {
        return new Promise((resolve, reject) => {
            if (isNullOrUndefined(key)) {
                // Nothing to return
            } else {
                database().ref(`accounts/${key}`).once("value").then(function (snapshot) {
                    let gw = new Gateway(snapshot.val());
                    resolve(gw);
                });
            }
        });
    }

    public getOneBy(key: string, value): Promise<Gateway> {
        return new Promise((resolve, reject) => {
            database().ref(`gateways`)
                .orderByChild(key).equalTo(value)
                .limitToFirst(1)
                .once("value", function (snapshot) {
                    if (!snapshot.exists()) {
                        resolve(null);
                    } else {
                        let key = Object.keys(snapshot.val())[0];
                        let newGw = snapshot.val()[key];
                        newGw._key = key;
                        resolve(<Gateway>new Gateway(newGw));
                    }

                }, err => {
                    reject(err);
                });
        });
    }

    public save(args = null): any {
        return new Promise((resolve, reject) => {
            if (isNullOrUndefined(this._key)) {
                // new object
                this._created = { ".sv": "timestamp" };

                let schemaRef = database().ref(`gateways`);
                let promise = schemaRef.push(this.sanitize(true));
                this._key = promise.key;
                return resolve(this);

            } else {
                // update
                let ref = database().ref(`gateways/${this._key}`);
                ref.update(this.sanitize(true));
                resolve(this);
            }

        });
    }

    public sanitize(flag) {
      return this;
    }
}
