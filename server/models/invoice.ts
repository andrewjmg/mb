import * as Abstracts from './abstracts';
import {IInvoice} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';
import * as moment from 'moment';

export class Invoice extends Persistable implements Abstracts.IInvoice {

  id;
  amountDue;
  attemptCount;
  attempted;
  gatewayTransactionId;
  status;
  adjustment;
  interval;
  planId;
  currency;
  date;
  amount;
  periodStart;
  periodEnd;
  gatewaySubscriptionId;
  gatewayOriginalObject;

  constructor(obj: IInvoice = null) {
    super(obj);
    Object.assign({}, this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Invoice.model().sync({force: config.forceDb}).then(sync => {
        Invoice.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public static model(): SequelizeStatic.Model<IInvoice> {
    return sequelize.define<IInvoice>("Invoice", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      gatewayId: {type: Sequelize.TEXT, allowNull: true},
      amountDue: {type: Sequelize.INTEGER, allowNull: false},
      attempted: {type: Sequelize.BOOLEAN, allowNull: true},
      attemptedCount: {type: Sequelize.INTEGER, allowNull: true},
      gatewayTransactionId: {type: Sequelize.TEXT, allowNull: true},
      planId: {type: Sequelize.TEXT, allowNull: true},
      interval: {type: Sequelize.TEXT, allowNull: true},
      status: {type: Sequelize.TEXT, allowNull: true},
      adjustment: {type: Sequelize.BOOLEAN, allowNull: true, defaultValue: false},
      currency: {type: Sequelize.TEXT, allowNull: true},
      date: {type: Sequelize.INTEGER, allowNull: false},
      amount: {type: Sequelize.INTEGER, allowNull: false},
      periodStart: {type: Sequelize.INTEGER, allowNull: true},
      periodEnd: {type: Sequelize.INTEGER, allowNull: true},
      gatewaySubscriptionId: {type: Sequelize.TEXT, allowNull: true},
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

