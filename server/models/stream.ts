import * as Abstracts from './abstracts';
import {IStream} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import config from './../config/config';
import * as moment from 'moment';

export class Stream extends Persistable implements IStream {

  date;
  entityType;
  currency;
  amount;
  planId;
  extra;

  constructor(obj: IStream = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  static composeEntry(entity){
    const obj = {
      gatewayName: entity.gatewayName,
      gatewayKey: entity.gatewayKey,
      gatewayOriginalObject: null,
      gatewayId: entity.gatewayId,
      entityType: entity.$modelOptions.name.singular,
      date: moment().utc().toString(),
      currency: entity.currency,
      amount: null,
      planId: null,
      extra: null
    };

    switch(entity.$modelOptions.name.singular){
      case "Subscription": {
        obj.planId = entity.planId;
        obj.amount = entity.planAmount;
        break;
      }

      case "Invoice": {
        obj.amount = entity.amount;
        break;
      }

      case "Transaction": {
        obj.amount = entity.amount;
        break;
      }

      default:
        break;
    }

    return obj;
  }

  public save(args = null): any {
    return new Promise((resolve, reject) => {
      Stream.model().sync({force: config.forceDb}).then(sync => {
        Stream.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public report() {
    return new Promise((resolve, reject) => {
      resolve();
    });

  }

  public static model(): SequelizeStatic.Model<IStream> {
    return sequelize.define<IStream>("Stream", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      gatewayId: {type: Sequelize.TEXT, allowNull: true},
      date: {type: Sequelize.DATE, allowNull: false},
      entityType: {type: Sequelize.TEXT, allowNull: true},
      currency: {type: Sequelize.TEXT, allowNull: true},
      amount: {type: Sequelize.INTEGER, allowNull: true},
      planId: {type: Sequelize.TEXT, allowNull: true},
      extra: {type: Sequelize.TEXT, allowNull: true}

    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

