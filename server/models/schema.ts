export const schema = {

    Transaction: {
        className: 'Transaction',
        reference: 'transactions',
        relations: {
            users: {
                type: 'child',
                entity: 'User',
                reference: 'transactions/user/id'
            },
            gatewayCusomer: {
                type: 'child',
                entity: 'GatewayCustomer',
                reference: 'gatewayCustomers/user/id'
            },
        }
    },
    User: {
        className: 'User',
        reference: 'users',
        relations: {
            transactions: {
                type: "reference",
                entity: 'Transaction',
                reference: 'transactions_users'
            }
        }
    },
    GatewayCustmer: {
        className: 'GatewayCustomer',
        reference: 'gatewayCustomers',
        relations: null
    }
};