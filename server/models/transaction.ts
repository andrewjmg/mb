import * as Abstracts from './abstracts';
import {GatewayCustomer} from "./gateway-customer";
import {ETransactionStatus} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import {EEntity} from "./abstracts";
import {ITransaction} from "./abstracts";
import {isNull, isNullOrUndefined} from "util";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import * as TIMESTAMP from 'sequelize-mysql-timestamp';
import config from './../config/config';

export class Transaction extends Persistable implements Abstracts.ITransaction {

  amount;
  currency;
  date;
  description;
  gatewayInvoiceId;
  status;
  type;

  constructor(obj: ITransaction = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): Promise<Transaction> {
    return new Promise((resolve, reject) => {
      Transaction.model().sync({force: config.forceDb}).then(sync => {
        Transaction.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public get(key: string = null): Promise<ITransaction> {
    return new Promise((resolve, reject) => {
    });
  }

  public static model(): SequelizeStatic.Model<ITransaction> {
    return sequelize.define<ITransaction>("Transaction", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      gatewayId: {type: Sequelize.TEXT, allowNull: true},
      amount: {type: Sequelize.INTEGER, allowNull: false},
      currency: {type: Sequelize.TEXT, allowNull: false},
      date: {type: Sequelize.INTEGER, allowNull: false},
      description: {type: Sequelize.TEXT, allowNull: true},
      gatewayInvoiceId: {type: Sequelize.TEXT, allowNull: true},
      status: {type: Sequelize.TEXT, allowNull: false},
      type: {type: Sequelize.TEXT, allowNull: false}
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }
}

