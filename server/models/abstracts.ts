import {GatewayCustomer} from './gateway-customer';
import {Transaction} from './transaction';
import {Gateway} from './gateway';

export default {}

export enum EEntity {
  Transaction,
  User,
  Gateway
}

export const Models = {
  Transaction,
  Gateway
}

export enum ERelations {
  transactions_users
}

export interface IPersistable {
  gatewayName: string;
  gatewayKey: string
  gatewayOriginalObject: string | null;
  gatewayId: string;
}

export interface ISchemaInfo {

}

export type TSchemaRelation = {
  source: string;
  target: string;
  relation: string;
}

export interface IMeta {
  id?: string;
  key?: string;
  uid?: string;
  accountId?: string;
  gatewayId?: string;
  created?: number | any;
  originalObject?: string | any;
}

export enum ETransactionType {
  Charge,
  Refund
}

export enum ESubscriptionType {
  New,
  Upgrade,
  Downgrade,
  Suspended,
  Other,
  Stop,
  Cancel
}

export enum EFeeType {
  Refund,
  Charge,
  Other
}

export enum ERefundStatus {
  Pending,
  Succeeded,
  Failed,
  Cancelled,
  Other
}

export enum ERefundReason {
  Duplicate,
  Fraudulent,
  'Requested By Customer'
}

export enum ECurrency {
  usd
}

export enum ESubscriptionStatus {
  Active,
  Inactive,
  Unknown
}

export enum ESubscriptionInterval {
  Month,
  Year,
  Week
}

export enum EInvoiceStatus {
  Open,
  Closed,
  Unknown
}

export enum ETransactionStatus {
  Success,
  Fail,
  Unknown
}

export enum EAuthType {
  google,
  email,
  password
}


// Models ----------------------------
export interface ITransaction extends IPersistable {
  amount: number;
  currency: ECurrency;
  date: number;
  description: string;
  gatewayInvoiceId: string | null;
  status: ETransactionStatus;
  type: ETransactionType;
}

export interface IMrr extends IPersistable {
  date: number;
  currency: ECurrency;
  amount: number;
  expansion: number;
  contraction: number;
  churn: number;
  change: number;
}


export interface ISubscription extends IPersistable {
  type?: ESubscriptionType;
  date?: number;
  periodStart?: number;
  periodEnd?: number;
  planId?: string;
  planName?: string;
  planAmount?: number;
  currency?: ECurrency;
  planInterval: string;
  status: ESubscriptionStatus;
  previousPlanId;
  previousPlanName;
  previousPlanAmount;
}

export interface IFee extends IPersistable {
  type?: EFeeType;
  amount: number;
  transactionAmount: number;
  refunded: boolean;
}

export interface IRefund extends IPersistable {
  amount: number;
  balanceTransaction: string;
  currency?: ECurrency;
  date?: number;
  status: ERefundStatus;
  reason: ERefundReason;
  refundReceiptNumber: string;
  gatewayTransactionId?: string;
  gatewayCustomerId?: string;
  gatewayInvoiceId?: string;
}

export interface IStream extends IPersistable {
  date;
  entityType;
  currency;
  amount;
  planId;
  extra;
}

export interface IInvoice extends IPersistable {
  attemptCount?: number;
  attempted?: boolean;
  gatewayTransactionId?: string;
  status?: string;
  currency?: ECurrency;
  date?: number;
  amount?: number;
  periodStart?: number;
  periodEnd?: number;
  gatewaySubscriptionId?: string;
}

export interface IDatawarehouse extends IPersistable {
  gatewayId: string;
  planId: string;
  date: string;
  arrTotal: number;
  mrrTotal: number;
  activeCount: number;
  arpuTotal: number;
  cancelationsCount: number;
  ltvTotal: number;
  revenueChurnRate: number;
}

export interface IGatewayCustomer extends IPersistable {
  email?: string;
  created?: number;
  accountBalance?: number;
  paymentSource_id?: string;
  delinquent?: boolean;
}

export interface IUser extends IPersistable {
  email: string;
  displayName: string;
  roles?: any;
}

export interface IFirebasePersistable {

}

export interface IGateway extends IFirebasePersistable {
}

// Reports ----------------------
export interface IRevenueReport {
  meta: IMeta,
  charges: {
    total: number,
    transactions: number,
    average: number
  },
  refunds: {
    total: number,
    transactions: number,
    average: number
  },
  declines: {
    total: number,
    transactions: number,
    average: number
  },
  series: {
    "7days": {},
    "30days": {}
  }
}
