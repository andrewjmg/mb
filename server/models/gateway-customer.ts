import * as Abstracts from './abstracts';
import {database} from 'firebase-admin';
import {Persistable} from './persistable';
import {isNull, isNullOrUndefined} from "util";
import {IGatewayCustomer} from "./abstracts";


export class GatewayCustomer extends Persistable implements Abstracts.IGatewayCustomer {
    static _entity = 'customers';

    id = null;
    meta = {
        created: {".sv": "timestamp"},
        originalObject: null,
        key: null,
        accountId: null
    };
    email = null;
    created: null;

    constructor(obj: IGatewayCustomer = null) {
        super(obj);
        Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
    }

    public save(args = null): any {
        if (isNullOrUndefined(this.meta.accountId))
            throw new TypeError("Account is required");

        return new Promise((resolve, reject) => {
            let schemaRef = database().ref(`accounts/${this.meta.accountId}/customers`);
            let promise = schemaRef.push(this.sanitize());
            this.meta.key = promise.key;
            resolve(promise);
        });
    }

    public get(key: string = null): Promise<IGatewayCustomer> {
        let accountId = this.meta.accountId;

        return new Promise((resolve, reject) => {
            database().ref(`accounts/${accountId}/customers/${key}`).once("value").then(function (snapshot) {
                resolve(<GatewayCustomer>new GatewayCustomer(snapshot.val()));
            });
        });
    }
}

