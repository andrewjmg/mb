import * as Abstracts from './abstracts';
import {GatewayCustomer} from "./gateway-customer";
import {ETransactionStatus} from "./abstracts";
import {database} from 'firebase-admin';
import {Persistable} from "./persistable";
import {EEntity} from "./abstracts";
import {IMrr} from "./abstracts";
import {isNull, isNullOrUndefined} from "util";
import * as SequelizeStatic from "sequelize";
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import * as TIMESTAMP from 'sequelize-mysql-timestamp';
import config from './../config/config';

export class Mrr extends Persistable implements Abstracts.IMrr {

  date;
  currency;
  amount;
  expansion;
  contraction;
  churn;
  change;

  public static model(): SequelizeStatic.Model<ITransaction> {
    return sequelize.define<ITransaction>("Transaction", {
      id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
      gatewayName: {type: Sequelize.TEXT, allowNull: false},
      gatewayKey: {type: Sequelize.TEXT, allowNull: false},
      gatewayOriginalObject: {type: Sequelize.TEXT, allowNull: true},
      date: {type: Sequelize.INTEGER, allowNull: false},
      currency: {type: Sequelize.TEXT, allowNull: false},
      amount: {type: Sequelize.INTEGER, allowNull: false},
      expansion: {type: Sequelize.INTEGER, allowNull: false},
      contraction: {type: Sequelize.INTEGER, allowNull: false},
      churn: {type: Sequelize.INTEGER, allowNull: false},
      change: {type: Sequelize.INTEGER, allowNull: false},
      generated: {type: Sequelize.INTEGER, allowNull: null}
    }, {
      indexes: [],
      classMethods: {},
      timestamps: false
    });
  }

  constructor(obj: IMrr = null) {
    super(obj);
    Object.assign(this, obj); // Object destrucuring! This overrides placeholder values
  }

  public save(args = null): Promise<Mrr> {
    return new Promise((resolve, reject) => {
      Mrr.model().sync({force: config.forceDb}).then(sync => {
        Mrr.model().build(this).save().then(record => {
          resolve(record);
        }).catch(err => {
          reject(err);
        });
      })
    });
  }

  public get(key: string = null): Promise<IMrr> {
    return new Promise((resolve, reject) => {
    });
  }
}

