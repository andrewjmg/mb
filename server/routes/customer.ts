import { Router, Response, Request, NextFunction } from 'express';
import * as admin from 'firebase-admin';

declare var require;
const str = require('stripe');

export default function () {
  const api: Router = Router();
  const stripe = str('sk_test_3oqCBXkEtIPvT2K9nRtl9QN7');
  const db = admin.database();

  api.post('/billing/info', (req: Request, res: Response, next: NextFunction) => {

    const uid      = req.body.uid;
    const usersRef = db.ref(`/users/${uid}`);

    usersRef.on('value', snapshot => {
      const user = snapshot.val();
      stripe.customers.retrieve(user.customer_id, (error, customer) => {

        if (error) return res.status(420).json({error});

        const subs = {subscription: {}, source: {}, charges: {}};
        if (customer.subscriptions && customer.subscriptions.data && customer.subscriptions.data.length > 0) {
          subs.subscription = customer.subscriptions.data[0];
        }

        if (customer.sources && customer.sources.data && customer.sources.data.length > 0) {
          subs.source = customer.sources.data[0];
        }

        stripe.charges.list({limit: 5, customer: user.customer_id}, (err, charges) => {

          if (error) res.status(420).json({error});

          subs.charges = charges;
          res.status(200).json({billing: subs});
        });

      });
    }, error => {
      console.log(error);
      return res.status(500).json({say: error});
    });
  });

  api.post('/info', (req: Request, res: Response, next: NextFunction) => {
    const customerId = req.body.customerId;
    stripe.customers.retrieve(customerId, (error, customer) => {

      if (error) {
        return res.status(500).json({error});
      }


      const { sources } = customer;
      return res.status(200).json({sources});

    });
  });

  api.post('/new/card', (req: Request, res: Response, next: NextFunction) => {
    const { customerId, token, sources } = req.body;

    stripe.customers.createSource(customerId, {source: token}, (err, card) => {

      if (err) {
        return res.status(500).json({err});
      }

      stripe.customers.deleteCard(
        customerId,
        sources[0].id,
        function(error, confirmation) {

          if (error) {
            return res.status(500).json({error})
          }

          return res.status(200).json({card, confirmation});
        }
      );

    });

  });


  return api;
}
