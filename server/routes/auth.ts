import { Router, Response, Request, NextFunction } from 'express';
import {decodeJwt, setUserGateways, updateUserClaims} from '../middleware/firebase';

export default function () {
  const api: Router = Router();

  api.post('/', decodeJwt, setUserGateways, updateUserClaims, (req: Request, res: Response) => {
    const uid = req['uid'];
    return res.json({status: 'success', message: `Updated user claims for ${uid}`});
  });

  return api;
}
