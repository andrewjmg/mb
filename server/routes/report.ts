import { Router, Response, Request, NextFunction } from 'express';
import {Arpu} from '../reports/arpu';
import {Arr} from '../reports/arr';
import {Ltv} from '../reports/ltv';
import {Mrr} from '../reports/mrr';
import {RevenueChurn} from '../reports/revenue-churn';
import {Subscription} from '../reports/subscription';
import {Transaction} from '../reports/transaction';
import {UserChurn} from '../reports/user-churn';

export default function () {
  const api: Router = Router();


  api.get('/custom', (req: Request, res: Response, next: NextFunction) => {
    console.log('asdadadadas')
    let {gatewayKey, report, from , to, type} = req.query;
    console.log(gatewayKey, report, from, to, type)
    if (!gatewayKey || !report || !from || !to) {
      return res.sendStatus(400);
    }

    from = Number(from);
    to   = Number(to);

    let customPromise;

    switch (report) {
      case 'arpu':
        const arpu    = new Arpu({gatewayKey});
        customPromise = arpu.generateCustom(from, to);
        break;

      case 'mrr':
      case 'arr':
        const mrr     = new Mrr({gatewayKey});
        customPromise = mrr.generateCustom(from, to);
        break;

      case 'ltv':
        const ltv     = new Ltv({gatewayKey});
        customPromise = ltv.generateCustom(from, to);
        break;

      case 'revenuechurn':
        const revenuechurn = new RevenueChurn({gatewayKey});
        customPromise      = revenuechurn.generateCustom(from , to);
        break;

      case 'subscription':
        if (!type) {
          return res.sendStatus(400);
        }
        console.log('Creating report object', type, gatewayKey);
        const subscription = new Subscription({gatewayKey, type});
        customPromise      = subscription.generateCustom(from, to);
        break;

      case 'transaction':
        const transaction = new Transaction({gatewayKey});
        customPromise     = transaction.generateCustom(from, to);
        break;

      case 'churn':
        const churn   = new UserChurn({gatewayKey});
        customPromise = churn.generateCustom(from, to);
        break;

      default:
        customPromise = null;
        break;
    }

    if (!customPromise) {
      return res.status(404).json({error: 'Not Found'});
    }

    customPromise
      .then(data => {
        return res.status(200).json({data})
      })
      .catch(error => {
        return res.status(500).json({error});
      });

  });


  return api;
}
