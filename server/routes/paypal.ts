import {PaypalGateway} from '../gateways/paypal-gateway';
import {database} from 'firebase-admin';
import {eventHandler} from './../lib/event-handler';
import {dataTypes, Sequelize} from "sequelize";
import {models, sequelize} from "../models/index";
import {Request, Response, Router} from 'express';
import * as config from './../config/config';
import {setHookData, getGatewayKey, mapHookResource} from '../middleware/paypal';
const PaypalSDK = require('paypal-rest-sdk');


export default function () {

  // Authenticating PaypalSDK
  PaypalSDK.configure(config.paypal);

  const api: Router = Router();

  /**
   * IPN endpoint
   */
  api.post('/ipn/:businessEmail', (req: Request, res: Response) => {
    const businessEmail = req.params.businessEmail ? req.params.businessEmail : null;

    if (businessEmail === null) {
      res.status(422).send({error: `Business email id is required.`});
    }

    // req.body.userId = businessEmail;
    console.log("Webhook request! Business email = ", businessEmail);

    // invoice subscr_payment

    database().ref('gateways')
      .orderByChild('id').equalTo(businessEmail)
      .limitToFirst(1)
      .once("value", function (snapshot) {
        if (!snapshot.exists()) {
          res.status(422).send({error: `Unable to find gateway with id: ${businessEmail}`});
        } else {
          const gatewayKey = Object.keys(snapshot.val())[0];
          switch (req.body.txn_type) {
            // New Subscription
            case 'subscr_signup':
              req.body.bodyType = 'created'; // injecting some elements

              PaypalGateway.mapDataToSubscription(req.body, gatewayKey)
                .save() // To mysql
                .then(tx => {
                  eventHandler.emit("subscription:created", tx);
                  res.status(200).send({status: "success"});
                })
                .catch(err => {
                  console.error(err);
                  res.status(500).send({error: err});
                });

              break;





            // New invoice (payment for a subscription)
            case 'subscr_payment':
              req.body.bodyType = 'created'; // injecting some elements

              PaypalGateway.mapDataToInvoice(req.body, gatewayKey)
                .save() // To mysql
                .then(tx => {
                  eventHandler.emit("invoice:created", tx);
                  res.status(200).send({status: "success"});
                })
                .catch(err => {
                  console.error(err);
                  res.status(500).send({error: err});
                });
              break;


            case 'BILLING.SUBSCRIPTION.SUSPENDED':
              req.body.bodyType = 'suspended';
              PaypalGateway.mapDataToSubscription(req.body, gatewayKey)
                .save() // To mysql
                .then(tx => {
                  eventHandler.emit("subscription:created", tx);
                }).catch(err => {
                console.error(err);
              });
              break;
            case 'BILLING.SUBSCRIPTION.UPDATED':
              return new Promise((resolve, reject) => {
                const query = `
                    SELECT * FROM Subscriptions
                    WHERE gatewayKey = '${gatewayKey}'
                    `;
                sequelize.query(query, {type: sequelize.QueryTypes.SELECT})
                  .then(subscription => {
                    req.body.previous_attributes = subscription;
                    req.body.bodyType = 'updated';
                    PaypalGateway.mapDataToSubscription(req.body, gatewayKey);
                    eventHandler.emit("subscription:updated", req.body);
                    resolve(true);
                  }).catch(err => {
                  reject(err);
                });
              });
            default:
              console.log('Some unknown webhook/IPN');
              res.status(422).send({message: `Webhook/IPN not implemented for transaction type: ${req.body.txn_type}`});
              break;
          }
        }
      });
  });

  api.post('/webhooks/:user', setHookData, getGatewayKey, mapHookResource, (req: Request, res: Response) => {
    const {params: {user}} = req;
    console.log(JSON.stringify(req.body, null, 2));
    return res.sendStatus(200);
  });

  api.get('/verify-webhooks', (req: Request, res: Response) => {
    PaypalSDK.configure(config.paypalServer);

    PaypalSDK.notification.webhook.list(function (error, webhooks) {
      if (error) {
        res.status(500).json({'error': error});
      } else {
        console.log(webhooks);
        res.status(200).json({'message': webhooks});
      }
    });

  });

  /**
   * Token authentication and webhook creation
   */
  api.post('/token', (req: Request, res: Response) => {
    PaypalSDK.configure(config.paypal);
    const openIdconnect = PaypalSDK.openIdConnect;
    openIdconnect.tokeninfo.create(req.body.code, function (error: any, tokeninfo: any) {
      if (error) {
        console.error('Error creating token', error);
        res.status(480).json({'error': error});
      } else {
        console.log(tokeninfo);
        openIdconnect.userinfo.get(tokeninfo.access_token, function (error: any, userinfo: any) {
          if (error) {
            console.error("Error creating user info", error);
            res.status(500).json({error});
          } else {
            console.log(userinfo);
            const userId: any = userinfo.user_id.split("/");
            const url = 'https://sdev-api.metricbeast.io/api/gateways/paypal/webhooks/' + userId[userId.length - 1];
            const event_types = [
              // Subscriptions start

              {
                name: 'BILLING.SUBSCRIPTION.CREATED'
              },

              {
                name: 'BILLING.SUBSCRIPTION.CANCELLED'
              },

              {
                name: 'BILLING.SUBSCRIPTION.RE-ACTIVATED'
              },

              {
                name: 'BILLING.SUBSCRIPTION.SUSPENDED'
              },

              {
                name: 'BILLING.SUBSCRIPTION.UPDATED'
              },
              // Subscriptions end
              // Transactions
              {
                name: 'PAYMENT.SALE.COMPLETED'
              },
              // Failed charges
              {
                name: 'PAYMENT.SALE.DENIED'
              },
              // Refunds
              {
                name: 'PAYMENT.SALE.REFUNDED'
              }
            ];
            // const event_types = events.map(name => name.name );
            const webhooks = {
              url,
              event_types
            };

            PaypalSDK.configure(config.paypalServer);

            console.log('WEBHOOK_PAYPAL', webhooks);
            PaypalSDK.notification.webhook.create(webhooks, function (err: any, webhook: any) {
              if (err) {
                console.log('ERROR_GENERAL', err);
                if (err.name === 'WEBHOOK_URL_ALREADY_EXISTS') {
                  console.error('Attempting to recreate a webhook for a user');
                  return res.status(400).json({error: 'Webhook already exists'});
                } else {
                  console.log(err);
                  console.error('Error while attempting to create PayPal webhook', err.message, err.name);
                  return res.status(500).json({error: err.message});
                }
              }
              const response = {
                access_token: tokeninfo.access_token,
                refresh_token: tokeninfo.refresh_token,
                id: userinfo.user_id,
                logout_url: openIdconnect.logoutUrl({'id_token': tokeninfo.id_token}),
                user: userinfo
              };
              console.log(response);
              return res.status(200).send(response);
            });
          }
        });
      }
    });
  });

  return api
}
