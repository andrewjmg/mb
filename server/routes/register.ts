import { Router, Response, Request, NextFunction } from 'express';
declare var require;
const stripe = require('stripe')('sk_test_3oqCBXkEtIPvT2K9nRtl9QN7');

export default function () {
  const api: Router = Router();
  api.get('/plans', (req: Request, res: Response, next: NextFunction) => {
    stripe.plans.list({}, (err, plans) => {
      if (err) {
        res.status(500).json(err);
      }else {
        const month: any[] = [];
        const year: any[] = [];
        plans.data.map(plan => {
          if (plan.interval === 'month') {
            month.push(plan)
          }else {
            year.push(plan);
          }
        });
        month.sort((a, b) => a.id - b.id );
        year.sort((a, b) => a.id - b.id );
        res.status(200).json({ month, year });
      }
    });
  });

  api.post('/customer/create', (req: Request, res: Response, next: NextFunction) => {
    stripe.customers.create({
      email: req.body.email,
      source: req.body.source
    }, (err, customer) => {
      err ? res.status(500).json(err) : res.status(200).json(customer);
    });
  });

  api.post('/customer/subscription', (req: Request, res: Response, next: NextFunction) => {
    stripe.subscriptions.create({
      customer: req.body.customer,
      items: [
        {
          plan: req.body.plan_id
        }
      ]
    }, (err, subscription) => {
      err ? res.status(500).json(err) : res.status(200).json(subscription);
    });
  });


  api.post('/account', (req: Request, res: Response, next: NextFunction) => {
    console.log(JSON.stringify(req.body, null, 2));
    const {email, source, metadata, plan_id: plan} = req.body;
    stripe.customers.create({email, source, metadata}, (customerErr, customer) => {
      if (customerErr) {
        console.error('Error creating stripe customer', customerErr);
        return res.status(500).json({error: customerErr});
      }
      console.info('Customer created', customer);
      const {id} = customer;
      stripe.subscriptions.create({
        customer: id,
        items: [
          {
            plan
          }
        ]
      }, (subscriptionErr, subscription) => {
        if (subscriptionErr) {
          return res.status(500).json({error: subscriptionErr});
        }
        return res.status(200).json({customer, subscription});
      });
    });

  });


  return api;
}
