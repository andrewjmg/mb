import { Router, Response, Request, NextFunction } from 'express';
import * as admin from 'firebase-admin';
import * as request from 'request';
import * as fs from 'fs';
declare var require;
const str = require('stripe');

export default function () {
  const api: Router = Router();
  const db          = admin.database();

  api.post('/pdf', (req: Request, res: Response, next: NextFunction) => {


    const exports                       = db.ref().child('exports');
    const newExport                     = exports.push();
    const { report, range, reportName } = req.body;
    newExport.set({report, range, reportName}).then(() => {

      request({
        url: 'https://restpack.io/api/html2pdf/v2/convert',
        method: 'GET',
        qs: {
          access_token: 'BUgmdt2EIqh7AOFg39kX4Y9IC3CCXbnDu9blxFTtbC3KzzHO',
          url: `http://sdev.metricbeast.io/services/pdf/${newExport.key}`,
          json: true,
          fresh: true,
          pdf_margins: 2,
          delay: '10000'
        }
      }, (err, resp, body) => {
        if (err) {
          res.status(500).json({err});
        }
        return res.json({body});
      });


    }).catch(error => {
      res.status(500).json({error});
    });
  });

  return api;
}
