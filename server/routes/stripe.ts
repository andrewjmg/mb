import {Router, Response, Request, NextFunction} from 'express';
import * as Abstract from './../models/abstracts';
import {isNullOrUndefined} from "util";
import {StripeGateway} from "../gateways/stripe-gateway";
import {database} from 'firebase-admin';
import * as request from 'request';
import * as config from './../config/config';
import {Transaction} from '../reports/transaction';
import {eventHandler} from './../lib/event-handler';
import * as logger from 'morgan';
import {ESubscriptionType} from "../models/abstracts";

export default function () {
  const api: Router = Router();

  /**
   * Main webhook
   */
  api.post('/webhook', (req: Request, res: Response, next: NextFunction) => {
    // For easier testing.
    const userId = (req.body.account === 'acct_00000000000000') ? 'acct_19s0rJIF8FPjO9pQ' : req.body.account;

    database().ref(`gateways`)
      .orderByChild('id').equalTo(userId)
      .limitToFirst(1)
      .once("value", function (snapshot) {
        if (!snapshot.exists()) {
          res.status(422).send({error: `Unable to find gateway with key: ${req.body.user_id}`});
        } else {

          let gatewayKey = Object.keys(snapshot.val())[0]
          const {body: {type}} = req;
          switch (type) {
            case "charge.succeeded": {
              const transactionId = req.body.data.object.balance_transaction;
              const transaction = StripeGateway.mapDataToTransaction(req.body.data.object, gatewayKey).save();
              const fee =  StripeGateway.retrieveAndMapFee(userId, transactionId, gatewayKey)
                .then(fee => fee.save());
              Promise.all([transaction, fee])
                .then(_ => {
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error processing %s', type, err);
                  res.status(422).send({error: err, mssage: err.message});
              });
              break;
            }

            case "charge.failed": {
              const {body: {data: {object: transaction}}} = req;
              transaction.status = 'failed';
              const charge = StripeGateway.mapDataToTransaction(transaction, gatewayKey).save();
              charge.then(_ => {
                res.status(201).send({status: "success"});
              })
              .catch(err => {
                console.error('Error processing %s', type, err);
                res.status(422).send({error: err, mssage: err.message});
              });
              break;
            }

            case "invoice.payment_succeeded": {

              StripeGateway.mapDataToInvoice(req.body.data.object, gatewayKey)
                .save() // To mysql
                .then(invoice => {
                  eventHandler.emit("invoice:created:adjustment", invoice);
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error when processing Stripe hook', req.body.type, err);
                  res.status(422).send({error: err, mssage: err.message});
              });

              break;
            }

            case "customer.subscription.created": {

              req.body.data.bodyType = "created"; // Needed to move down the type.
              StripeGateway.mapDataToSubscription(req.body.data, gatewayKey)
                .save() // To mysql
                .then(subscription => {
                  eventHandler.emit("subscription:created", subscription);
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error when processing Stripe hook', req.body.type, err);
                  res.status(422).send({error: err, mssage: err.message});
              });

              break;
            }

            case "customer.subscription.updated": {

              req.body.data.bodyType = "updated"; // Needed to move down the type.
              StripeGateway.mapDataToSubscription(req.body.data, gatewayKey)
                .save() // To mysql
                .then(subscription => {
                  eventHandler.emit("subscription:updated", subscription);
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error when processing Stripe hook', req.body.type, err);
                  res.status(422).send({error: err, mssage: err.message});
              });

              break;
            }

            case "customer.subscription.deleted": {

              req.body.data.bodyType = "deleted"; // Needed to move down the type.
              StripeGateway.mapDataToSubscription(req.body.data, gatewayKey)
                .save() // To mysql
                .then(subscription => {
                  eventHandler.emit("subscription:cancelled", subscription);
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error when processing Stripe hook', err);
                res.status(422).send({error: err, mssage: err.message});
              });

              break;
            }

            case "charge.refunded": {
              let refunds = StripeGateway.mapDataToRefunds(req.body.data.object, gatewayKey);
              Promise.all(refunds.map(refund => refund.save()))
                .then(refund => {
                  eventHandler.emit("refunds:created", req.body.data.object);
                  res.status(201).send({status: "success"});
                }).catch(err => {
                  console.error('Error when processing Stripe hook', err);
                res.status(422).send({error: err, mssage: err.message});
              });

              break;
            }

            default: {
              console.log(req.body.type)
              res.status(202).send({error: `Stripe method: ${req.body.type} not mapped in router`});
              break;
            }

          }
        }
      }, err => {
        res.status(422).send({error: err.message});
      });
  });

  /**
   * Main callback after oauth
   */
  api.get('/callback', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({'session': req.session});
  });

  /**
   * Token endpoint required to exchange with stripe
   */
  api.post('/token', (req: Request, res: Response, next: NextFunction) => {
    request.post({
      url: 'https://connect.stripe.com/oauth/token',
      form: {
        grant_type: 'authorization_code',
        client_id: config.stripe.clientID,
        code: req.body.code,
        client_secret: config.stripe.clientSecret
      }
    }, function (err, r, body) {
      if (err)
        return res.status(422).send({error: err.message});

      body = JSON.parse(body);
      if (isNullOrUndefined(body.access_token)) {
        return res.status(422).send({error: body});
      }
      return res.send({
        access_token: body.access_token,
        id: body.stripe_user_id
      });
    });
  });

  api.put('/:gateway_key', (req: Request, res: Response, next: NextFunction) => {
    let {gateway_key: gatewayKey} = req.params;
    let {access_token} = req.body;
    let newGateway = new StripeGateway({access_token, gatewayKey});
    newGateway.getStripeResources();
    return res.sendStatus(200);
  });

  return api;
}
