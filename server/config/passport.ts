import * as passport from 'passport';
import {authConfig} from './auth';
import * as Jwt from 'passport-jwt'; // .Strategy; ExtractJwt;
import * as PassportLocal from 'passport-local';
import {isNullOrUndefined} from "util"; //.Strategy;
import * as config from './../config/config';
const Stripe = require('./../lib/sm-stripe-passport/');

let localOptions = {
    usernameField: 'email'
};


let jwtOptions = {
    jwtFromRequest: Jwt.ExtractJwt.fromAuthHeader(),
    secretOrKey: authConfig.secret
};

let stripeLogin = new Stripe.Strategy({
        clientID: config.stripe.clientID,
        clientSecret: config.stripe.clientSecret,
        callbackURL: config.stripe.callbackURL,
        state: 'iamconfused'
    }, function (accessToken, refreshToken, stripeProperties, done) {
        done(null, 1);
    }
);

passport.use(stripeLogin);
