import {Response, Request, NextFunction} from 'express';
import {database, auth} from 'firebase-admin';
import {PaypalGateway} from '../gateways/paypal-gateway';
import *  as logger from 'morgan';

export const setHookData = (req: Request, res: Response, next: NextFunction) => {
  const {body: {id: eventId, event_type, resource}} = req;
  req['hookEvent'] = event_type;
  req['resource'] = resource;
  return next();
}

export const getGatewayKey = (req: Request, res: Response, next: NextFunction) => {
  const {params: {user}} = req;
  database().ref('gateways').orderByChild('uid').equalTo(user).limitToFirst(1)
    .once('value', snapshot => {
      if (!snapshot.exists()) {
        console.warn('No gateway keys found for authenticated user', user);
        return res.status(404).json({error: 'No existing gateway keys found'});
      }
      let gatewayKey = Object.keys(snapshot.val())[0]
      req['gatewayKey'] = gatewayKey;
      return next();
    })
    .catch(err => {
      logger.error('Error while searching PayPal user gateway', err);
      return res.sendStatus(500);
    });
}

export const mapHookResource = (req: Request, res: Response, next: NextFunction) => {
  const gatewayKey = req['gatewayKey'];
  const hookEvent = req['hookEvent'];
  const resource = req['resource'];

  switch (hookEvent) {
    case 'BILLING.SUBSCRIPTION.CREATED':
    case 'BILLING.SUBSCRIPTION.CANCELLED':
    case 'BILLING.SUBSCRIPTION.RE-ACTIVATED':
    case 'BILLING.SUBSCRIPTION.SUSPENDED':
    case 'BILLING.SUBSCRIPTION.UPDATED':
      const subscription = PaypalGateway.mapDataToSubscription(resource, gatewayKey);
      subscription.save().then(_ => {

      });
      break;
    case 'PAYMENT.SALE.COMPLETED':
    case 'PAYMENT.SALE.DENIED':
      const charge = PaypalGateway.mapDataToTransaction(resource, gatewayKey);
      charge.save().then(_ => {

      });
      break;
    case 'PAYMENT.SALE.REFUNDED':
      const refund = PaypalGateway.mapDataToRefund(resource, gatewayKey);
      refund.save().then(_ => {

      });
      break;
  }
}
