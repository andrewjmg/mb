import {Response, Request, NextFunction} from 'express';
import {database, auth} from 'firebase-admin';

const WRONG_JWT_CODE = 'auth/argument-error';

export const decodeJwt = (req: Request, res: Response, next: NextFunction) => {
  const header = req.header('Authorization');
  if (!header) {
    return res.status(401).json({error: 'No Authorization header provided'});
  }
  
  const idToken = header.split('Bearer ')[1];
  if (!idToken) {
    return res.status(401).json({error: 'No valid Authorization header format provided'});
  }

  auth().verifyIdToken(idToken)
    .then(function(decodedToken) {
      const uid = decodedToken.uid;
      req['uid'] = uid;
      if (decodedToken.gateways) {
        req['userGateways'] = decodedToken.gateways;
      } 
      return next();
    }).catch(err => {
      const {code} = err;
      // TODO: Must check expired JWT cases
      if (code === WRONG_JWT_CODE) {
        console.warn('Tampered JWT provided', err.code);
        return res.status(401).json({error: 'No valid JWT provided'});
      } else {
        console.error('Error while validating user idToken', err.code);
        return res.status(500).json({error: 'Error authenticating user. Contact support'});
      }
    });
}

export const setUserGateways = (req: Request, res: Response, next: NextFunction) => {
  const uid = req['uid'];
  database().ref('gateways').orderByChild('uid').equalTo(uid)
    .once('value', snapshot => {

      if (!snapshot.exists()) {
        console.warn('No gateway keys found for authenticated user', uid);
        return res.status(404).json({error: 'No existing gateway keys found'});
      }

      const gateways = snapshot.val();
      const userGateways = Object.keys(gateways).map(gateway => {
        return {
          gatewayKey: gateway,
          name: gateways[gateway].name,
          type: gateways[gateway].type,
          deleted: gateways[gateway].deleted,
        };
      });
      req['userGateways'] = userGateways;
      return next();
    });
}

export const updateUserClaims = (req: Request, res: Response, next: NextFunction) => {
  const uid = req['uid'];
  const gateways = req['userGateways'];
  const customClaims = {gateways};
  auth().setCustomUserClaims(uid, customClaims)
    .then(next)
    .catch(err => {
      console.error('Error while updating user', uid, 'claims', customClaims, err);
      return res.status(500).json({error: 'Error while authorizing'});
    });
}
