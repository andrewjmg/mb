import {database} from 'firebase-admin';
import {Stream} from '../models/stream';
import {Subscription} from '../models/subscription';
import {eventHandler} from "../lib/event-handler";
import {Transaction} from "../models/transaction";
import * as moment from 'moment';


export const entityStream = function (entity) {
  console.log('Stream event triggered for gatewayKey', entity.gatewayKey);


  const stream = new Stream(Stream.composeEntry(entity))
    .save()
    .then(data => {
      eventHandler.emit('stream:added', data);
      console.log('Finished subscription stream');
    })
    .catch(err => {
      console.error(err);
    });
};
