import {database} from 'firebase-admin';
import Datawarehouse from '../models/data-warehouse';
import {eventHandler} from "../lib/event-handler";

export const updateDatawarehouse = function ({gatewayKey}) {
  console.log('Datawarehouse event triggered for gatewayKey', gatewayKey);
  Datawarehouse.updateRecords(gatewayKey)
    .then(_ => {
      console.log('Finished datawarehouse table');
    })
    .catch(err => {
      console.error(err);
    });
};

