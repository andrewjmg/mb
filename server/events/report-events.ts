import * as fbInit from './../lib/firebase-init';
import {database} from 'firebase-admin';
import {Transaction} from '../reports/transaction';
import {Subscription} from './../reports/subscription';
import {Mrr} from './../reports/mrr';
import {Arpu} from './../reports/arpu';
import {UserChurn} from './../reports/user-churn';
import {RevenueChurn} from './../reports/revenue-churn';
import {Stream} from './../reports/stream';
import {Ltv} from './../reports/ltv';
import {eventHandler} from '../lib/event-handler';

fbInit;

export const transactionReport = function (data) {
  console.info('Transaction event triggered');
  const {gatewayKey, type} = data;
  const revenueReport = new Transaction({gatewayKey, type});
  revenueReport.generate({daysRange: 7}).then(() => {
    revenueReport.generate({daysRange: 14}).then(() => {
      revenueReport.generate({daysRange: 30});
      console.info('Transaction report generated.');
    });
  }).catch(err => {
    console.error(err);
  });
};

export const subscriptionReport = function (data) {
  console.info('Subscription event triggered');
  const {gatewayKey, type} = data;
  const subscriptionReport = new Subscription({gatewayKey, type});
  subscriptionReport.generate({daysRange: 7}).then(() => {
    subscriptionReport.generate({daysRange: 14}).then(() => {
    subscriptionReport.generate({daysRange: 30}).then(() => {
    console.info('Subscription report generated');

    eventHandler.emit('reports:suscription:finished', data);
    console.info('Subscription report triggered within subscription event.');
    });
    });
  }).catch(err => {
    console.error(err);
  });
};

export const mrrReport = function (data) : Promise<any> {
  console.info('Mrr event triggered');
  const mrrReport = new Mrr({gatewayKey: data.gatewayKey});
  return mrrReport
    .generate({daysRange: 7})
    .then(_ => mrrReport.generate({daysRange: 14}))
    .then(_ => mrrReport.generate({daysRange: 30}))
    .then(_ => {
      console.info('Mrr reports generated');
      eventHandler.emit('reports:mrr:finished', data);
      return Promise.resolve();
    }).catch(err => {
      console.error(err);
    });
};

export const arpuReport = function (data) : Promise<any> {
  console.info('ARPU event triggered');
  const arpuReport = new Arpu({gatewayKey: data.gatewayKey});
  return arpuReport
    .generate({daysRange: 7})
    .then(_ => arpuReport.generate({daysRange: 14}))
    .then(_ => arpuReport.generate({daysRange: 30}))
    .then(_ => {
      console.info('ARPU report generated');
      eventHandler.emit('reports:arpu:finished', data);
    }).catch(err => {
      console.error(err);
    });
}

export const userChurnReport = function (data) {
  console.info('User churn event triggered');
  const churnReport = new UserChurn({gatewayKey: data.gatewayKey});
  return churnReport
  .generate({daysRange: 7})
  .then(_ => churnReport.generate({daysRange: 14}))
  .then(_ => churnReport.generate({daysRange: 30}))
  .then(_ => {
    console.info('User churn report generated');
    eventHandler.emit('reports:user-churn:finished', data);
  }).catch(err => {
    console.error(err);
  });
}

export const ltvReport = function (data) {
  console.info('LTV event triggered');
  const ltvReport = new Ltv({gatewayKey: data.gatewayKey});
  return ltvReport.generate({daysRange: 7})
  .then(_ => ltvReport.generate({daysRange: 14}))
  .then(_ => ltvReport.generate({daysRange: 30}))
  .then(_ => {
    console.info('ltvReport report generated');
    eventHandler.emit('reports:ltv:finished', data);
  }).catch(err => {
    console.error(err);
  });
}

export const revenueChurnReport = function (data) {
  console.info('Revenue churn event triggered');
  const revenueChurnReport = new RevenueChurn({gatewayKey: data.gatewayKey});
  return revenueChurnReport.generate({daysRange: 7})
  .then(_ => revenueChurnReport.generate({daysRange: 14}))
  .then(_ => revenueChurnReport.generate({daysRange: 30}))
  .then(_ => {
    console.info('User churn report generated');
    eventHandler.emit('reports:revenue-churn:finished', data);
  }).catch(err => {
    console.error(err);
  });
}

export const generateRevenueReports = function (data) : Promise<any> {
  const {gatewayKey} = data;
  console.debug('Generating revenue reports for', gatewayKey);
  let mrr = mrrReport(data);
  let arpu = arpuReport(data);
  let ltv = ltvReport(data);
  let userChurn = userChurnReport(data);
  let revenueChurn = revenueChurnReport(data);
  return Promise.all([
    mrr,
    arpu,
    ltv,
    userChurn,
    revenueChurn
  ]).then(_ => {
    eventHandler.emit('reports:revenue:finished');
  });
};

export const generateStreamReport = function (streamData: Stream) {
  console.log('Stream event updated for gatewayKey', streamData.gatewayKey);
  const stream = new Stream(streamData)
    .generate({gatewayKey: streamData.gatewayKey, daysRange: 30})
    .then(_ => {
      console.log('Finished updating streams');
    })
    .catch(err => {
      console.error(err);
    });
};


