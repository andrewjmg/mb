import { MbPage } from './app.po';

describe('mb App', () => {
  let page: MbPage;

  beforeEach(() => {
    page = new MbPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
